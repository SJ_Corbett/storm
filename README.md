# StORM #

### What is StORM? ###

* A very fast, in memory ORM for Delphi.
* Originally designed for use with mobile applications but equally at home on the desktop:
    - Uses RTTI information allowing it to access database table where the structure has changed or is not known at the time of program creation.  This enables the same application to interact with multiple databases that may have different structures.
    - Ability to stream efficiently to JSON.
    - Reduces the need to code SQL.
* Tested on Seattle to Tokyo.  (Use of TJSON Object writers prevents use of earlier versions, though could be re-worked to work with earlier Delphi releases.)  
* Designed to be easily extended as needed.
* Uses FireDac to access databases.
* Generator program to create and customise classes based on existing databases.  (Currently SQLite only, but could easily be extended to extract details from other databases.)

### Contact ###


* corbett@corbtech.com.au