unit stormBaseClasses;

interface

uses
  System.Generics.Collections, System.Rtti, System.JSON, System.Classes,
  System.JSON.Writers, FireDAC.Comp.Client, FireDAC.Phys.Intf, Data.Db,
  FireDAC.DApt,
  XSuperJSON, XSuperObject, stormInterfaces;

type
  StOrmAliasOfAttribute = class(TCustomAttribute)
  private
    FString: string;
  public
    constructor Create(AliasOf: string);
    property AliasOf: string read FString;
  end;

  // TStOrmKey = record
  // private
  // FString: string;
  // FInteger: Integer;
  // function GetAsInteger: Integer;
  // procedure SetAsInteger(const Value: Integer);
  // function GetAsString: string;
  // procedure SetAsString(const Value: string);
  // public
  // class operator Implicit(const Value: string): TStOrmKey;
  // class operator Implicit(const Value: Integer): TStOrmKey;
  // class operator Implicit(const Value: TStOrmKey): string;
  // class operator Implicit(const Value: TStOrmKey): Integer;
  // property AsString: string read GetAsString write SetAsString;
  // property AsInteger: Integer read GetAsInteger write SetAsInteger;
  // end;

  TStormJsonOptions = set of (sjoIsoDateFormat);

  TStormOptionsClass = class
  strict private
    class var fUseUtcTime: Boolean;
  public
    class function GetNow: TDateTime;
    class property UseUtcTime: Boolean read fUseUtcTime write fUseUtcTime;
  end;

  TStormFieldList = class(TDictionary<string, TRttiProperty>)
  end;

  TStormBaseClass = class
  private
    fLclLastUpdated: TDateTime;
    fLclCreated: TDateTime;
    fFieldList: TStormFieldList;
    fRttiType: TRttiType;
  published
    /// <summary>Unless overridden, returns an empty string.
    /// (Could be enhanced to return the value of the
    /// first field in RTTI as a string by default.)</summary>
    function GetId: string; virtual;
    /// <summary>Unless overridden, returns zero.
    /// (Could be enhanced to return the value of the
    /// first numeric field in RTTI as a string by default.)</summary>
    function GetIdInt: Int64; virtual;

  public
    constructor Create; virtual;
    constructor CreateFromJSON(JSONObject: System.JSON.TJSONObject);
    constructor CreateFromSeperated(Seperated: string);
    procedure OnCreate; virtual;
    destructor Destroy; override;

    /// <summary>Append a JSON representation of this object as a
    /// list of JSON pairs to the given JSON Writer</summary>
    procedure AppendJson(Writer: TJSonObjectWriter;
      StormJsonOptions: TStormJsonOptions = []);

    /// <summary>Append a JSON representation of this object as a
    /// JSON array of values to the given JSON Writer.</summary>
    /// <remarks>This is much smaller/more efficient than AppendJson</remarks>
    procedure AppendJsonArray(Writer: TJSonObjectWriter);

    /// <summary>Append a JSON representation of this objects properties
    /// (ie Columns/fields) as a JSON array of string values to the
    /// given JSON Writer.</summary>
    class procedure AppendJsonArrayColumns(Writer: TJSonObjectWriter);

    /// <summary>
    /// Assigns all matching property values from the source object to
    /// this object.
    /// </summary>
    procedure Assign(Source: TStormBaseClass); virtual;

    property Id: string read GetId;
    property FieldList: TStormFieldList read fFieldList;
    property LclCreated: TDateTime read fLclCreated write fLclCreated;
    property LclLastUpdated: TDateTime read fLclLastUpdated
      write fLclLastUpdated;

    /// <summary>Return a full JSON representation of this object as a
    /// list of JSON pairs in a JSON object.</summary>
    // property JSON: TJSONObject read GetJSon;
    function GetJson(StormJsonOptions: TStormJsonOptions = [])
      : System.JSON.TJSONObject;

    /// <summary>Return a full JSON representation of this object as a
    /// JSON array of values.</summary>
    // property JSONArray: TJSONArray read GetJsonArray;
    function GetJsonArray: string;

    /// <summary>Return a full JSON representation of this object as a
    /// string.</summary>
    // property JSONString: string read GetJSonString;
    function GetJsonString(StormJsonOptions: TStormJsonOptions = []): string;

    /// <summary>Reset all linked objects.  Currently a placeholder
    /// but could be enhanced to use RTTI information (using attributes) to
    /// generically do this.
    /// </summary>
    procedure ResetLinkedObjects; virtual;

    /// <summary>Set the value of a given field.</summary>
    /// <returns>True if the value was found and set, otherwise False.<returns>
    function SetFieldValueByName(inFieldName: string; inValue: string)
      : Boolean; overload;
    function SetFieldValueByName(inFieldName: string; inValue: integer)
      : Boolean; overload;
    function SetFieldValueByName(inFieldName: string; inValue: Int64)
      : Boolean; overload;
    function SetFieldValueByName(inFieldName: string; inValue: Boolean)
      : Boolean; overload;
    function SetFieldValueByName(inFieldName: string; inValue: double)
      : Boolean; overload;
    /// <summary>Return a representation of this object as a
    /// seperated string.  Currently only returns as a tab seperated
    /// string but could be enhanced to return CSV.</summary>
    function ToSeperatedValue: string;

  end;

  // TStormBaseClassIntId = class(TStormBaseClass)
  // class function HasIntId: Boolean; override;
  // published
  // end;

  TStormPropertiesList<T: class> = class(TList<T>)
  public
    procedure RegisterProperty(inT: T);
  end;

  /// <summary>A generic list of base class items that has the ability to
  /// be loaded from and saved using JSON.
  /// </summary>
  TStormBaseClassList<T: TStormBaseClass, constructor> = class(TObjectList<T>)
  private
    fSorted: Boolean;
    fHasIntId: Boolean;
    fOnLoading: TNotifyEvent;
    fOnLoaded: TNotifyEvent;
    procedure PrivAdd(AItem: T);
    function PrivBinarySearch(Id: string; out FoundIndex: integer): T; overload;
    function PrivBinarySearch(Id: integer; out FoundIndex: integer): T;
      overload;
    procedure SetSorted(const Value: Boolean);
    function GetAsJSONString: string;
  protected
    fRttiType: TRttiType;
    fContext: TRTTIContext;
    fListName: string;
    function CreateItem: T; overload; virtual;
    function GetJson: System.JSON.TJSONObject;
  published
    constructor Create(ListName: string; AOwnsObjects: Boolean = True); virtual;
    destructor Destroy; override;
    property OnLoaded: TNotifyEvent read fOnLoaded write fOnLoaded;
    property OnLoading: TNotifyEvent read fOnLoading write fOnLoading;
  public
    /// <summary>Adds zero or more new items to the list from the given JSON
    /// string as created by GetJsonArray. (Which is an object containing the
    /// column names as an array, followed by an object containing the items as
    /// an array where each item is an array of values in the same sequence as
    /// the column array.)
    /// </summary>
    procedure AddItemsFromJsonArray(inJSONString: string); overload; virtual;

    /// <summary>Adds zero or more new items to the list from a given JSON Array
    /// object.
    /// </summary>
    procedure AddItemsFromJsonArray(inJSONArray: System.JSON.TJSONArray);
      overload; virtual;

    /// <summary>Returns a JSON string representation of the list.
    /// </summary>
    property AsJSONString: string read GetAsJSONString;

    /// <summary>Find the object by string ID.
    /// </summary>
    function GetById(Id: string): T; overload; virtual;
    /// <summary>Find the object by integer ID.
    /// </summary>
    function GetById(Id: integer): T; overload; virtual;

    /// <summary>Return a full JSON representation of this list of objects as
    /// a list of JSON records.</summary>
    property JSON: System.JSON.TJSONObject read GetJson;

    /// <summary>Return a full JSON representation of this list of objects as
    /// a list of JSON pairs.  The first, "columns", contains the column names
    /// as an array.  The second pair contains the data as an array of records.
    /// </summary>
    function GetJsonArray(fromIndex: integer = 0; toIndex: integer = -1)
      : System.JSON.TJSONObject;

    /// <summary>Return a full JSON representation of this list of objects as
    /// a string of JSON pairs.  The first, "columns", contains the column names
    /// as an array.  The second pair contains the data as an array of records.
    /// </summary>
    function GetJsonArrayString(fromIndex: integer = 0;
      toIndex: integer = -1): string;

    /// <summary>Specifies whether the Id field of the list
    /// is an Integer or not.</summary>
    property HasIntId: Boolean read fHasIntId write fHasIntId;

    /// <summary>Specifies whether the list is sorted in order of the
    /// Id field.  Setting this value to true will sort the list.</summary>
    /// <remarks>
    /// Sorted lists are much quicker to search however you need to ensure
    /// that the lit is maintained in the correct sequence after adding new
    /// entries.  The easiest way to do this is to set the sorted property
    /// to false and then back to true.
    /// </remarks>
    property Sorted: Boolean read fSorted write SetSorted;

    /// <summary>Return a representation of this list as a
    /// seperated string.  Currently only returns as a tab seperated
    /// string but could be enhanced to return CSV.</summary>
    function ToSeperatedValue: string; virtual;
  end;

  /// <summary>Extends the TStormBaseList generic list of base class items
  /// to add database functionality.
  /// </summary>
  TStormBaseClassDbList<T: TStormBaseClass, constructor> = class
    (TStormBaseClassList<T>)
  private
  strict private
    fTable: string;
    fTableId: integer;
    fFieldNames: TStringList;
    fKeyFieldNames: TStringList;
    fFieldTypes: TDictionary<string, Boolean>;
    fLogChanges: Boolean;
    fLoadOnDemand: Boolean;
    fLoaded: Boolean;
    fLoadSql: string;
    fLoadTime: Single;
    fDfltConnection: TFDConnection;
    fCountSql: string;
    fPropertyToFieldMatches: TDictionary<string, TField>;

    function CreateItem: T; overload;
    /// <summary>
    /// Load the fFieldNames and fKeyFieldNames into string lists (in
    /// upper case) from the appropriate source
    /// (database via TFDConnection or dataset).
    /// </summary>
    procedure GetFieldNames(Fdc: TFDConnection); overload; virtual;
    procedure GetFieldNames(ADataSet: TDataset); overload; virtual;
    function GetTableId(Fdc: TFDConnection): integer;
    procedure OutputChanges(inObj: T; keyValue: string; Fdc: TFDConnection);
    procedure SetPropertyToFieldMatches(Fields: TFields);
  protected
  published
    constructor Create(Table: string; AOwnsObjects: Boolean = True); virtual;
    destructor Destroy; override;

    property DfltConnection: TFDConnection read fDfltConnection
      write fDfltConnection;
    function CustomPreLoadFieldsProcessing(var inObj: T; Fields: TFields)
      : Boolean; virtual;
    procedure LoadFields(var inObj: T; Fields: TFields); virtual;
    procedure CustomPostLoadFieldsProcessing(var inObj: T;
      Fields: TFields); virtual;
    property CountSql: string read fCountSql write fCountSql;
    property LoadSql: string read fLoadSql write fLoadSql;
  public
    /// <summary>Adds a given item to the list and optionally to the database.
    /// </summary>
    /// <remarks>
    /// This could be optimised since it does a database insert and an update!
    /// </remarks>
    procedure AddItem(Fdc: TFDConnection; inItem: T); virtual;

    /// <summary>Clears all the items, setting the "Loaded" property to false.
    /// </summary>
    procedure Clear;

    /// <summary>Returns a new item that has been added it to the list. Only
    /// the key and timestamp fields are initialised.
    /// </summary>
    function CreateItem(Fdc: TFDConnection; inKey: string = ''): T; overload;
    function CreateItem(inKey: string): T; overload;

    property LoadOnDemand: Boolean read fLoadOnDemand write fLoadOnDemand;
    /// <summary>
    /// Specifies whether the data has been loaded or not.
    /// </summary>
    property Loaded: Boolean read fLoaded;

    /// <summary>Specify whether or not changes are to be logged to the
    /// LclChanges table so they can be used for minimum data exchange
    /// synchronisation.  Default is True. </summary>
    property LogChanges: Boolean read fLogChanges write fLogChanges;

    /// <summary>
    /// Procedure to load all the objects from the database using a given
    /// connection or dataset.  (Since we might be running in a thread, the
    /// code calling this needs to make sure the connection is
    /// unique to this thread.)
    /// </summary>
    procedure LoadAllItems(Fdc: TFDConnection); overload; virtual;
    procedure LoadAllItems(ADataSet: TDataset); overload; virtual;
    procedure LoadAllItems; overload;

    /// <summary>
    /// Procedure to load selected objects from the database using a given
    /// connection and SQL selection clause.  (Since we might be running in a
    /// thread, the code calling this needs to make sure the connection is
    /// unique to this thread.)
    /// </summary>
    /// <param name="SelectionClause">
    /// Additional SQL to append to the LoadSql statment.  Optional but if given
    /// it may be necessary to start with "Where" (depending on the contents of
    /// the LoadSql statment).
    /// </param>
    /// <param name="Fdc">
    /// The FiresDacx connection to use. Optional and if not given, the default
    /// connection will be used.
    /// </param>
    /// <remarks>
    /// Duplications of existing objects are not added to the list.
    /// </remarks>
    procedure LoadSelectedItems(const AdditionalSql: string;
      const Fdc: TFDConnection); overload; virtual;
    procedure LoadSelectedItems(const AdditionalSql: string = ''); overload;

    /// <summary>The cumulative time to load the table</summary>
    property LoadTime: Single read fLoadTime;

    /// <summary>
    /// Update the given item using the given database connection
    /// </summary>
    procedure UpdateDb(Item: T; Fdc: TFDConnection); overload;
    procedure UpdateDb(Item: T); overload;

    /// <summary>Removes the given item from the list and from the given
    /// data connection.
    /// </summary>
    /// <param name="Item">The item to be removed</param>
    /// <param name="Fdc">The FireDac connection used to delete the
    /// corresponding database record.  Pass nil if you don't wish to remove
    /// any records.</param>
    /// <param name="FreeItem">If item is also to be freed.  If not given
    /// this defaults to True</param>
    /// <returns>True if the operation was successful.
    /// </returns>
    function DeleteDb(var Item: T; Fdc: TFDConnection; FreeItem: Boolean = True)
      : Boolean; overload;
    function DeleteDb(var Item: T; FreeItem: Boolean = True): Boolean; overload;
    function DeleteDb(AItemId: string; Fdc: TFDConnection;
      FreeItem: Boolean = True): Boolean; overload;
    function DeleteDb(AItemId: string; FreeItem: Boolean = True)
      : Boolean; overload;

    /// <summary>Ensure the item contains all the fields defined for
    /// this list.
    /// </summary>
    /// <remarks>Note that it's possible (but unusual) for an item to
    /// have additional fields that aren't in an associated database.  (For
    /// example, if the item was created from JSON.)
    /// </remarks>
    procedure SetItemFields(Item: T);

    /// <summary>The name of the table in the database that this list
    /// was loaded from.  (Empty if not associated with a database table...
    /// which means you should be using a TStormBaseClassList!)
    /// </summary>
    property TableId: string read fTable;
  end;

  // TStormBassClassListEnumorator<T: TStormBaseClass, constructor> = class
  // (TInterfacedObject, IEnumerator<T>)
  // private
  // fIndex: integer;
  // fList: TObjectList<T>;
  // function GetCurrent: T;
  // public
  // constructor Create(aList: TObjectList<T>);
  // property Current: T read GetCurrent;
  // function MoveNext: Boolean;
  // procedure Reset;
  // end;

const
  stOrmSeperator = #9;

implementation

uses
  System.SysUtils, System.TypInfo, System.Generics.Defaults,
  System.Math, System.DateUtils,
{$IFDEF MSWINDOWS}
  MMSystem,
{$ENDIF MSWINDOWS}
  FireDAC.Stan.Param, System.JSON.Readers, System.JSON.Builders,
  System.JSON.Types, System.JSON.Utils, FireDAC.Stan.Intf,
{$IFDEF IOS}
  Macapi.CoreFoundation,
{$ENDIF IOS}
  stormFunctions;

{ TAppBaseClass }

{ TAppBaseClassList }

procedure TStormBaseClassDbList<T>.AddItem(Fdc: TFDConnection; inItem: T);
var
  fFdq: TFDQuery;
  wkSql: string;
  wkValues: string;
  wkKey: string;
  delimiter: string;
  isNumeric: Boolean;
begin
  Self.PrivAdd(inItem);
  inItem.ResetLinkedObjects;

  if (not Assigned(Fdc)) or (fTable = '') then
    Exit;

  GetFieldNames(Fdc);

  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    wkSql := 'Insert Into ' + fTable + ' (LclCreated, LclLastUpdated';
    wkValues := '';

    for wkKey in fKeyFieldNames do
    begin
      wkSql := wkSql + ', ' + wkKey;
    end;

    wkSql := wkSql + ') Values (:LclCreated, :LclLastUpdated';

    // Assume we're only creating entries with a single string or numeric key.
    // If it turns out we're also creating records with multiple
    // key fields, this will need adjusting!
    for wkKey in fKeyFieldNames do
    begin
      fFieldTypes.TryGetValue(wkKey, isNumeric);
      if isNumeric then
        delimiter := ''
      else
        delimiter := '''';
      wkSql := Format('%s, %s%s%s', [wkSql, delimiter, inItem.Id, delimiter]);
    end;

    wkSql := wkSql + ')';

    fFdq.SQL.Text := wkSql;
    fFdq.Params[0].AsDateTime := inItem.LclCreated;
    fFdq.Params[1].AsDateTime := inItem.LclLastUpdated;
    fFdq.ExecSQL;

    // Now update the other fields from the item and output a changes record.
    UpdateDb(inItem, Fdc);
  finally
    fFdq.Free;
  end;

end;

procedure TStormBaseClassDbList<T>.Clear;
begin
  inherited;
  fLoaded := false;
end;

constructor TStormBaseClassDbList<T>.Create(Table: string;
  AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  fTable := Table;
  fFieldNames := TStringList.Create;
  fKeyFieldNames := TStringList.Create;
  fFieldTypes := TDictionary<string, Boolean>.Create;
  fPropertyToFieldMatches := TDictionary<string, TField>.Create;
  // Logging for 2+ way synch...not fully implemented!
  fLogChanges := false;
  fLoadOnDemand := false;
  fLoaded := false;
  fDfltConnection := nil;

  fLoadSql := 'Select * from ' + fTable;
end;

function TStormBaseClassDbList<T>.CreateItem: T;
begin
  Result := inherited CreateItem;
  SetItemFields(Result);
  // Set attribute lists here?
end;

function TStormBaseClassDbList<T>.CreateItem(inKey: string): T;
begin
  Result := CreateItem(fDfltConnection, inKey);
end;

function TStormBaseClassDbList<T>.CreateItem(Fdc: TFDConnection;
  inKey: string): T;
var
  fFdq: TFDQuery;
  wkSql: string;
  wkKey: string;
  wkAnd: string;
  wkGuid: string;
  retrieveSQL: string;
  keyValue: string;
  delimiter: string;
  isNumeric: Boolean;
begin
  Result := CreateItem;
  Result.LclCreated := TStormOptionsClass.GetNow;
  Result.LclLastUpdated := Result.LclCreated;

  Self.PrivAdd(Result);

  if (not Assigned(Fdc)) or (fTable = '') then
    Exit;

  if (fFieldNames.Count = 0) then
  begin
    GetFieldNames(Fdc);
    SetItemFields(Result)
  end;

  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    wkSql := 'Insert Into ' + fTable + ' (LclCreated, LclLastUpdated';
    retrieveSQL := 'Select * from ' + fTable + ' Where ';

    for wkKey in fKeyFieldNames do
    begin
      wkSql := wkSql + ', ' + wkKey;
    end;

    wkSql := wkSql + ') Values (:LclCreated, :LclLastUpdated';

    wkAnd := '';
    // Assume we're only creating entries with a single string or numeric key.
    // If it turns out we're also creating records with multiple
    // key fields, this will need adjusting!
    for wkKey in fKeyFieldNames do
    begin
      fFieldTypes.TryGetValue(wkKey, isNumeric);
      if isNumeric then
        delimiter := ''
      else
        delimiter := '''';

      if (inKey = '') then
        wkGuid := StOrmGetRandomGuid
      else
        wkGuid := inKey;

      wkSql := Format('%s, %s%s%s', [wkSql, delimiter, wkGuid, delimiter]);
      retrieveSQL := retrieveSQL + wkAnd + wkKey + '=' + delimiter + wkGuid +
        delimiter;
      wkAnd := ' And ';
    end;

    keyValue := wkGuid;
    wkSql := wkSql + ')';
    fFdq.SQL.Text := wkSql;
    fFdq.Params[0].AsDateTime := Result.LclCreated;
    fFdq.Params[1].AsDateTime := Result.LclLastUpdated;
    fFdq.ExecSQL;

    // Now read back what we've just written.  Any defaults will be
    // retrieved.
    fFdq.SQL.Text := retrieveSQL;
    fFdq.Open;
    if Not fFdq.Eof then
    begin
      SetPropertyToFieldMatches(fFdq.Fields);
      LoadFields(Result, fFdq.Fields);
    end;
    // Now create changes record
    OutputChanges(Result, keyValue, Fdc);
  finally
    fFdq.Free;
  end;
end;

procedure TStormBaseClassDbList<T>.CustomPostLoadFieldsProcessing(var inObj: T;
  Fields: TFields);
begin
  //
end;

function TStormBaseClassDbList<T>.CustomPreLoadFieldsProcessing(var inObj: T;
  Fields: TFields): Boolean;
begin
  Result := True;
end;

function TStormBaseClassDbList<T>.DeleteDb(var Item: T; Fdc: TFDConnection;
  FreeItem: Boolean): Boolean;
begin
  Result := DeleteDb(Item.GetId, Fdc, FreeItem);
end;

function TStormBaseClassDbList<T>.DeleteDb(var Item: T;
  FreeItem: Boolean): Boolean;
begin
  Result := DeleteDb(Item.GetId, fDfltConnection, FreeItem);
end;

function TStormBaseClassDbList<T>.DeleteDb(AItemId: string; Fdc: TFDConnection;
  FreeItem: Boolean): Boolean;
var
  fFdq: TFDQuery;
  wkItem: T;
  wkSql: string;
  wkProp: TRttiProperty;
  wkField: TField;
  wkParam: TFDParam;
  wkChar: string;
  wkKey: string;
  wkAnd: string;
  keyValue: string;
  wkFieldName: string;

begin
  wkItem := GetById(AItemId);
  if not Assigned(wkItem) then
    Exit(false);

  Self.Extract(wkItem);
  Result := True;

  if (not Assigned(Fdc)) or (fTable = '') then
  begin
    if FreeItem then
    begin
      wkItem.DisposeOf;
      wkItem := nil;
    end;
    Exit;
  end;

  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    wkSql := 'Delete from ' + fTable + ' Where ';
    wkChar := '';

    // Create delete SQL.
    for wkProp in fRttiType.GetProperties do
    begin
      if (wkProp.Visibility in [TMemberVisibility.mvPublished,
        TMemberVisibility.mvPublic]) and (wkProp.IsWritable) and
        (wkProp.IsReadable) then
      begin
        wkFieldName := StOrmGetAliasOf(wkProp);
        if (fKeyFieldNames.IndexOf(wkFieldName) = 0) then
        begin
          wkSql := wkSql + wkChar + wkFieldName + ' = :' + wkFieldName;
          wkChar := ' and ';

          wkParam := fFdq.Params.Add;
          wkParam.Name := wkFieldName;
          case wkProp.PropertyType.TypeKind of
            TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
              TTypeKind.tkUString:
              wkParam.AsString := wkProp.GetValue(TStormBaseClass(wkItem)
                ).AsString;
            TTypeKind.tkWString, TTypeKind.tkWChar:
              wkParam.AsWideString := wkProp.GetValue(TStormBaseClass(wkItem)
                ).AsString;

            TTypeKind.tkInteger, TTypeKind.tkInt64:
              wkParam.AsInteger := wkProp.GetValue(TStormBaseClass(wkItem))
                .AsInteger;

            TTypeKind.tkFloat:
              wkParam.AsFloat := wkProp.GetValue(TStormBaseClass(wkItem))
                .AsExtended;
            // The following works for Boolean fields but won't cut the
            // mustard for real enumarated fields!
            TTypeKind.tkEnumeration:
              wkParam.AsBoolean := wkProp.GetValue(TStormBaseClass(wkItem))
                .AsBoolean;
          end;
        end;
      end;
    end;

    fFdq.SQL.Text := wkSql;
    fFdq.ExecSQL;

    // Now create changes record???
    // OutputChanges(Item, keyValue, Fdc);
    if FreeItem then
    begin
      wkItem.DisposeOf;
      wkItem := nil;
    end;
  finally
    fFdq.Free;
  end;
end;

function TStormBaseClassDbList<T>.DeleteDb(AItemId: string;
  FreeItem: Boolean): Boolean;
begin
  Result := DeleteDb(AItemId, fDfltConnection, FreeItem);
end;

destructor TStormBaseClassDbList<T>.Destroy;
begin
  fFieldNames.Free;
  fKeyFieldNames.Free;
  fFieldTypes.Free;
  fPropertyToFieldMatches.Free;
  inherited;
end;

procedure TStormBaseClassDbList<T>.GetFieldNames(Fdc: TFDConnection);
var
  catalog: string;
  schema: string;
  FDMetaInfoQuery1: TFDMetaInfoQuery;
  fieldName: string;
  isNumeric: Boolean;
  ix: integer;
begin
  if Assigned(Fdc) And (fFieldNames.Count = 0) then
  begin
    Fdc.GetCatalogNames('', fFieldNames);
    if fFieldNames.Count > 0 then
      catalog := fFieldNames[0];
    Fdc.GetSchemaNames(catalog, '', fFieldNames);
    if fFieldNames.Count > 0 then
      schema := fFieldNames[0];
    Fdc.GetFieldNames(catalog, schema, fTable, '', fFieldNames);
    Fdc.GetKeyFieldNames(catalog, schema, fTable, '', fKeyFieldNames);

    // Ensure all field names are uppercase
    for ix := 0 to fFieldNames.Count - 1 do
      fFieldNames[ix] := fFieldNames[ix].ToUpper;
    // In case this is a TFdMemTable there's no primary index so
    // assume the first field is the index.
    if (fKeyFieldNames.Count = 0) and (fFieldNames.Count > 0) then
      fKeyFieldNames.Add(fFieldNames[0])
    else
      for ix := 0 to fKeyFieldNames.Count - 1 do
        fKeyFieldNames[ix] := fKeyFieldNames[ix].ToUpper;

    // We can use the following if we need to get the field types.
    // Also good to determine foreign keys
    FDMetaInfoQuery1 := TFDMetaInfoQuery.Create(nil);
    try
      FDMetaInfoQuery1.Connection := Fdc;
      FDMetaInfoQuery1.MetaInfoKind := TFDPhysMetaInfoKind.mkTableFields;
      FDMetaInfoQuery1.CatalogName := catalog;
      FDMetaInfoQuery1.SchemaName := schema;
      FDMetaInfoQuery1.ObjectName := fTable;
      FDMetaInfoQuery1.Active := True;

      while not FDMetaInfoQuery1.Eof do
      begin
        fieldName := FDMetaInfoQuery1.FieldByName('Column_Name').AsString;
        isNumeric := FDMetaInfoQuery1.FieldByName('Column_DataType').AsInteger
        // in [TFDDataType.dtInt16, TFDDataType.dtInt32, TFDDataType.dtInt64,
        // TFDDataType.dtUInt16, TFDDataType.dtUInt32, TFDDataType.dtUInt64];
          in [3, 4, 5, 7, 8, 9];
        fFieldTypes.Add(fieldName.ToUpper, isNumeric);
        FDMetaInfoQuery1.Next;
      end;
    finally
      FDMetaInfoQuery1.Free;
    end;
  end;
end;

procedure TStormBaseClassDbList<T>.GetFieldNames(ADataSet: TDataset);
var
  field: TField;
  fieldName: string;
  isNumeric: Boolean;
begin
  fFieldNames.Clear;
  fKeyFieldNames.Clear;
  fFieldTypes.Clear;

  for field in ADataSet.Fields do
  begin
    fieldName := field.FullName.ToUpper;
    fFieldNames.Add(fieldName);
    if field.IsIndexField then
      fKeyFieldNames.Add(fieldName);

    isNumeric := field.DataType in [ftSmallint, ftInteger, ftWord, ftFloat,
      ftCurrency, ftBCD, ftAutoInc, ftTypedBinary, ftLargeint, ftFMTBcd,
      ftLongWord, ftShortint, ftByte, TFieldType.ftExtended,
      TFieldType.ftSingle];
    fFieldTypes.Add(fieldName.ToUpper, isNumeric);
  end;

  if (fKeyFieldNames.Count = 0) and (fFieldNames.Count > 0) then
    fKeyFieldNames.Add(fFieldNames[0])
end;

function TStormBaseClassDbList<T>.GetTableId(Fdc: TFDConnection): integer;
var
  wkItem: T;
  fFdq: TFDQuery;
begin
  if (not Assigned(Fdc)) or (fTableId > 0) then
    Exit(fTableId);
  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    fFdq.SQL.Text :=
      'Select LclTableNameId from LclTableNames Where Upper(TableName) = ''' +
      fTable.ToUpper + '''';
    fFdq.Active := True;

    if not fFdq.Eof then
      fTableId := fFdq.Fields[0].AsInteger;
    Result := fTableId;
  finally
    fFdq.Free;
  end;
end;

procedure TStormBaseClassDbList<T>.LoadAllItems(Fdc: TFDConnection);
var
  wkItem: T;
  fFdq: TFDQuery;
  startTime: Cardinal;

begin
  if (fLoadSql = '') or fLoadOnDemand then
    Exit;
  Self.Clear;
  if (not Assigned(Fdc)) or (fTable = '') then
    Exit;

  if Assigned(fOnLoading) then
    fOnLoading(Self);

{$IFDEF MSWINDOWS}
  startTime := timeGetTime;
{$ENDIF MSWINDOWS}
  GetFieldNames(Fdc);
  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;

    if (Self.fCountSql <> '') then
    begin
      fFdq.SQL.Text := fCountSql;
      fFdq.Active := True;
      if not fFdq.Eof then
      begin
        Self.Capacity := fFdq.Fields[0].AsInteger;
      end;
      fFdq.Active := false;
    end;

    fFdq.SQL.Text := fLoadSql;
    fFdq.Active := True;
    SetPropertyToFieldMatches(fFdq.Fields);

    while not fFdq.Eof do
    begin
      wkItem := CreateItem;
      LoadFields(wkItem, fFdq.Fields);
      Self.Add(wkItem);
      fFdq.Next;
    end;
    fFdq.Active := false;

    if fSorted then
    begin
      Sorted := false;
      Sorted := True;
    end;

{$IFDEF MSWINDOWS}
    fLoadTime := fLoadTime + ((timeGetTime - startTime) * 0.001);
{$ENDIF MSWINDOWS}
    fLoaded := True;
    if Assigned(fOnLoaded) then
      fOnLoaded(Self);

  finally
    fFdq.Free;
  end;
end;

procedure TStormBaseClassDbList<T>.LoadAllItems;
begin
  LoadAllItems(fDfltConnection);
end;

procedure TStormBaseClassDbList<T>.LoadAllItems(ADataSet: TDataset);
var
  wkItem: T;
  startTime: Cardinal;
begin
  if (not Assigned(ADataSet)) or (not ADataSet.Active) or
    (ADataSet.RecordCount = 0) then
    Exit;
  Self.Clear;

  if Assigned(fOnLoading) then
    fOnLoading(Self);

{$IFDEF MSWINDOWS}
  startTime := timeGetTime;
{$ENDIF MSWINDOWS}
  GetFieldNames(ADataSet);
  Self.Capacity := ADataSet.RecordCount;

  ADataSet.First;
  SetPropertyToFieldMatches(ADataSet.Fields);
  while not ADataSet.Eof do
  begin
    wkItem := CreateItem;
    LoadFields(wkItem, ADataSet.Fields);
    Self.Add(wkItem);
    ADataSet.Next;
  end;

  if fSorted then
  begin
    Sorted := false;
    Sorted := True;
  end;

{$IFDEF MSWINDOWS}
  fLoadTime := fLoadTime + ((timeGetTime - startTime) * 0.001);
{$ENDIF MSWINDOWS}
  fLoaded := True;
  if Assigned(fOnLoaded) then
    fOnLoaded(Self);
end;

procedure TStormBaseClassDbList<T>.LoadFields(var inObj: T; Fields: TFields);
var
  wkProp: TRttiProperty;
  wkField: TField;
  wkTv: TValue;
  wkFieldName: string;
  propIx: integer;
  fieldsIx: integer;
  wkProperties: TArray<TRttiProperty>;
begin
  if CustomPreLoadFieldsProcessing(inObj, Fields) then
  begin
    wkProperties := fRttiType.GetProperties;
    for propIx := 0 to length(wkProperties) - 1 do
    begin
      wkProp := wkProperties[propIx];
      if fPropertyToFieldMatches.TryGetValue(wkProp.Name.ToUpper, wkField) then
      begin
        case wkProp.PropertyType.TypeKind of
          TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
            TTypeKind.tkUString:
            wkProp.SetValue(TStormBaseClass(inObj), wkField.AsString);

          TTypeKind.tkWString, TTypeKind.tkWChar:
            wkProp.SetValue(TStormBaseClass(inObj), wkField.AsWideString);

          TTypeKind.tkInteger, TTypeKind.tkInt64:
            wkProp.SetValue(TStormBaseClass(inObj), wkField.AsInteger);

          TTypeKind.tkFloat:
            wkProp.SetValue(TStormBaseClass(inObj), wkField.AsFloat);
          // The following works for Boolean and enumerated fields!
          TTypeKind.tkEnumeration:
            begin
              if (wkField.DataType = TFieldType.ftBoolean) then
                wkProp.SetValue(TStormBaseClass(inObj), wkField.AsBoolean)
              else
              begin
                wkTv := wkProp.GetValue(TStormBaseClass(inObj));
                wkTv := TValue.FromOrdinal(wkTv.TypeInfo, wkField.AsInteger);
                wkProp.SetValue(TStormBaseClass(inObj), wkTv);
              end;
            end;
        end;
      end;
    end;
  end;
  CustomPostLoadFieldsProcessing(inObj, Fields);
end;

procedure TStormBaseClassDbList<T>.LoadSelectedItems(const AdditionalSql
  : string; const Fdc: TFDConnection);
var
  wkItem: T;
  fFdq: TFDQuery;
  startTime: Cardinal;
  wkExistingItem: T;
  insertIx: integer;
begin
  if (fLoadSql = '') or (not Assigned(Fdc)) or (fTable = '') then
    Exit;

{$IFDEF MSWINDOWS}
  startTime := timeGetTime;
{$ENDIF MSWINDOWS}
  GetFieldNames(Fdc);
  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    fFdq.SQL.Text := fLoadSql + ' ' + AdditionalSql;
    fFdq.Active := True;
    SetPropertyToFieldMatches(fFdq.Fields);

    while not fFdq.Eof do
    begin
      wkItem := CreateItem;
      LoadFields(wkItem, fFdq.Fields);

      if fSorted then
      begin
        if fHasIntId then
          wkExistingItem := PrivBinarySearch(wkItem.GetIdInt, insertIx)
        else
          wkExistingItem := PrivBinarySearch(wkItem.GetId, insertIx);
        if Not Assigned(wkExistingItem) then
          Self.Insert(insertIx, wkItem)
        else
          // Unlink from any related object/lists
          wkItem.Free;
      end
      else
      begin
        wkExistingItem := Self.GetById(wkItem.GetId);
        if Not Assigned(wkExistingItem) then
          Add(wkItem)
        else
          // Unlink from any related object/lists
          wkItem.Free;
      end;

      fFdq.Next;
    end;
    fFdq.Active := false;

{$IFDEF MSWINDOWS}
    fLoadTime := fLoadTime + ((timeGetTime - startTime) * 0.001);
{$ENDIF MSWINDOWS}
    fLoaded := True;
  finally
    fFdq.Free;
  end;
end;

procedure TStormBaseClassDbList<T>.LoadSelectedItems
  (const AdditionalSql: string);
begin
  LoadSelectedItems(AdditionalSql, fDfltConnection);
end;

procedure TStormBaseClassDbList<T>.OutputChanges(inObj: T; keyValue: string;
  Fdc: TFDConnection);
var
  fFdq: TFDQuery;
  wkSql: string;
begin
  if (not Assigned(Fdc)) or (fTable = '') or (Not fLogChanges) then
    Exit;

  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    wkSql := 'Select count(*) from LclChanges where LclTableNameId =' +
      IntToStr(GetTableId(Fdc)) + ' And RecordId = ''' + keyValue + '''';
    fFdq.SQL.Text := wkSql;
    fFdq.Active := True;

    if (fFdq.Eof) or (fFdq.Fields[0].AsInteger = 0) then
    begin
      fFdq.Active := false;
      wkSql := 'Insert into LclChanges (LclChangeId, LclTableNameId, ' +
        'RecordId, DeleteRecd, OriginalData) Values (''' + StOrmGetRandomGuid +
        ''', ' + IntToStr(GetTableId(Fdc)) + ', ''' + keyValue + ''', '' '', ' +
        StOrmPrepSqlString(inObj.GetJsonString) + ')';
      fFdq.SQL.Text := wkSql;
      fFdq.ExecSQL;
    end;
  finally;
    fFdq.Free;
  end;
end;

procedure TStormBaseClassDbList<T>.SetItemFields(Item: T);
var
  wkFn: string;
begin
  for wkFn in fFieldNames do
  begin
    if Not Item.FieldList.ContainsKey(wkFn) then
      Item.FieldList.Add(wkFn, nil);
  end;

end;

procedure TStormBaseClassDbList<T>.SetPropertyToFieldMatches(Fields: TFields);
var
  wkProperties: TArray<TRttiProperty>;
  wkProp: TRttiProperty;
  wkField: TField;
  wkFieldName: string;
  propIx: integer;
  fieldsIx: integer;
begin
  fPropertyToFieldMatches.Clear;
  wkProperties := fRttiType.GetProperties;
  for propIx := 0 to length(wkProperties) - 1 do
  begin
    wkProp := wkProperties[propIx];
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      wkFieldName := StOrmGetAliasOf(wkProp);
      for fieldsIx := 0 to Fields.Count - 1 do
      begin
        wkField := Fields[fieldsIx];
        if (CompareText(wkField.fieldName, wkFieldName) = 0) then
        begin
          fPropertyToFieldMatches.Add(wkProp.Name.ToUpper, wkField);
          Break;
        end;
      end;
    end;
  end;
end;

procedure TStormBaseClassDbList<T>.UpdateDb(Item: T);
begin
  UpdateDb(Item, fDfltConnection);
end;

procedure TStormBaseClassDbList<T>.UpdateDb(Item: T; Fdc: TFDConnection);
var
  fFdq: TFDQuery;
  wkSql: string;
  wkProp: TRttiProperty;
  wkField: TField;
  wkParam: TFDParam;
  wkChar: string;
  wkKey: string;
  wkAnd: string;
  keyValue: string;
  wkTv: TValue;
  delimiter: string;
  isNumeric: Boolean;
  wkFieldName: string;
  wkLookupName: string;
begin
  if (not Assigned(Fdc)) or (fTable = '') then
    Exit;

  Item.LclLastUpdated := TStormOptionsClass.GetNow;
  GetFieldNames(Fdc);

  fFdq := TFDQuery.Create(nil);
  try
    fFdq.Connection := Fdc;
    wkSql := 'Update ' + fTable + ' Set ';
    wkChar := '';

    // Create update SQL. We can only update the fields that are common to the
    // database and the object!  (To handle different versions of app and
    // host server.)
    // wkRttiType := Context.GetType(T.ClassInfo);
    for wkProp in fRttiType.GetProperties do
    begin
      if (wkProp.Visibility in [TMemberVisibility.mvPublished,
        TMemberVisibility.mvPublic]) and (wkProp.IsWritable) and
        (wkProp.IsReadable) then
      begin
        wkFieldName := StOrmGetAliasOf(wkProp);
        // Reserved words may need quoting!
        // SqLite needs "row" in double quotes...
        if (wkFieldName.ToUpper = 'ROW') then
          wkLookupName := '"' + wkFieldName + '"'
        else
          wkLookupName := wkFieldName;

        if (fKeyFieldNames.IndexOf(wkFieldName) = -1) and
          (fFieldNames.IndexOf(wkLookupName) > -1) then
        begin
          wkSql := wkSql + wkChar + wkFieldName + '= :' + wkFieldName;
          wkChar := ', ';
        end;

        if (fKeyFieldNames.IndexOf(wkFieldName) = 0) then
        begin
          assert((fKeyFieldNames.Count = 1),
            'Multi-Keyed table updates not supported!');
          case wkProp.PropertyType.TypeKind of
            TTypeKind.tkInteger, TTypeKind.tkInt64:
              keyValue := IntToStr(wkProp.GetValue(TStormBaseClass(Item))
                .AsInteger);
          else
            keyValue := wkProp.GetValue(TStormBaseClass(Item)).AsString;
          end;
        end;

        wkParam := fFdq.Params.Add;
        wkParam.Name := wkFieldName;
        case wkProp.PropertyType.TypeKind of
          TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
            TTypeKind.tkUString:
            wkParam.AsString := wkProp.GetValue(TStormBaseClass(Item)).AsString;
          TTypeKind.tkWString, TTypeKind.tkWChar:
            wkParam.AsWideString := wkProp.GetValue(TStormBaseClass(Item)
              ).AsString;

          TTypeKind.tkInteger, TTypeKind.tkInt64:
            wkParam.AsInteger := wkProp.GetValue(TStormBaseClass(Item))
              .AsInteger;

          TTypeKind.tkFloat:
            wkParam.AsFloat := wkProp.GetValue(TStormBaseClass(Item))
              .AsExtended;
          // The following works for Boolean fields but won't cut the
          // mustard for real enumarated fields!
          TTypeKind.tkEnumeration:
            begin
              if wkProp.PropertyType.Name = 'Boolean' then
                wkParam.AsBoolean := wkProp.GetValue(TStormBaseClass(Item))
                  .AsBoolean
              else
              begin
                // wkTv := wkProp.GetValue(TStormBaseClass(Item));
                // wkParam.AsInteger := wkTv.AsOrdinal;
                wkParam.AsInteger := wkProp.GetValue(TStormBaseClass(Item))
                  .AsOrdinal;
              end;
            end;
        end;
      end;
    end;

    wkSql := wkSql + ' Where ';
    wkAnd := '';
    for wkKey in fKeyFieldNames do
    begin
      wkSql := wkSql + wkAnd + wkKey + '=:' + wkKey;
      wkAnd := ' And ';
    end;

    fFdq.SQL.Text := wkSql;
    fFdq.ExecSQL;

    // Now create changes record
    OutputChanges(Item, keyValue, Fdc);
  finally
    fFdq.Free;
  end;
end;

{ TAppBaseClass }

procedure TStormBaseClass.AppendJson(Writer: TJSonObjectWriter;
  StormJsonOptions: TStormJsonOptions);
var
  wkProp: TRttiProperty;
  Context: TRTTIContext;
  wkFieldName: string;

begin
  Writer.WriteStartObject;
  fRttiType := Context.GetType(Self.ClassInfo);
  for wkProp in fRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      wkFieldName := StOrmGetAliasOf(wkProp);
      case wkProp.PropertyType.TypeKind of
        TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
          TTypeKind.tkUString:
          begin
            Writer.WritePropertyName(wkFieldName);
            Writer.WriteValue(wkProp.GetValue(TStormBaseClass(Self)).AsString);
          end;

        // TTypeKind.tkWString, TTypeKind.tkWChar:
        // wkProp.SetValue(TAppBaseClass(inObj), wkField.AsWideString);

        TTypeKind.tkInteger:
          begin
            Writer.WritePropertyName(wkFieldName);
            Writer.WriteValue(wkProp.GetValue(TStormBaseClass(Self)).AsInteger);
          end;

        TTypeKind.tkInt64:
          begin
            Writer.WritePropertyName(wkFieldName);
            Writer.WriteValue(wkProp.GetValue(TStormBaseClass(Self)).AsInt64);
          end;

        TTypeKind.tkFloat:
          begin
            Writer.WritePropertyName(wkFieldName);

            if (sjoIsoDateFormat in StormJsonOptions) and
              (wkProp.PropertyType.Name = 'TDateTime') then
              Writer.WriteValue(FormatDateTime('yyyy-mm-dd"T"hh:nn:ss',
                wkProp.GetValue(TStormBaseClass(Self)).AsExtended))
            else
              Writer.WriteValue(wkProp.GetValue(TStormBaseClass(Self))
                .AsExtended);
          end;

        // The following works for Boolean and real enumerated fields!
        TTypeKind.tkEnumeration:
          begin
            Writer.WritePropertyName(wkFieldName);
            if wkProp.PropertyType.Name = 'Boolean' then
            begin
              if (wkProp.GetValue(Self).AsBoolean) then
                Writer.WriteValue(1)
              else
                Writer.WriteValue(0);
            end
            else
            begin
              Writer.WriteValue(wkProp.GetValue(TStormBaseClass(Self))
                .AsOrdinal);
            end;
          end;

      end;
    end;
  end;
  Writer.WriteEndObject;
end;

procedure TStormBaseClass.AppendJsonArray(Writer: TJSonObjectWriter);
var
  wkProp: TRttiProperty;
  wkStrWriter: TStringWriter;
  wkBuffer: TCharArray;
  ws: string;
begin
  Writer.WriteStartArray;

  for wkProp in fRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      case wkProp.PropertyType.TypeKind of
        TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
          TTypeKind.tkUString:
          begin
            ws := wkProp.GetValue(Self).AsString;
            wkStrWriter := TStringWriter.Create;
            try
              StOrmWriteEscapedString(wkStrWriter, ws, '"', false, [],
                TJsonStringEscapeHandling.EscapeNonAscii, wkBuffer);
              ws := wkStrWriter.ToString;
              Writer.WriteValue(ws);
            finally
              wkStrWriter.Free;
            end;
          end;

        // TTypeKind.tkWString, TTypeKind.tkWChar:
        // wkProp.SetValue(TAppBaseClass(inObj), wkField.AsWideString);

        TTypeKind.tkInteger:
          begin
            Writer.WriteValue(wkProp.GetValue(Self).AsInteger);
          end;

        TTypeKind.tkInt64:
          begin
            Writer.WriteValue(wkProp.GetValue(Self).AsInt64);
          end;

        TTypeKind.tkFloat:
          begin
            Writer.WriteValue(wkProp.GetValue(Self).AsExtended);
          end;

        // The following works for Boolean and real enumerated fields!
        TTypeKind.tkEnumeration:
          begin
            if wkProp.PropertyType.Name = 'Boolean' then
            begin
              if (wkProp.GetValue(Self).AsBoolean) then
                Writer.WriteValue(1)
              else
                Writer.WriteValue(0);
            end
            else
            begin
              Writer.WriteValue(wkProp.GetValue(TStormBaseClass(Self))
                .AsOrdinal);
            end;
          end;

      end;
    end;
  end;

  Writer.WriteEndArray;
end;

class procedure TStormBaseClass.AppendJsonArrayColumns
  (Writer: TJSonObjectWriter);
var
  Context: TRTTIContext;
  wkRttiType: TRttiType;
  wkProp: TRttiProperty;
  wkFieldName: string;
begin
  Writer.WriteStartArray;
  wkRttiType := Context.GetType(Self.ClassInfo);
  for wkProp in wkRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      wkFieldName := StOrmGetAliasOf(wkProp);
      // I have left this in the same format/layout as AppendJsonArray
      // to make future maintenance easier.
      case wkProp.PropertyType.TypeKind of
        TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
          TTypeKind.tkUString:
          begin
            Writer.WriteValue(wkFieldName);
          end;

        // TTypeKind.tkWString, TTypeKind.tkWChar:
        // wkProp.SetValue(TAppBaseClass(inObj), wkField.AsWideString);

        TTypeKind.tkInteger:
          begin
            Writer.WriteValue(wkFieldName);
          end;

        TTypeKind.tkInt64:
          begin
            Writer.WriteValue(wkFieldName);
          end;

        TTypeKind.tkFloat:
          begin
            Writer.WriteValue(wkFieldName);
          end;

        // The following works for Boolean fand real enumerated fields!
        TTypeKind.tkEnumeration:
          begin
            Writer.WriteValue(wkFieldName);
          end;

      end;
    end;
  end;
  Writer.WriteEndArray;
end;

procedure TStormBaseClass.Assign(Source: TStormBaseClass);
var
  lSourceRtti: TRttiProperty;
  lSourceFieldName: string;
  lMyRtti: TRttiProperty;
begin
  for lSourceFieldName in Source.FieldList.Keys do
  begin
    lSourceRtti := Source.FieldList.Items[lSourceFieldName];
    if Assigned(lSourceRtti) and
      (fFieldList.TryGetValue(lSourceFieldName, lMyRtti)) and Assigned(lMyRtti)
      and (lMyRtti.PropertyType.TypeKind = lSourceRtti.PropertyType.TypeKind)
    then
    begin
      lMyRtti.SetValue(Self, lSourceRtti.GetValue(Source));
    end;
  end;
end;

constructor TStormBaseClass.Create;
begin
  inherited;
  OnCreate;
end;

constructor TStormBaseClass.CreateFromJSON(JSONObject: System.JSON.TJSONObject);
var
  wkProp: TRttiProperty;
  wkTv: TValue;
  pair: System.JSON.TJSONPair;
  wkInt: integer;
  wkStr: string;
  wkDouble: double;
  wkBoolean: Boolean;
  wkDate: TDate;
  wkDateTime: TDateTime;
  wkTime: TTime;
begin
  Create;
  for wkProp in fRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      for pair in JSONObject do
      begin
        if CompareText(pair.JsonString.Value, wkProp.Name) = 0 then
        begin
          case wkProp.PropertyType.TypeKind of
            TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
              TTypeKind.tkUString, TTypeKind.tkWString, TTypeKind.tkWChar:
              if pair.JsonValue.TryGetValue(wkStr) then
                wkProp.SetValue(Self, wkStr);

            TTypeKind.tkInteger, TTypeKind.tkInt64:
              if pair.JsonValue.TryGetValue(wkInt) then
                wkProp.SetValue(Self, wkInt);

            TTypeKind.tkFloat:
              try
                if pair.JsonValue.TryGetValue(wkDouble) then
                  wkProp.SetValue(Self, wkDouble)
                else
                begin
                  if (wkProp.PropertyType.Name = 'TDate') then
                  begin
                    if pair.JsonValue.TryGetValue(wkDate) then
                      // Date is in ISO yyy-mm-ddThh:mm:ss format
                      wkProp.SetValue(Self, wkDate)
                    else
                    begin
                      if (pair.JsonValue.TryGetValue(wkStr)) and
                        (TryStrToDate(wkStr, wkDateTime)) then
                        wkProp.SetValue(Self, wkDateTime);
                    end;
                  end
                  else if (wkProp.PropertyType.Name = 'TDateTime') then
                  begin
                    if pair.JsonValue.TryGetValue(wkDateTime) then
                      wkProp.SetValue(Self, wkDateTime);
                  end
                  else if (wkProp.PropertyType.Name = 'TTime') then
                  begin
                    if pair.JsonValue.TryGetValue(wkTime) then
                      // Time is in ISO yyy-mm-ddThh:mm:ss format
                      wkProp.SetValue(Self, wkTime)
                    else
                    begin
                      if (pair.JsonValue.TryGetValue(wkStr)) and
                        (TryStrToTime(wkStr, wkDateTime)) then
                        wkProp.SetValue(Self, wkDateTime);
                    end;
                  end;
                end;
              except
                on E: Exception do
                  wkProp.SetValue(Self, 0);
              end;

            // The following works for Boolean and real enumerated fields!
            TTypeKind.tkEnumeration:
              if wkProp.PropertyType.Name = 'Boolean' then
              begin
                if pair.JsonValue.TryGetValue(wkBoolean) then
                  wkProp.SetValue(Self, wkBoolean);
              end
              else
              begin
                if pair.JsonValue.TryGetValue(wkInt) then
                begin
                  wkTv := wkProp.GetValue(Self);
                  wkTv := TValue.FromOrdinal(wkTv.TypeInfo, wkInt);
                  wkProp.SetValue(TStormBaseClass(Self), wkTv);
                end;
              end;
          end;
          Break;
        end;
      end;
    end;
  end;
end;

constructor TStormBaseClass.CreateFromSeperated(Seperated: string);
var
  wkProp: TRttiProperty;
  Values: TArray<String>;
  ix: integer;
  wkTv: TValue;

begin
  // inherited Create here?
  OnCreate;
  Values := Seperated.Split([stOrmSeperator]);
  ix := 0;
  for wkProp in fRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      case wkProp.PropertyType.TypeKind of
        TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
          TTypeKind.tkUString:
          wkProp.SetValue(Self, Values[ix]);

        TTypeKind.tkWString, TTypeKind.tkWChar:
          wkProp.SetValue(Self, Values[ix]);

        TTypeKind.tkInteger, TTypeKind.tkInt64:
          wkProp.SetValue(Self, StrToInt(Values[ix]));

        TTypeKind.tkFloat:
          wkProp.SetValue(Self, StrToFloat(Values[ix]));
        // The following works for Boolean and real enumerated fields!
        TTypeKind.tkEnumeration:
          if wkProp.PropertyType.Name = 'Boolean' then
          begin
            wkProp.SetValue(Self, Values[ix] = '1');
          end
          else
          begin
            wkTv := wkProp.GetValue(Self);
            wkTv := TValue.FromOrdinal(wkTv.TypeInfo, StrToInt(Values[ix]));
            wkProp.SetValue(TStormBaseClass(Self), wkTv);
          end;

      end;
      inc(ix);
      if ix >= length(Values) then
        Break;
    end;
  end;
end;

destructor TStormBaseClass.Destroy;
begin
  fFieldList.Free;
  inherited;
end;

function TStormBaseClass.GetJsonArray: string;
var
  wkJsonWriter: TJSonObjectWriter;
begin
  wkJsonWriter := TJSonObjectWriter.Create(True);
  try
    Self.AppendJsonArray(wkJsonWriter);
  finally
    Result := (wkJsonWriter.JSON).ToString;
    wkJsonWriter.Free;
  end;
end;

function TStormBaseClass.GetJsonString(StormJsonOptions
  : TStormJsonOptions): string;
var
  js: System.JSON.TJSONObject;
begin
  Result := '';
  js := Self.GetJson(StormJsonOptions);
  try
    Result := js.ToString;
  finally
    js.Free;
  end;
end;

procedure TStormBaseClass.OnCreate;
var
  Context: TRTTIContext;
  wkProp: TRttiProperty;
begin
  fLclLastUpdated := TStormOptionsClass.GetNow;
  fLclCreated := fLclLastUpdated;

  fRttiType := Context.GetType(Self.ClassInfo);
  if not Assigned(fFieldList) then
    fFieldList := TStormFieldList.Create
  else
    fFieldList.Clear;

  for wkProp in fRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      fFieldList.Add(wkProp.Name.ToUpper, wkProp);
    end;
  end;
end;

procedure TStormBaseClass.ResetLinkedObjects;
begin
  //
end;

function TStormBaseClass.SetFieldValueByName(inFieldName: string;
  inValue: double): Boolean;
var
  wkProp: TRttiProperty;
begin
  Result := false;
  for wkProp in fRttiType.GetProperties do
    if (CompareText(inFieldName, wkProp.Name) = 0) then
    begin
      if (wkProp.Visibility in [TMemberVisibility.mvPublished,
        TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
      begin
        case wkProp.PropertyType.TypeKind of
          TTypeKind.tkFloat:
            begin
              Result := True;
              wkProp.SetValue(Self, inValue);
              Break;
            end;
        end;
      end;
    end;
end;

function TStormBaseClass.SetFieldValueByName(inFieldName: string;
  inValue: integer): Boolean;
begin
  Result := SetFieldValueByName(inFieldName, IntToStr(inValue));
end;

function TStormBaseClass.SetFieldValueByName(inFieldName: string;
  inValue: Int64): Boolean;
begin
  Result := SetFieldValueByName(inFieldName, IntToStr(inValue));
end;

function TStormBaseClass.SetFieldValueByName(inFieldName: string;
  inValue: Boolean): Boolean;
begin
  if inValue then
    SetFieldValueByName(inFieldName, '1')
  else
    SetFieldValueByName(inFieldName, '0');
  Result := True;
end;

function TStormBaseClass.SetFieldValueByName(inFieldName,
  inValue: string): Boolean;
var
  wkProp: TRttiProperty;
  wkInt: integer;
  wkFloat: double;
  wkTv: TValue;
begin
  Result := false;
  for wkProp in fRttiType.GetProperties do
    if (CompareText(inFieldName, wkProp.Name) = 0) then
    begin
      if (wkProp.Visibility in [TMemberVisibility.mvPublished,
        TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
      begin
        case wkProp.PropertyType.TypeKind of
          TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
            TTypeKind.tkUString:
            begin
              wkProp.SetValue(Self, inValue);
              Result := True;
              Break;
            end;

          TTypeKind.tkWString, TTypeKind.tkWChar:
            begin
{$IFDEF MSWINDOWS}
              wkProp.SetValue(Self, WideString(inValue));
{$ELSE}
              wkProp.SetValue(Self, String(inValue));
{$ENDIF}
              Result := True;
              Break;
            end;

          TTypeKind.tkInteger, TTypeKind.tkInt64:
            begin
              Result := TryStrToInt(inValue, wkInt);
              if Result then
                wkProp.SetValue(Self, wkInt);
              Break;
            end;

          TTypeKind.tkFloat:
            begin
              Result := TryStrToFloat(inValue, wkFloat);
              if Result then
                wkProp.SetValue(Self, wkFloat);
              Break;
            end;

          // The following works for Boolean and real enumerated fields!
          TTypeKind.tkEnumeration:
            begin
              Result := TryStrToInt(inValue, wkInt);
              if Result then
                if wkProp.PropertyType.Name = 'Boolean' then
                  wkProp.SetValue(Self, wkInt <> 0)
                else
                begin
                  wkTv := wkProp.GetValue(Self);
                  wkTv := TValue.FromOrdinal(wkTv.TypeInfo, wkInt);
                  wkProp.SetValue(Self, wkTv);
                end;

              Break;
            end;
        end;
      end;
    end;
end;

function TStormBaseClass.ToSeperatedValue: string;
var
  wkProp: TRttiProperty;
  wkSeperator: string;
begin
  Result := '';
  wkSeperator := '';
  for wkProp in fRttiType.GetProperties do
  begin
    if (wkProp.Visibility in [TMemberVisibility.mvPublished,
      TMemberVisibility.mvPublic]) and (wkProp.IsWritable) then
    begin
      case wkProp.PropertyType.TypeKind of
        TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
          TTypeKind.tkUString:
          begin
            // Result.AddPair(TJSONPair.Create(wkProp.Name,
            // wkProp.GetValue(TAppBaseClass(Self)).AsString));
            Result := Result + wkSeperator +
              wkProp.GetValue(TStormBaseClass(Self)).AsString;
          end;

        // TTypeKind.tkWString, TTypeKind.tkWChar:
        // wkProp.SetValue(TAppBaseClass(inObj), wkField.AsWideString);

        TTypeKind.tkInteger:
          begin
            // Result.AddPair(TJSONPair.Create(wkProp.Name,
            // TJSONNumber.Create(wkProp.GetValue(TAppBaseClass(Self))
            // .AsInteger)));
            Result := Result + wkSeperator +
              IntToStr(wkProp.GetValue(TStormBaseClass(Self)).AsInteger);
          end;

        TTypeKind.tkInt64:
          begin
            // Result.AddPair(TJSONPair.Create(wkProp.Name,
            // TJSONNumber.Create(wkProp.GetValue(TAppBaseClass(Self))
            // .AsInt64)));
            Result := Result + wkSeperator +
              IntToStr(wkProp.GetValue(TStormBaseClass(Self)).AsInt64);
          end;

        TTypeKind.tkFloat:
          begin
            // Result.AddPair(TJSONPair.Create(wkProp.Name,
            // TJSONNumber.Create(wkProp.GetValue(TAppBaseClass(Self))
            // .AsExtended)));
            Result := Format('%s%s%g', [Result, wkSeperator,
              wkProp.GetValue(TStormBaseClass(Self)).AsExtended]);
          end;

        // The following works for Boolean fields but won't cut the
        // mustard for real enumerated fields!
        TTypeKind.tkEnumeration:
          begin
            // Result.AddPair(TJSONPair.Create(wkProp.Name,
            // TJSONNumber.Create(wkProp.GetValue(TAppBaseClass(Self))
            // .AsVariant)));
            if (wkProp.GetValue(TStormBaseClass(Self)).AsBoolean) then
              Result := Result + wkSeperator + '1'
            else
              Result := Result + wkSeperator + '0';
          end;
      end;
      wkSeperator := stOrmSeperator;
    end;
  end;
end;

function TStormBaseClass.GetId: string;
begin
  Result := '';
end;

function TStormBaseClass.GetIdInt: Int64;
begin
  Result := 0;
end;

function TStormBaseClass.GetJson(StormJsonOptions: TStormJsonOptions)
  : System.JSON.TJSONObject;
var
  wkJsonWriter: TJSonObjectWriter;
begin
  wkJsonWriter := TJSonObjectWriter.Create(True);
  try
    AppendJson(wkJsonWriter, StormJsonOptions);
    Result := (wkJsonWriter.JSON).clone as System.JSON.TJSONObject;
  finally
    FreeAndNil(wkJsonWriter);
  end;
end;

{ TStormPropertiesList<T> }

procedure TStormPropertiesList<T>.RegisterProperty(inT: T);
begin
  if IndexOf(inT) = -1 then
    Self.Add(inT);
end;

{ TStormBaseList<T> }

constructor TStormBaseClassList<T>.Create(ListName: string;
  AOwnsObjects: Boolean);
begin
  inherited Create(AOwnsObjects);
  fListName := ListName;
  fRttiType := fContext.GetType(T.ClassInfo);
  fSorted := false;
  fHasIntId := false;
end;

procedure TStormBaseClassList<T>.AddItemsFromJsonArray
  (inJSONArray: System.JSON.TJSONArray);
var
  wkT: T;
  wkValue: System.JSON.TJSONValue;
begin
  for wkValue in inJSONArray do
  begin
    if (wkValue is System.JSON.TJSONObject) then
    begin
      wkT := T.CreateFromJSON(System.JSON.TJSONObject(wkValue));
      Self.PrivAdd(wkT);
    end;
  end;
end;

procedure TStormBaseClassList<T>.AddItemsFromJsonArray(inJSONString: string);
var
  wkMbr: IMember;
  wkTble: ISuperObject;
  wkColumns: ISuperObject;
  wkStructure: ISuperArray;
  wkData: ISuperObject;
  wkRecords: ISuperArray;
  wkRecord: ISuperArray;
  ix: integer;
  iy: integer;
  wkFields: TStringList;
  wkItem: T;
  wkVar: TVarType;
  wkInt: integer;
begin
  wkTble := TSuperObject.Create(inJSONString);
  wkFields := TStringList.Create;
  try
    // This gets column names
    wkColumns := wkTble.O[wkTble.CurrentKey];
    // wl.Add(wkColumns.CurrentKey);   // 'colums'
    wkStructure := wkColumns.A[wkColumns.CurrentKey];
    for ix := 0 to wkStructure.length - 1 do
    begin
      wkFields.Add(wkStructure.S[ix]);
    end;

    // This gets records
    wkColumns.Next;
    wkRecords := wkColumns.A[wkColumns.CurrentKey];

    for ix := 0 to wkRecords.length - 1 do
    begin
      wkRecord := wkRecords.A[ix];
      wkItem := T.Create;
      // wl.Add(Format('%d %s', [wkRecord.I[0], wkRecord.S[2]]));
      for iy := 0 to wkFields.Count - 1 do
      begin
        wkVar := wkRecord.GetType(iy);
        case wkRecord.GetType(iy) of
          varString, varUString:
            wkItem.SetFieldValueByName(wkFields[iy], wkRecord.S[iy]);
          varSmallInt, varShortInt, varByte, varWord, varLongWord, varInt64,
            varUInt64:
            begin
              wkItem.SetFieldValueByName(wkFields[iy], wkRecord.I[iy]);
              wkInt := wkRecord.I[iy];
            end;
          varBoolean:
            wkItem.SetFieldValueByName(wkFields[iy], wkRecord.B[iy]);
          varSingle, varDouble:
            wkItem.SetFieldValueByName(wkFields[iy], wkRecord.F[iy]);
        end;
      end;
      Self.PrivAdd(wkItem);
    end;

  finally
    wkFields.Free;
  end;
end;

function TStormBaseClassList<T>.CreateItem: T;
begin
  Result := T.Create;
end;

destructor TStormBaseClassList<T>.Destroy;
begin
  Self.Clear;
  inherited;
end;

function TStormBaseClassList<T>.GetById(Id: string): T;
var
  ix: integer;
  wkId: string;
  wkItem: T;
begin
  if fSorted then
    Result := PrivBinarySearch(Id, ix)
  else
  begin
    for ix := 0 to Count - 1 do
    begin
      wkId := Items[ix].GetId;
      wkItem := Items[ix];
      if Items[ix].GetId = Id then
        Exit(Items[ix]);
    end;
    Result := nil;
  end;
end;

function TStormBaseClassList<T>.GetAsJSONString: string;
var
  wkJSON: System.JSON.TJSONObject;
begin
  wkJSON := Self.GetJson;
  Result := wkJSON.ToString;
  wkJSON.Free;
end;

function TStormBaseClassList<T>.GetById(Id: integer): T;
var
  ix: integer;
begin
  if fSorted then
    Result := PrivBinarySearch(Id, ix)
  else
  begin
    for ix := 0 to Count - 1 do
    begin
      if Items[ix].GetIdInt = Id then
        Exit(Items[ix]);
    end;
    Result := nil;
  end;
end;

function TStormBaseClassList<T>.GetJson: System.JSON.TJSONObject;
var
  wkJsonWriter: TJSonObjectWriter;
  wkItem: T;
begin
  wkJsonWriter := TJSonObjectWriter.Create(false);
  try
    wkJsonWriter.WriteStartObject;
    wkJsonWriter.WritePropertyName(fListName);
    wkJsonWriter.WriteStartArray;

    for wkItem in Self do
    begin
      wkItem.AppendJson(wkJsonWriter);
    end;

  finally
    wkJsonWriter.WriteEndArray;
    wkJsonWriter.WriteEndObject;
    Result := wkJsonWriter.JSON As System.JSON.TJSONObject;
    wkJsonWriter.Free;
  end;
end;

function TStormBaseClassList<T>.GetJsonArray(fromIndex: integer;
  toIndex: integer): System.JSON.TJSONObject;
var
  wkJsonWriter: TJSonObjectWriter;
  wkItem: T;
  ix: integer;
begin
  if (toIndex < 0) then
    toIndex := Self.Count - 1;
  if (fromIndex > Self.Count - 1) then
    fromIndex := Self.Count - 1;
  if (fromIndex > toIndex) then
    toIndex := fromIndex;
  if (toIndex > Self.Count - 1) then
    toIndex := Self.Count - 1;

  wkJsonWriter := TJSonObjectWriter.Create(True);
  try
    wkJsonWriter.WriteStartObject;
    wkJsonWriter.WritePropertyName(fListName);

    wkJsonWriter.WriteStartObject;
    wkJsonWriter.WritePropertyName('colums');
    T.AppendJsonArrayColumns(wkJsonWriter);

    wkJsonWriter.WritePropertyName('data');
    wkJsonWriter.WriteStartArray;

    if (fromIndex >= 0) then
      for ix := fromIndex to toIndex do
      begin
        wkItem := Self[ix];
        wkItem.AppendJsonArray(wkJsonWriter);
      end;

  finally
    wkJsonWriter.WriteEndArray;
    wkJsonWriter.WriteEndObject;
    Result := wkJsonWriter.JSON.clone As System.JSON.TJSONObject;
    wkJsonWriter.Free;
  end;
end;

function TStormBaseClassList<T>.GetJsonArrayString(fromIndex: integer;
  toIndex: integer): string;
var
  jo: System.JSON.TJSONObject;
begin
  jo := GetJsonArray(fromIndex, toIndex);
  try
    Result := jo.ToString;
  finally
    jo.Free;
  end;
end;

function TStormBaseClassList<T>.PrivBinarySearch(Id: string;
  out FoundIndex: integer): T;
var
  lowIx: integer;
  highIx: integer;
  midPointIx: integer;
  compareResult: integer;
begin
  FoundIndex := 0;
  Result := nil;
  if (Self.Count = 0) then
    Exit;
  lowIx := 0;
  highIx := Self.Count - 1;
  while lowIx <= highIx do
  begin
    midPointIx := lowIx + (highIx - lowIx) shr 1;
    compareResult := CompareStr(Self[midPointIx].GetId, Id);
    if compareResult < 0 then
      lowIx := midPointIx + 1
    else
    begin
      highIx := midPointIx - 1;
      if compareResult = 0 then
        Result := Self[midPointIx];
    end;
  end;
  FoundIndex := lowIx;
end;

procedure TStormBaseClassList<T>.PrivAdd(AItem: T);
var
  insertIx: integer;
begin
  if fSorted then
  begin
    if fHasIntId then
      PrivBinarySearch(AItem.GetIdInt, insertIx)
    else
      PrivBinarySearch(AItem.GetId, insertIx);
    Self.Insert(insertIx, AItem);
  end
  else
    Self.Add(AItem);
end;

function TStormBaseClassList<T>.PrivBinarySearch(Id: integer;
  out FoundIndex: integer): T;
var
  lowIx: integer;
  highIx: integer;
  midPointIx: integer;
  compareResult: integer;
begin
  FoundIndex := 0;
  Result := nil;
  if (Self.Count = 0) then
    Exit;

  lowIx := 0;
  highIx := Self.Count - 1;
  while lowIx <= highIx do
  begin
    midPointIx := lowIx + (highIx - lowIx) shr 1;
    compareResult := CompareValue(Self[midPointIx].GetIdInt, Id);
    if compareResult < 0 then
      lowIx := midPointIx + 1
    else
    begin
      highIx := midPointIx - 1;
      if compareResult = 0 then
        Result := Self[midPointIx];
    end;
  end;
  FoundIndex := lowIx;
end;

procedure TStormBaseClassList<T>.SetSorted(const Value: Boolean);
begin
  if (fSorted <> Value) then
  begin
    fSorted := Value;
    if fSorted then
      if fHasIntId then
        Self.Sort(TComparer<T>.Construct(
          function(const L, R: T): integer
          begin
            Result := CompareValue(L.GetIdInt, R.GetIdInt);
          end))
      else
        Self.Sort(TComparer<T>.Construct(
          function(const L, R: T): integer
          begin
            Result := CompareText(L.Id, R.Id);
          end));
  end;
end;

function TStormBaseClassList<T>.ToSeperatedValue: string;
var
  wkItem: T;
  wkBreak: string;
begin
  Result := '';
  wkBreak := '';
  for wkItem in Self do
  begin
    Result := Result + wkBreak + wkItem.ToSeperatedValue;
    wkBreak := #10;
  end;
end;

{ StOrmAliasOfAttribute }

constructor StOrmAliasOfAttribute.Create(AliasOf: string);
begin
  FString := AliasOf;
end;

{ TStOrmKey }

// function TStOrmKey.GetAsInteger: Integer;
// begin
// Result := FInteger;
// end;
//
// function TStOrmKey.GetAsString: string;
// begin
// Result := FString;
// end;
//
// class operator TStOrmKey.Implicit(const Value: Integer): TStOrmKey;
// begin
// Result.AsInteger := Value;
// end;
//
// class operator TStOrmKey.Implicit(const Value: string): TStOrmKey;
// begin
// Result.AsString := Value;
// end;
//
// class operator TStOrmKey.Implicit(const Value: TStOrmKey): Integer;
// begin
// Result := Value.AsInteger;
// end;
//
// class operator TStOrmKey.Implicit(const Value: TStOrmKey): string;
// begin
// Result := Value.AsString;
// end;
//
// procedure TStOrmKey.SetAsInteger(const Value: Integer);
// begin
// FInteger := Value;
// end;
//
// procedure TStOrmKey.SetAsString(const Value: string);
// begin
// FString := Value;
// end;
//

{ TStormOptionsClass }

class function TStormOptionsClass.GetNow: TDateTime;
begin
  if fUseUtcTime then
    Result := TTimeZone.Local.ToUniversalTime(now)
  else
    Result := now;
end;

initialization

TStormOptionsClass.UseUtcTime := false;

end.
