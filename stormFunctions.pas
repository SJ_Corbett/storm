unit stormFunctions;

interface

uses
  System.Classes, System.JSON.Types, System.SysUtils, System.Rtti;

/// <summary>Return the column (field) name in the database of a property
/// using attributes defined on the property.
/// </summary>
function StOrmGetAliasOf(AProperty: TRttiProperty): string;

function StOrmGetRandomGuid: string;

/// <summary>Replace all line feed and carriage return characters with \\n
/// </summary>
function StOrmPrepJsonString(const input: string): string;

/// <summary>Replace all single quotes with double quotes.  The result is
/// surrounded by single quotes.</summary>
function StormPrepSqlString(const input: string): string;

/// <summary>Replacement to class procedure TJsonTextUtils.WriteEscapedString
/// which does not escape regular characters</summary>
procedure StOrmWriteEscapedString(const Writer: TTextWriter; const Str: string;
  Delimiter: Char; AppendDelimiters: Boolean;
  const CharEscapeFlags: array of Boolean;
  StringEscapeHandling: TJsonStringEscapeHandling; var WriteBuffer: TCharArray);

const
  StOrmLf = #10;
  StOrmCr = #13;
  // StOrmCrLf = StOrmCr + StOrmLf;
  // StOrmLfCr = StOrmLf + StOrmCr;

implementation

uses
  System.JSON.Utils, stormBaseClasses;

function StOrmGetAliasOf(AProperty: TRttiProperty): string;
var
  wkAttributes: TArray<TCustomAttribute>;
  wkAttribute: TCustomAttribute;
  //ix: integer;
begin
  Result := AProperty.Name;
  wkAttributes := AProperty.GetAttributes;
  for wkAttribute in wkAttributes do
    if (wkAttribute is StOrmAliasOfAttribute) then
    begin
      Exit(StOrmAliasOfAttribute(wkAttribute).AliasOf);
    end;
end;

function StOrmGetRandomGuid: string;
var
  wkGuid: TGUID;
begin
  CreateGUID(wkGuid);
  Result := wkGuid.ToString.Substring(1, 36);
end;

function StOrmPrepJsonString(const input: string): string;

begin
  // Result := Result.Replace(StOrmLfCr, '\\n', [rfReplaceAll]);
  // Result := Result.Replace(StOrmCrLf, '\\n', [rfReplaceAll]);
  Result := input.Replace(StOrmCr, '\r', [rfReplaceAll]);
  Result := Result.Replace(StOrmLf, '\n', [rfReplaceAll]);
end;

function StormPrepSqlString(const input: string): string;
begin
  Result := input;
  Result := '''' + Result.Replace('''', '''''', [rfReplaceAll]) + '''';
end;

procedure StOrmWriteEscapedString(const Writer: TTextWriter; const Str: string;
  Delimiter: Char; AppendDelimiters: Boolean;
  const CharEscapeFlags: array of Boolean;
  StringEscapeHandling: TJsonStringEscapeHandling; var WriteBuffer: TCharArray);
var
  LastWritePosition, I, Start, Len: integer;
  EscapedValue: string;
  C: Char;
  IsEscapedUnicodeText: Boolean;

const
  EscapedUnicodeText = '!';
begin
  if AppendDelimiters then
    Writer.Write(Delimiter);

  LastWritePosition := 0;
  for I := 0 to Length(Str) - 1 do
  begin
    C := Str[I + Low(Str)];

    if (Ord(C) < Length(CharEscapeFlags)) and not CharEscapeFlags[Ord(C)] then
      Continue;

    case C of
      // tab
      #9:
        EscapedValue := '\t';
      // end line
      #10:
        EscapedValue := '\n';
      // carriage return
      #13:
        EscapedValue := '\r';
      // formfeed
      #12:
        EscapedValue := '\f';
      // backspace
      #8:
        EscapedValue := '\b';
      // backslash
      #92:
        EscapedValue := '\\';
      // new line
      #$0085:
        EscapedValue := '\u0085';
      // line separator
      #$2028:
        EscapedValue := '\u2028';
      // paragraph separator
      #$2029:
        EscapedValue := '\u2029';
    else
      begin
        if (((C >= 'A') And (C <= 'Z')) or (C = ' ') or (C = '.') or (C = ',')
          or (C = '[') or (C = ']') or (C = '{') or (C = '}') or (C = '-') or
          (C = '=') or (C = '(') or (C = ')') or (C = '&') or (C = '?') or
          (C = '`') or (C = '/') or ((C >= 'a') And (C <= 'z')) or
          ((C >= '0') And (C <= '9'))) or (C = '#') then
          EscapedValue := C
        else if (Ord(C) < Length(CharEscapeFlags)) or
          (StringEscapeHandling = TJsonStringEscapeHandling.EscapeNonAscii) then
        begin
          // I took out the ' and " handling since this caused us to get double
          // escaped in TStormBaseClass.AppendJsonArray
          if (C = '''') and
            (StringEscapeHandling <> TJsonStringEscapeHandling.EscapeHtml) then
            EscapedValue := '\'''
          else if (C = '"') and
            (StringEscapeHandling <> TJsonStringEscapeHandling.EscapeHtml) then
            // EscapedValue := '\"'
            EscapedValue := '\u0022'
          else
          begin
            if Length(WriteBuffer) = 0 then
              SetLength(WriteBuffer, 6);
            TJsonTextUtils.ToCharAsUnicode(C, WriteBuffer);
            EscapedValue := EscapedUnicodeText;
          end;
        end
        else
          EscapedValue := '';
      end;
    end; // case

    if EscapedValue = '' then
      Continue;

    IsEscapedUnicodeText := EscapedValue = EscapedUnicodeText;

    if (I > LastWritePosition) then
    begin
      Len := I - LastWritePosition;
      Start := 0;
      if IsEscapedUnicodeText then
      begin
        Inc(Len, 6);
        Inc(Start, 6);
      end;

      if (Length(WriteBuffer) = 0) or (Length(WriteBuffer) < Len) then
        SetLength(WriteBuffer, Len);

      Move(Str[Low(string) + LastWritePosition], WriteBuffer[Start],
        (Len - Start) * SizeOf(Char));
      Writer.Write(WriteBuffer, Start, Len - Start);
    end;
    LastWritePosition := I + 1;
    if not IsEscapedUnicodeText then
      Writer.Write(EscapedValue)
    else
      Writer.Write(WriteBuffer, 0, 6);
  end; // for

  if LastWritePosition = 0 then
    Writer.Write(Str)
  else
  begin
    Len := Length(Str) - LastWritePosition;
    if (Length(WriteBuffer) = 0) or (Length(WriteBuffer) < Len) then
      SetLength(WriteBuffer, Len);

    if Len > 0 then
    begin
      Move(Str[Low(string) + LastWritePosition], WriteBuffer[0],
        Len * SizeOf(Char));
      Writer.Write(WriteBuffer, 0, Len);
    end;
  end;

  if AppendDelimiters then
    Writer.Write(Delimiter);
end;

end.
