unit frmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, System.Generics.Collections,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.VCLUI.Wait,
  Vcl.StdCtrls, FireDAC.Comp.UI, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteDef,
  FireDAC.Phys.SQLite, Vcl.Grids, Vcl.DBGrids, clsTable, System.Actions,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.ComCtrls,
  clsGenerator;

type
  TAppDbType = (dbtUnknown, dbtSqLite);

  TFormMain = class(TForm)
    fdc: TFDConnection;
    FDMetaInfoQuery1: TFDMetaInfoQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FileOpenDialog1: TFileOpenDialog;
    Label1: TLabel;
    edtDb: TEdit;
    btnOpenDb: TButton;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    mmoInterface: TMemo;
    btnGenerateAll: TButton;
    ActionManager1: TActionManager;
    actGenerateAll: TAction;
    fdcGenerate: TFDConnection;
    PageControl1: TPageControl;
    tsInteface: TTabSheet;
    tsImplementation: TTabSheet;
    mmoImplementation: TMemo;
    actOptions: TAction;
    Button2: TButton;
    tsConstructor: TTabSheet;
    btnRefresh: TButton;
    mmoConstructor: TMemo;
    tsFullUnit: TTabSheet;
    mmoFullUnit: TMemo;
    procedure edtDbChange(Sender: TObject);
    procedure btnOpenDbClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure mmoKeyPress(Sender: TObject; var Key: Char);
    procedure actGenerateAllExecute(Sender: TObject);
    procedure actOptionsExecute(Sender: TObject);
    procedure actOptionsUpdate(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
  private
    fAppDbType: TAppDbType;
    fAppTables: TAppTables;
    fTables: TTablesList;
    fColumnsList: TColumnsList;
    fTableTemplateList: TTableTemplateList;
    fTemplatePointList: TTemplatePointList;
    fOptions: TOptionsList;
    fOption: TOptions;
    procedure CheckForDbUpdates;
    function ConnectDB(fileName: string): Boolean;
    procedure EmptyTables;
    function ExecuteSqlUpdate(Version: integer): Boolean;
    function Generate(table: string): TAppTable;
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses
  System.IOUtils, frmOptions, frmTableOptions;

procedure TFormMain.actOptionsExecute(Sender: TObject);
var
  wkForm: TFormOptions;
begin
  wkForm := TFormOptions.Create(Self);
  try
    wkForm.fdc := fdcGenerate;
    wkForm.Options := fOptions;
    wkForm.ShowModal;
  finally
    wkForm.Free;
  end;
end;

procedure TFormMain.actOptionsUpdate(Sender: TObject);
begin
  actOptions.Enabled := fdcGenerate.Connected;
  actGenerateAll.Enabled := actOptions.Enabled;
  btnRefresh.Enabled := actOptions.Enabled;
end;

procedure TFormMain.btnOpenDbClick(Sender: TObject);
begin
  FileOpenDialog1.DefaultFolder := ExtractFilePath(edtDb.Text);
  if FileOpenDialog1.Execute then
    edtDb.Text := FileOpenDialog1.fileName;
end;

procedure TFormMain.btnRefreshClick(Sender: TObject);
begin
  edtDbChange(Sender);
end;

procedure TFormMain.actGenerateAllExecute(Sender: TObject);
  procedure DoFullUnit;
  begin
    if (fOption.UnitName = '') then
    begin
      fOption.UnitName := 'AppClasses';
      fOptions.UpdateDb(fOption, fdcGenerate);
    end;

    mmoFullUnit.Lines.Add(Format('unit %s;' + sLineBreak + sLineBreak +
      'interface' + sLineBreak + sLineBreak + 'uses', [fOption.UnitName]));
    mmoFullUnit.Lines.Add(fOption.InterfaceUses + sLineBreak);

    mmoFullUnit.Lines.Add(Trim(mmoInterface.Lines.Text) + sLineBreak);

    mmoFullUnit.Lines.Add(fAppTables.GetAllTablesInterface(fOption.UnitName,
      fOption.BaseClassList));

    mmoFullUnit.Lines.Add('implementation' + sLineBreak + sLineBreak + 'uses' +
      sLineBreak + fOption.ImplementationUses + sLineBreak);
    mmoFullUnit.Lines.Add(fAppTables.GetAllTablesImplementation
      (fOption.UnitName, fOption.BaseClass, fOption.BaseClassList));

    mmoFullUnit.Lines.Add(Trim(mmoImplementation.Lines.Text) + sLineBreak);
    mmoFullUnit.Lines.Add('end.');
  end;

var
  wkTable: TAppTable;
begin
  if FDMetaInfoQuery1.Active then
  begin
    mmoInterface.Lines.BeginUpdate;
    mmoImplementation.Lines.BeginUpdate;
    mmoConstructor.Lines.BeginUpdate;
    mmoFullUnit.Lines.BeginUpdate;
    try
      mmoInterface.Lines.Clear;
      mmoImplementation.Lines.Clear;
      mmoConstructor.Lines.Clear;
      mmoFullUnit.Lines.Clear;

      fAppTables.Clear;
      FDMetaInfoQuery1.First;
      while not FDMetaInfoQuery1.Eof do
      begin
        wkTable := Generate(FDMetaInfoQuery1.FieldByName('Table_Name')
          .AsString);
        if Assigned(wkTable) then
          fAppTables.Add(wkTable);
        FDMetaInfoQuery1.Next;
      end;

      fAppTables.SynchForeignKeys;
      mmoInterface.Lines.Add(fAppTables.GetInterfaceClause);
      mmoImplementation.Lines.Add(fAppTables.GetImplementationClause);
      mmoConstructor.Lines.Add(fAppTables.GetConstructorClause);
      mmoConstructor.Lines.Add(fAppTables.GetDestructorClause);

      DoFullUnit;
    finally
      mmoInterface.Lines.EndUpdate;
      mmoImplementation.Lines.EndUpdate;
      mmoConstructor.Lines.EndUpdate;
      mmoFullUnit.Lines.EndUpdate;
    end;
  end;
end;

procedure TFormMain.CheckForDbUpdates;
var
  wkQry: TFDQuery;
  Version: integer;
begin
  wkQry := TFDQuery.Create(Self);
  try
    wkQry.Connection := fdcGenerate;
    wkQry.SQL.Text := 'Select Version From Options';
    try
      wkQry.Open
    except
      on E: Exception do
      begin
        ExecuteSqlUpdate(1);
        wkQry.Open;
      end;
    end;

    if not wkQry.Eof then
    begin
      Version := wkQry.Fields[0].AsInteger;
      while ExecuteSqlUpdate(Version) do
        inc(Version);
    end;

  finally
    wkQry.Free;
  end;
end;

function TFormMain.ConnectDB(fileName: string): Boolean;
begin
  fdc.Connected := False;
  fdc.Params.Clear;
  // FDMetaInfoQuery1.Active := False;

  // For now, just SqLite
  fAppDbType := TAppDbType.dbtSqLite;
  try
    case fAppDbType of
      TAppDbType.dbtSqLite:
        begin
          fdc.Params.Values['DriverID'] := 'SQLite';
          fdc.Params.Values['Database'] := fileName;
          fdc.Params.Values['SharedCache'] := 'False';
          fdc.Params.Values['DateTimeFormat'] := 'DateTime';
          fdc.Params.Values['OpenMode'] := 'CreateUTF16';
          fdc.Params.Values['JournalMode'] := 'Off';
          fdc.Params.Values['LockingMode'] := 'Normal';
        end;
    end;
    fdc.Connected := True;
    Result := True;
  except
    on E: Exception do
    begin
      Exit;
    end;
  end;

  FDMetaInfoQuery1.Active := True;
  // FDMetaInfoQuery1.Refresh;
  DataSource1.Enabled := True;

end;

procedure TFormMain.DBGrid1DblClick(Sender: TObject);
var
  wkForm: TFormTableOptions;
begin
  // Generate();
  wkForm := TFormTableOptions.Create(Self);
  try
    wkForm.SetTable(FDMetaInfoQuery1.FieldByName('Table_Name').AsString, fdc,
      fdcGenerate, fTables, fTemplatePointList, fTableTemplateList,
      fColumnsList);
    wkForm.ShowModal;
  finally
    wkForm.Free;
  end;
end;

procedure TFormMain.edtDbChange(Sender: TObject);
var
  sourceDb: string;
  optionsFile: string;
  createOptions: Boolean;

begin
  if not ConnectDB(edtDb.Text) then
    showMessage('Unable to open database!')
  else
  begin
    // Open options Db
    fdcGenerate.Connected := False;
    fOptions.Clear;
    sourceDb := fdc.Params.Values['Database'];
    optionsFile := TPath.Combine(ExtractFilePath(sourceDb),
      TPath.GetFileNameWithoutExtension(sourceDb));
    optionsFile := optionsFile + 'Options' + ExtractFileExt(sourceDb);
    fdcGenerate.Params.Values['Database'] := optionsFile;
    createOptions := NOT FileExists(optionsFile);
    try
      fdcGenerate.Connected := True;
      if (createOptions) then
      begin
        ExecuteSqlUpdate(0);
      end;

      CheckForDbUpdates;

      EmptyTables;
      fOptions.LoadAllItems(fdcGenerate);
      fOption := fOptions[0];
      fTables.LoadAllItems(fdcGenerate);
      fColumnsList.LoadAllItems(fdcGenerate);
      fTemplatePointList.LoadAllItems(fdcGenerate);
      fTableTemplateList.LoadAllItems(fdcGenerate);
      actGenerateAll.Execute;
    except
      on E: Exception do
      begin
        showMessage('Unable to connect to or create options database!');
      end;
    end;

  end;
end;

procedure TFormMain.EmptyTables;
begin
  fAppTables.Clear;
  fOptions.Clear;
  fTableTemplateList.Clear;
  fColumnsList.Clear;
  fTables.Clear;
  fTemplatePointList.Clear;
end;

function TFormMain.ExecuteSqlUpdate(Version: integer): Boolean;
var
  wkQry: TFDQuery;
  ResStream: TResourceStream;
  strStream: TStringStream;
begin
  Result := False;
  wkQry := TFDQuery.Create(Self);
  try
    wkQry.Connection := fdcGenerate;
    try
      ResStream := TResourceStream.Create(hInstance,
        Format('SqlUpdate%d', [Version]), RT_RCDATA);
      try
        strStream := TStringStream.Create;
        try
          strStream.CopyFrom(ResStream, 0);
          strStream.Position := 0;
          wkQry.SQL.Text := strStream.ReadString(strStream.Size);
          wkQry.ExecSQL;
          Result := True;
        finally
          strStream.Free;
        end;
      finally
        ResStream.Free;
      end;
    except
      // Resource not found?
    end;
  finally
    wkQry.Free;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := True;
  edtDb.Text := ExtractFilePath(ParamStr(0));
  // edtDb.Text := TPath.Combine(TPath.GetHomePath, 'Corbtech\StgDemo\');
  edtDb.Text := '..\..\';
  edtDb.OnChange := edtDbChange;

  fAppTables := TAppTables.Create;
  fOptions := TOptionsList.Create('Options');
  fOptions.LogChanges := False;
  fTables := TTablesList.Create('Tables');
  fColumnsList := TColumnsList.Create('Columns', fTables);
  fTemplatePointList := TTemplatePointList.Create('TemplatePoints');
  fTableTemplateList := TTableTemplateList.Create('TableTemplates', fTables,
    fTemplatePointList);
end;

procedure TFormMain.FormDestroy(Sender: TObject);
begin
  fdc.Connected := False;
  fAppTables.Free;
  fOptions.Free;
  fTableTemplateList.Free;
  fColumnsList.Free;
  fTables.Free;
  fTemplatePointList.Free;
end;

function TFormMain.Generate(table: string): TAppTable;
const
  indent = '   ';
var
  metaInfo: TFDMetaInfoQuery;
  metaInfoFKeyFields: TFDMetaInfoQuery;
  ws: string;
  wkDataType: string;
  fieldName: string;
  dataType: integer;
  wkField: TAppField;
  wkTable: TTables;
  wkColumn: TColumns;
begin
  metaInfo := TFDMetaInfoQuery.Create(Self);
  metaInfoFKeyFields := TFDMetaInfoQuery.Create(Self);
  try
    metaInfo.Connection := fdc;
    metaInfoFKeyFields.Connection := fdc;
    metaInfo.MetaInfoKind := mkTableFields;
    metaInfo.ObjectName := table;
    metaInfo.Open;

    wkTable := fTables.FindTable(table);
    if not Assigned(wkTable) then
    begin
      wkTable := fTables.CreateItem(fdcGenerate);
      wkTable.Table_Name := table;
      wkTable.ObjectName := table;
      fTables.UpdateDb(wkTable, fdcGenerate);
    end;

    if wkTable.IgnoreTable then
      Result := nil
    else
    begin
      Result := TAppTable.Create(table, fOption, wkTable, fTableTemplateList);
      Result.ListAttributes := wkTable.ListAttributes;
      Result.ObjectAttributes := wkTable.ObjectAttributes;

      // This should all be pushed into the TAppTable object!
      while Not metaInfo.Eof do
      begin
        fieldName := metaInfo.FieldByName('COLUMN_NAME').AsString;
        dataType := metaInfo.FieldByName('COLUMN_DATATYPE').AsInteger;
        wkColumn := wkTable.FindColumn(fieldName);

        if Not Assigned(wkColumn) then
        begin
          Result.Fields.Add(TAppField.Create(fieldName, dataType, Result));
        end
        else if Not wkColumn.IgnoreColumn then
        begin
          wkField := TAppField.Create(fieldName, dataType, Result);
          wkField.ColumnAlias := wkColumn.ColumnAlias;
          wkField.ForeignKeyObjectName := wkColumn.ObjectName;
          wkField.ParentTableListName := wkColumn.ObjectListName;
          wkField.Attributes := wkColumn.Attributes;
          wkField.OneWayLinkToForeignKey := wkColumn.OneWayLinkToForeignKey;
          Result.Fields.Add(wkField);
        end;

        // case dataType of
        // 1:
        // wkDataType := 'Boolean';
        // 2, 3, 4:
        // wkDataType := 'integer';
        // 11:
        // wkDataType := 'double';
        // 16:
        // wkDataType := 'TDateTime';
        // 17:
        // wkDataType := 'TTime';
        // 18:
        // wkDataType := 'TDate';
        // 23, 28:
        // wkDataType := 'string';
        // else
        // wkDataType := 'Unknown??: ' + metaInfo.FieldByName('COLUMN_DATATYPE')
        // .AsString + ' ' + metaInfo.FieldByName('COLUMN_TYPENAME').AsString;
        // end;
        metaInfo.Next;
      end;

      metaInfo.Close;
      metaInfo.MetaInfoKind := mkForeignKeys;
      metaInfo.ObjectName := table;
      metaInfoFKeyFields.MetaInfoKind := mkForeignKeyFields;
      metaInfoFKeyFields.BaseObjectName := table;
      metaInfo.Open;

      while not metaInfo.Eof do
      begin
        metaInfoFKeyFields.ObjectName :=
          metaInfo.FieldByName('FKEY_NAME').AsString;
        metaInfoFKeyFields.Open;
        wkField := Result.Fields.FindField
          (metaInfoFKeyFields.FieldByName('COLUMN_NAME').AsString);
        if Assigned(wkField) then
        begin
          wkField.ParentTableName :=
            metaInfo.FieldByName('PKEY_TABLE_NAME').AsString;
          wkField.ParentFieldName := metaInfoFKeyFields.FieldByName
            ('PKEY_COLUMN_NAME').AsString;
        end;
        metaInfoFKeyFields.Close;
        metaInfo.Next;
      end;
    end;
  finally
    metaInfo.Free;
    metaInfoFKeyFields.Free;
  end;
end;

procedure TFormMain.mmoKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = ^a) and (Sender is TMemo) then
  begin
    (Sender as TMemo).SelectAll;
    Key := #0;
  end;
end;

end.
