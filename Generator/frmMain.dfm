object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'Storm Generator'
  ClientHeight = 589
  ClientWidth = 791
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    791
    589)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 27
    Width = 53
    Height = 13
    Caption = 'Database: '
  end
  object edtDb: TEdit
    Left = 75
    Top = 24
    Width = 663
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ReadOnly = True
    TabOrder = 0
    TextHint = 'Select database name'
  end
  object btnOpenDb: TButton
    Left = 744
    Top = 22
    Width = 33
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 1
    OnClick = btnOpenDbClick
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 82
    Width = 233
    Height = 478
    Anchors = [akLeft, akTop, akBottom]
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'RECNO'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'CATALOG_NAME'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'SCHEMA_NAME'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'TABLE_NAME'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TABLE_TYPE'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'TABLE_SCOPE'
        Visible = False
      end>
  end
  object btnGenerateAll: TButton
    Left = 16
    Top = 51
    Width = 75
    Height = 25
    Action = actGenerateAll
    TabOrder = 3
  end
  object PageControl1: TPageControl
    Left = 264
    Top = 82
    Width = 513
    Height = 478
    ActivePage = tsFullUnit
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 4
    object tsInteface: TTabSheet
      Caption = 'I&nterface'
      object mmoInterface: TMemo
        Left = 0
        Top = 0
        Width = 505
        Height = 450
        Align = alClient
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyPress = mmoKeyPress
      end
    end
    object tsImplementation: TTabSheet
      Caption = 'I&mplementation'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object mmoImplementation: TMemo
        Left = 0
        Top = 0
        Width = 505
        Height = 450
        Align = alClient
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyPress = mmoKeyPress
      end
    end
    object tsConstructor: TTabSheet
      Caption = '&Constructor/Destructor'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object mmoConstructor: TMemo
        Left = 0
        Top = 0
        Width = 505
        Height = 450
        Align = alClient
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyPress = mmoKeyPress
      end
    end
    object tsFullUnit: TTabSheet
      Caption = '&Full Unit'
      ImageIndex = 3
      object mmoFullUnit: TMemo
        Left = 0
        Top = 0
        Width = 505
        Height = 450
        Align = alClient
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyPress = mmoKeyPress
      end
    end
  end
  object Button2: TButton
    Left = 112
    Top = 51
    Width = 75
    Height = 25
    Action = actOptions
    TabOrder = 5
  end
  object btnRefresh: TButton
    Left = 216
    Top = 51
    Width = 75
    Height = 25
    Caption = 'Refresh'
    TabOrder = 6
    OnClick = btnRefreshClick
  end
  object fdc: TFDConnection
    Params.Strings = (
      'LockingMode=Normal'
      'DateTimeFormat=DateTime'
      
        'Database=C:\Users\Steve\AppData\Roaming\Corbtech\StgDemo\Stg0Opt' +
        'ions.s3db'
      'DriverID=SQLite')
    ResourceOptions.AssignedValues = [rvAutoConnect]
    LoginPrompt = False
    Left = 104
    Top = 152
  end
  object FDMetaInfoQuery1: TFDMetaInfoQuery
    Connection = fdc
    Left = 360
    Top = 96
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 288
    Top = 160
  end
  object FileOpenDialog1: TFileOpenDialog
    DefaultExtension = 's3db'
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'Database'
        FileMask = '*.db'
      end
      item
        DisplayName = 'SqLite'
        FileMask = '*.s3db'
      end
      item
        DisplayName = 'SqLite'
        FileMask = '*.SqLite*'
      end
      item
        DisplayName = 'All Files'
        FileMask = '*.*'
      end>
    FileTypeIndex = 2
    Options = [fdoPathMustExist, fdoFileMustExist, fdoDontAddToRecent]
    Left = 472
    Top = 96
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 216
    Top = 88
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = FDMetaInfoQuery1
    Left = 424
    Top = 184
  end
  object ActionManager1: TActionManager
    Left = 568
    Top = 184
    StyleName = 'Platform Default'
    object actGenerateAll: TAction
      Caption = '&Generate All'
      OnExecute = actGenerateAllExecute
    end
    object actOptions: TAction
      Caption = 'Options'
      OnExecute = actOptionsExecute
      OnUpdate = actOptionsUpdate
    end
  end
  object fdcGenerate: TFDConnection
    Params.Strings = (
      'LockingMode=Normal'
      'DateTimeFormat=DateTime'
      'DriverID=SQLite')
    ResourceOptions.AssignedValues = [rvAutoConnect]
    LoginPrompt = False
    Left = 168
    Top = 200
  end
end
