unit frmTableOptions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Variants,
  System.Classes, Vcl.Graphics, System.Generics.Collections,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Comp.Client,
  clsGenerator, Vcl.StdCtrls, Vcl.ComCtrls, frmAutoFill;

type
  TFormTableOptions = class(TFormAutoFill)
    btnOk: TButton;
    btnCancel: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cbxTemplates: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    mmoCode: TMemo;
    Label1: TLabel;
    edtBaseObjectName: TEdit;
    cbxIgnore: TCheckBox;
    lbxColumns: TListBox;
    lblColumns: TLabel;
    cbxIgnoreColumn: TCheckBox;
    gbxDetails: TGroupBox;
    edtColumnAlias: TEdit;
    Label4: TLabel;
    edtObjectName: TEdit;
    Label5: TLabel;
    edtObjectListName: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    edtAttributes: TEdit;
    Label8: TLabel;
    edtListAttributes: TEdit;
    Label9: TLabel;
    edtObjectAttributes: TEdit;
    cbxOneWayLinkToForeignKey: TCheckBox;
    procedure TableDataChange(Sender: TObject);
    procedure mmoKeyPress(Sender: TObject; var Key: Char);
    procedure cbxTemplatesChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbxColumnsClick(Sender: TObject);
    procedure cbxColumnClick(Sender: TObject);
    procedure edtFieldExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    fColumns: TList<TColumns>;
    fCurrTemplate: TTableTemplate;
    fCurrColumn: TColumns;
    fFdc: TFDConnection;
    fFdcGenerate: TFDConnection;
    fTable: TTables;
    fTableList: TTablesList;
    fTemplatePointList: TTemplatePointList;
    fTableTemplateList: TTableTemplateList;
    fColumnsList: TColumnsList;
    procedure LoadColumns;
    procedure SaveCurrTemplate;
    procedure SetCurrColumn(const Value: TColumns);
    procedure SetColumnText;
  public
    property CurrColumn: TColumns read fCurrColumn write SetCurrColumn;
    procedure SetTable(const Value: string; fdc: TFDConnection;
      fdcGenerate: TFDConnection; TableList: TTablesList;
      TemplatePointList: TTemplatePointList;
      TableTemplateList: TTableTemplateList; ColumnsList: TColumnsList);
  end;

var
  FormTableOptions: TFormTableOptions;

implementation

{$R *.dfm}

uses
  System.SysUtils, FireDAC.Phys.Intf, appConstants;
{ TForm1 }

procedure TFormTableOptions.cbxColumnClick(Sender: TObject);
begin
  if Assigned(CurrColumn) then
  begin
    CurrColumn.IgnoreColumn := cbxIgnoreColumn.Checked;
    CurrColumn.OneWayLinkToForeignKey := cbxOneWayLinkToForeignKey.Checked;
    fColumnsList.UpdateDb(CurrColumn, fFdcGenerate);
    SetColumnText;
  end;
end;

procedure TFormTableOptions.cbxTemplatesChange(Sender: TObject);
var
  wkTp: TTemplatePoint;
begin
  if Assigned(fCurrTemplate) then
    SaveCurrTemplate;

  wkTp := TTemplatePoint(cbxTemplates.Items.Objects[cbxTemplates.ItemIndex]);
  fCurrTemplate := fTableTemplateList.FindTemplate(fTable.Table_Id,
    wkTp.TemplatePoint_Id);
  if not Assigned(fCurrTemplate) then
  begin
    fCurrTemplate := fTableTemplateList.CreateItem(fFdcGenerate);
    fCurrTemplate.Table_Id := fTable.Table_Id;
    fCurrTemplate.TemplatePoint_Id := wkTp.TemplatePoint_Id;
    // fCurrTemplate.TemplateCode := '';
    mmoCode.Lines.Clear;
    SaveCurrTemplate;
  end;

  mmoCode.Lines.Text := fCurrTemplate.TemplateCode;
end;

procedure TFormTableOptions.edtFieldExit(Sender: TObject);
begin
  edtColumnAlias.Text := Trim(edtColumnAlias.Text);
  edtObjectName.Text := Trim(edtObjectName.Text);
  edtObjectListName.Text := Trim(edtObjectListName.Text);
  edtAttributes.Text := Trim(edtAttributes.Text);

  if Assigned(CurrColumn) then
  begin
    if (edtColumnAlias.Text <> CurrColumn.ColumnAlias) or
      (edtObjectName.Text <> CurrColumn.ObjectName) or
      (edtObjectListName.Text <> CurrColumn.ObjectListName) or
      (edtAttributes.Text <> CurrColumn.Attributes) then
    begin
      CurrColumn.ColumnAlias := edtColumnAlias.Text;
      CurrColumn.ObjectName := edtObjectName.Text;
      CurrColumn.ObjectListName := edtObjectListName.Text;
      CurrColumn.Attributes := edtAttributes.Text;
      fColumnsList.UpdateDb(CurrColumn, fFdcGenerate);
      SetColumnText;
    end;
  end;
end;

procedure TFormTableOptions.TableDataChange(Sender: TObject);
begin
  edtBaseObjectName.Text := Trim(edtBaseObjectName.Text);
  edtListAttributes.Text := Trim(edtListAttributes.Text);
  edtObjectAttributes.Text := Trim(edtObjectAttributes.Text);

  fTable.ObjectName := edtBaseObjectName.Text;
  fTable.IgnoreTable := cbxIgnore.Checked;
  fTable.ListAttributes := edtListAttributes.Text;
  fTable.ObjectAttributes := edtObjectAttributes.Text;
  fTableList.UpdateDb(fTable, fFdcGenerate);
end;

procedure TFormTableOptions.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  case ModalResult of
    mrOk:
      if Assigned(fCurrTemplate) then
        SaveCurrTemplate;
    mrNone:
      case MessageDlg('Save these changes?', TMsgDlgType.mtConfirmation,
        [mbYes, mbNo, mbCancel], 0) of
        mrYes:
          if Assigned(fCurrTemplate) then
            SaveCurrTemplate;
        mrCancel:
          CanClose := False;
      end;
  end;
end;

procedure TFormTableOptions.FormCreate(Sender: TObject);
begin
  fColumns := TList<TColumns>.Create;
end;

procedure TFormTableOptions.FormDestroy(Sender: TObject);
begin
  fColumns.Free;
end;

procedure TFormTableOptions.FormResize(Sender: TObject);
begin
  mmoCode.Width := TabSheet2.ClientWidth - mmoCode.Left - 10;
  mmoCode.Height := TabSheet2.ClientHeight - mmoCode.Top - 10;
end;

procedure TFormTableOptions.lbxColumnsClick(Sender: TObject);
begin
  CurrColumn := TColumns(lbxColumns.Items.Objects[lbxColumns.ItemIndex]);
end;

procedure TFormTableOptions.LoadColumns;
var
  metaInfo: TFDMetaInfoQuery;
  // metaInfoFKeyFields: TFDMetaInfoQuery;
  wkColumn: TColumns;
  fieldName: string;
  pfx: string;
begin
  metaInfo := TFDMetaInfoQuery.Create(Self);
  // metaInfoFKeyFields := TFDMetaInfoQuery.Create(Self);
  lbxColumns.Items.BeginUpdate;
  try
    fColumns.Clear;
    lbxColumns.Items.Clear;

    metaInfo.Connection := fFdc;
    // metaInfoFKeyFields.Connection := fFdc;
    metaInfo.MetaInfoKind := mkTableFields;
    metaInfo.ObjectName := fTable.Table_Name;
    metaInfo.Open;

    while Not metaInfo.Eof do
    begin
      fieldName := metaInfo.FieldByName('COLUMN_NAME').AsString.ToUpper;

      // Find existing column, if any
      for wkColumn in fColumnsList do
      begin
        if (wkColumn.Table_Id = fTable.Table_Id) and
          (wkColumn.ColumnName.ToUpper = fieldName) then
          break;
      end;

      if Not Assigned(wkColumn) or (wkColumn.Table_Id <> fTable.Table_Id) or
        (wkColumn.ColumnName.ToUpper <> fieldName) then
      begin
        wkColumn := fColumnsList.CreateItem(fFdcGenerate);
        wkColumn.Table_Id := fTable.Table_Id;
        wkColumn.ColumnName := metaInfo.FieldByName('COLUMN_NAME').AsString;
        fColumnsList.UpdateDb(wkColumn, fFdcGenerate);
      end;

      fColumns.Add(wkColumn);
      pfx := '';
      if wkColumn.IgnoreColumn or (wkColumn.ColumnAlias <> '') or
        (wkColumn.ObjectName <> '') or (wkColumn.ObjectListName <> '') or
        (wkColumn.Attributes <> '') or wkColumn.OneWayLinkToForeignKey then
        pfx := '*';
      lbxColumns.Items.AddObject(pfx + wkColumn.ColumnName, wkColumn);

      metaInfo.Next;
    end;

  finally
    metaInfo.Free;
    // metaInfoFKeyFields.Free;
    lbxColumns.Items.EndUpdate;
  end;

  if lbxColumns.Items.Count > 0 then
  begin
    CurrColumn := TColumns(lbxColumns.Items.Objects[0]);
    lbxColumns.ItemIndex := 0;
    SetColumnText;
  end;
end;

procedure TFormTableOptions.mmoKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = ^A) and (Sender is TMemo) then
  begin
    (Sender as TMemo).SelectAll;
    Key := #0;
  end;
end;

procedure TFormTableOptions.SaveCurrTemplate;
begin
  fCurrTemplate.TemplateCode := mmoCode.Lines.Text;
  fTableTemplateList.UpdateDb(fCurrTemplate, fFdcGenerate);
end;

procedure TFormTableOptions.SetColumnText;
var
  pfx: string;
  wkColumn: TColumns;

begin
  if (lbxColumns.ItemIndex > -1) then
  begin
    wkColumn := TColumns(lbxColumns.Items.Objects[lbxColumns.ItemIndex]);
    pfx := '';
    if wkColumn.IgnoreColumn or (wkColumn.ColumnAlias <> '') or
      (wkColumn.ObjectName <> '') or (wkColumn.ObjectListName <> '') or
      (wkColumn.Attributes <> '') or wkColumn.OneWayLinkToForeignKey then
      pfx := '*';
    lbxColumns.Items[lbxColumns.ItemIndex] := pfx + wkColumn.ColumnName;
  end;
end;

procedure TFormTableOptions.SetCurrColumn(const Value: TColumns);
begin
  fCurrColumn := Value;
  AutoFillObject := Value;
end;

procedure TFormTableOptions.SetTable(const Value: string; fdc: TFDConnection;
  fdcGenerate: TFDConnection; TableList: TTablesList;
  TemplatePointList: TTemplatePointList; TableTemplateList: TTableTemplateList;
  ColumnsList: TColumnsList);

// Check a single required point exists!
  procedure CheckEntry(tpl: TTemplatePointList; id: integer; desc: string);
  var
    wkTPL: TTemplatePoint;
  begin
    wkTPL := tpl.GetById(id);
    if not Assigned(wkTPL) then
    begin
      wkTPL := tpl.CreateItem(fFdcGenerate, intToStr(id));
      wkTPL.Description := desc;
      tpl.UpdateDb(wkTPL, fFdcGenerate);
    end;
  end;

// Check that all required points exist!
  procedure CheckEntries(tpl: TTemplatePointList);
  begin
    CheckEntry(tpl, tmpltBaseObjectPrivate, 'Base Object Private Vars/Methods');
    CheckEntry(tpl, tmpltBaseObjectStrictPrivate,
      'Base Object Strict Private Vars/Methods');
    CheckEntry(tpl, tmpltBaseObjectPublished,
      'Base Object Published Vars/Methods');
    CheckEntry(tpl, tmpltBaseObjectPublic, 'Base Object Public Vars/Methods');
    CheckEntry(tpl, tmpltBaseObjectMethods,
      'Base Object Method Implementations');
    CheckEntry(tpl, tmpltBaseObjectConstructor,
      'Base Object Constructor Custom Code');
    CheckEntry(tpl, tmpltBaseObjectDestructor,
      'Base Object Destructor Custom Code');

    CheckEntry(tpl, tmpltDbBaseObjectListPrivate,
      'DBase Object List Private Vars/Methods');
    CheckEntry(tpl, tmpltDbBaseObjectListStrictPrivate,
      'DBase Object List Strict Private Vars/Methods');
    CheckEntry(tpl, tmpltDbBaseObjectListPublished,
      'DBase Object List Published Vars/Methods');
    CheckEntry(tpl, tmpltDbBaseObjectListPublic,
      'DBase Object List Public Vars/Methods');
    CheckEntry(tpl, tmpltDbBaseObjectListMethods,
      'DBase Object List Public Method Implementations');
  end;

var
  wkTPL: TTemplatePoint;

begin
  Caption := ' ' + Value + ' Options';

  fFdc := fdc;
  fFdcGenerate := fdcGenerate;
  fTableList := TableList;
  fTemplatePointList := TemplatePointList;
  fTableTemplateList := TableTemplateList;
  fColumnsList := ColumnsList;

  fTable := fTableList.FindTable(Value);
  if not Assigned(fTable) then
  begin
    fTable := fTableList.CreateItem(fFdcGenerate);
    fTable.Table_Name := Value;
    fTable.ObjectName := Value;
    fTableList.UpdateDb(fTable, fFdcGenerate);
  end;

  CheckEntries(fTemplatePointList);

  edtBaseObjectName.Text := fTable.ObjectName;
  cbxIgnore.Checked := fTable.IgnoreTable;
  edtListAttributes.Text := fTable.ListAttributes;
  edtObjectAttributes.Text := fTable.ObjectAttributes;

  cbxTemplates.Items.BeginUpdate;
  try
    cbxTemplates.Items.Clear;
    for wkTPL in fTemplatePointList do
    begin
      cbxTemplates.Items.AddObject(wkTPL.Description, wkTPL);
    end;
  finally
    cbxTemplates.ItemIndex := cbxTemplates.Items.Count - 1;
    cbxTemplates.Items.EndUpdate;
    cbxTemplatesChange(nil);
  end;

  LoadColumns;

  edtBaseObjectName.OnChange := TableDataChange;
  edtListAttributes.OnChange := TableDataChange;
  edtObjectAttributes.OnChange := TableDataChange;
end;

end.
