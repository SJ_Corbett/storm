object FormOptions: TFormOptions
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Options'
  ClientHeight = 342
  ClientWidth = 824
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    824
    342)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 17
    Top = 19
    Width = 68
    Height = 13
    Caption = '&1. Base Class:'
    FocusControl = cbxBaseClass
  end
  object Label1: TLabel
    Left = 17
    Top = 60
    Width = 103
    Height = 13
    Caption = '&2. Base Db List Class:'
    FocusControl = cbxDbListClass
  end
  object Label3: TLabel
    Left = 17
    Top = 100
    Width = 132
    Height = 13
    Caption = '&3. Base Property List Class:'
    FocusControl = cbxPropertyListClass
  end
  object Label4: TLabel
    Left = 17
    Top = 140
    Width = 124
    Height = 13
    Caption = '&4. Generated Unit Name::'
    FocusControl = cbxPropertyListClass
  end
  object Label5: TLabel
    Left = 17
    Top = 179
    Width = 88
    Height = 13
    Caption = '&5. Interface Uses:'
    FocusControl = mmoInterface
  end
  object Label6: TLabel
    Left = 421
    Top = 179
    Width = 117
    Height = 13
    Caption = '&6. Implementation Uses:'
    FocusControl = mmoImplementation
  end
  object cbxBaseClass: TComboBox
    Left = 158
    Top = 16
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 0
    Text = 'TStormBaseClass'
    OnChange = cbxBaseClassChange
    Items.Strings = (
      'TStormBaseClass'
      'TStgBaseClass')
  end
  object Button1: TButton
    Left = 355
    Top = 301
    Width = 115
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Close'
    TabOrder = 6
    OnClick = Button1Click
    ExplicitTop = 304
  end
  object cbxDbListClass: TComboBox
    Left = 158
    Top = 57
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 1
    Text = 'TStormBaseClassDbList'
    OnChange = cbxBaseClassChange
    Items.Strings = (
      'TStormBaseClassDbList'
      'TStgBaseClassDbList')
  end
  object cbxPropertyListClass: TComboBox
    Left = 158
    Top = 97
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 2
    Text = 'TStormPropertiesList'
    OnChange = cbxBaseClassChange
    Items.Strings = (
      'TStormPropertiesList'
      'TStgPropertiesList')
  end
  object edtUnitName: TEdit
    Left = 160
    Top = 136
    Width = 143
    Height = 21
    TabOrder = 3
    OnExit = cbxBaseClassChange
  end
  object mmoInterface: TMemo
    Left = 17
    Top = 198
    Width = 390
    Height = 86
    Anchors = [akLeft, akTop, akBottom]
    Lines.Strings = (
      'mmoInterface')
    TabOrder = 4
    OnExit = cbxBaseClassChange
    OnKeyPress = mmoKeyPress
    ExplicitHeight = 89
  end
  object mmoImplementation: TMemo
    Left = 421
    Top = 198
    Width = 390
    Height = 86
    Anchors = [akLeft, akTop, akBottom]
    Lines.Strings = (
      'mmoInterface')
    TabOrder = 5
    OnExit = cbxBaseClassChange
    OnKeyPress = mmoKeyPress
    ExplicitHeight = 89
  end
end
