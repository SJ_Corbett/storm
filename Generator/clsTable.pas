unit clsTable;

interface

uses
  System.Classes, System.Types, System.Generics.Collections, clsGenerator;

type
  TAppTable = class;

  TAppField = class(TObject)
  strict private
    fAppTable: TAppTable;
    fFieldType: integer;
    fFieldName: string;
    fParentTableName: string;
    fParentFieldName: string;
    fForeignKey: TAppField;
    // private
    fForeignKeyObjectName: string;
    fColumnAlias: string;
    function GetFieldTypeStr: string;
  private
    fParentTableListName: string;
    fAttributes: string;
    FOneWayLinkToForeignKey: Boolean;
    function GetEffectiveName: string;
    function GetForeignKeyObjectName: string;
    function GetParentTableListName: string;
  public
    constructor Create(FieldName: string; FieldType: integer;
      AppTable: TAppTable);
    property AppTable: TAppTable read fAppTable;
    property Attributes: string read fAttributes write fAttributes;
    property ColumnAlias: string read fColumnAlias write fColumnAlias;
    property FieldName: string read fFieldName;
    property EffectiveName: string read GetEffectiveName;
    property FieldType: integer read fFieldType;
    property FieldTypeStr: string read GetFieldTypeStr;
    property ForeignKey: TAppField read fForeignKey write fForeignKey;
    property ForeignKeyObjectName: string read GetForeignKeyObjectName
      write fForeignKeyObjectName;
    property ParentTableName: string read fParentTableName
      write fParentTableName;
    property ParentTableListName: string read GetParentTableListName
      write fParentTableListName;
    property ParentFieldName: string read fParentFieldName
      write fParentFieldName;
    property OneWayLinkToForeignKey: Boolean read FOneWayLinkToForeignKey
      write FOneWayLinkToForeignKey;
  end;

  // TAppForeignKey = class
  // strict private
  // private
  // // fParentTableName: string;
  // // fField: TAppField;
  // // fParentFieldName: string;
  // public
  // constructor Create(Field: TAppField; ParentTableName: string;
  // ParentFieldName: string);
  // // property Field: TAppField read fField;
  // // property ParentTableName: string read fParentTableName;
  // // property ParentFieldName: string read fParentFieldName;
  // end;

  TAppFieldList = class(TObjectList<TAppField>)
  public
    function FindField(FieldName: string): TAppField;
  end;

  TAppTable = class(TObject)
  strict private
    fFields: TAppFieldList;
    fKeyFieldName: string;
    fKeyType: string;
    fObjectName: string;
    fOption: TOptions;
    fReferencedObjects: TAppFieldList;
    fTable: TTables;
    fTableTemplateList: TTableTemplateList;
    fTableName: string;
    fForeignKeys: TAppFieldList;
    fSortSeqn: integer;
    fForeignKeyTables: TList<TAppTable>;
    fObjectAttributes: string;
    fListAttributes: string;
    function GetCreateStmnt(setDefault: Boolean = False): string;
    function GetAssignFKClassLists: string;
    function GetFKClassLists: string;
    function GetInterfaceClause: string;
    function GetImplementationClause: string;
    function GetTemplateCode(TemplatePoint_Id: integer): string;
  private
    function GetHasNumericKey: Boolean;
  public
    constructor Create(TableName: string; Option: TOptions; Table: TTables;
      TableTemplateList: TTableTemplateList);
    destructor Destroy; override;
    property Fields: TAppFieldList read fFields;
    property ForeignKeys: TAppFieldList read fForeignKeys;
    property ForeignKeyTables: TList<TAppTable> read fForeignKeyTables;
    property HasNumericKey: Boolean read GetHasNumericKey;
    property ObjectName: string read fObjectName;
    property ListAttributes: string read fListAttributes write fListAttributes;
    property ObjectAttributes: string read fObjectAttributes
      write fObjectAttributes;
    property TableName: string read fTableName;
    property Table: TTables read fTable;
    property InterfaceClause: string read GetInterfaceClause;
    property ImplementationClause: string read GetImplementationClause;
    property ReferencedObjects: TAppFieldList read fReferencedObjects;
    property SortSeqn: integer read fSortSeqn write fSortSeqn;
  end;

  TAppTables = class(TObjectList<TAppTable>)
  private
  public
    function FindTable(TableName: string): TAppTable;
    function GetAllTablesInterface(const ClassName: string;
      const BaseClassListName: string): string;
    function GetAllTablesImplementation(const ClassName: string;
      const BaseClassName: string; const BaseClassListName: string): string;
    function GetConstructorClause: string;
    function GetDestructorClause: string;
    function GetImplementationClause: string;
    function GetInterfaceClause: string;
    procedure SortTables;
    procedure SynchForeignKeys;
  end;

implementation

uses
  System.SysUtils, System.Generics.Defaults, appConstants;

const
  indent = '  ';

function Indents(count: integer = 1): string;
var
  ix: integer;
begin
  Result := '';
  for ix := 1 to count do
    Result := Result + indent;
end;

{ TAppField }

constructor TAppField.Create(FieldName: string; FieldType: integer;
  AppTable: TAppTable);
begin
  inherited Create;
  fAppTable := AppTable;
  fFieldName := FieldName;
  fFieldType := FieldType;
  fForeignKey := nil;
  fForeignKeyObjectName := '';
end;

function TAppField.GetEffectiveName: string;
begin
  if fColumnAlias = '' then
    Result := fFieldName
  else
    Result := fColumnAlias;
end;

function TAppField.GetFieldTypeStr: string;
begin
  case FieldType of
    1:
      Result := 'Boolean';
    2, 3, 4:
      Result := 'integer';
    11, 14, 15:
      Result := 'double';
    16:
      Result := 'TDateTime';
    17:
      Result := 'TTime';
    18:
      Result := 'TDate';
    23, 24, 28:
      Result := 'string';
    // 26:
    // Result := 'TMemoryStream';  // Blob field, not implemented!
  else
    Result := 'Unknown??: ' + IntToStr(FieldType);
  end;
end;

function TAppField.GetForeignKeyObjectName: string;
begin
  Result := '';
  if (fForeignKeyObjectName <> '') then
    Result := fForeignKeyObjectName
  else if Assigned(fForeignKey) then
    Result := fForeignKey.AppTable.Table.ObjectName;
end;

function TAppField.GetParentTableListName: string;
begin
  if (fParentTableListName <> '') then
    Result := fParentTableListName
  else
    Result := AppTable.Table.ObjectName + 'List';
end;

{ TAppTable }

constructor TAppTable.Create(TableName: string; Option: TOptions;
  Table: TTables; TableTemplateList: TTableTemplateList);
begin
  inherited Create;
  fTableName := TableName;
  fOption := Option;
  fTable := Table;
  fTableTemplateList := TableTemplateList;
  fFields := TAppFieldList.Create;
  fReferencedObjects := TAppFieldList.Create(False);
  fForeignKeys := TAppFieldList.Create(False);
  fForeignKeyTables := TList<TAppTable>.Create;
  fObjectName := fTable.ObjectName;
end;

destructor TAppTable.Destroy;
begin
  fReferencedObjects.Free;
  fForeignKeys.Free;
  fForeignKeyTables.Free;
  fFields.Free;
  inherited;
end;

function TAppTable.GetAssignFKClassLists: string;
var
  wkAppTable: TAppTable;
begin
  Result := '';
  for wkAppTable in fForeignKeyTables do
  begin
    Result := Result + indent + Format('f%0:sList := %0:sList;',
      [wkAppTable.fTable.ObjectName]) + sLineBreak;
  end;
end;

function TAppTable.GetCreateStmnt(setDefault: Boolean): string;
var
  keyLists: string;
begin
  keyLists := GetFKClassLists;

  if (keyLists = '') and Not HasNumericKey then
    Exit('');

  Result := 'Create(Table: string; ';
  if (keyLists <> '') then
    Result := Result + keyLists + ';';
  Result := Result + ' AOwnsObjects: Boolean';
  if setDefault then
    Result := Result + ' = True';
  Result := Result + ');';
end;

function TAppTable.GetFKClassLists: string;
var
  wkAppTable: TAppTable;
begin
  Result := '';
  for wkAppTable in fForeignKeyTables do
  begin
    if (Result <> '') then
      Result := Result + ';' + sLineBreak + Indents(3);
    Result := Result + Format('%0:sList: T%0:sList',
      [wkAppTable.fTable.ObjectName]);
  end;
end;

function TAppTable.GetHasNumericKey: Boolean;
begin
  Result := (fKeyType <> 'string');
end;

function TAppTable.GetImplementationClause: string;
var
  wkFld: TAppField;
  ws: string;
  constructorMethod: string;
  fkLists: string;
  setLists: string;
  ForeignKey: TAppField;
  referencedField: TAppField;
  indentLvl: integer;
  wkAppTable: TAppTable;
begin
  Result := '';
  // First the object class
  Result := Result + Format('{ T%s }', [fObjectName]) + sLineBreak;

  ws := GetTemplateCode(tmpltBaseObjectConstructor);
  if (ws <> '') then
  begin
    Result := Result + (Format('constructor T%s.Create;', [fObjectName])) +
      sLineBreak;
    Result := Result + 'begin' + sLineBreak + ws;
    Result := Result + indent + 'inherited;' + sLineBreak;
    Result := Result + 'end;' + sLineBreak + sLineBreak;
  end;

  Result := Result + Format('destructor T%s.Destroy;', [fObjectName]) +
    sLineBreak;
  Result := Result + 'begin' + sLineBreak +
    GetTemplateCode(tmpltBaseObjectDestructor);
  // Do we reference a foreign key?
  for ForeignKey in ForeignKeys do
  begin
    if not ForeignKey.OneWayLinkToForeignKey then
      Result := Result + indent + Format('if Assigned(f%0:s) then',
        [ForeignKey.GetForeignKeyObjectName]) + sLineBreak + Indents(2) +
        Format('f%s.%s.Extract(Self);', [ForeignKey.GetForeignKeyObjectName,
        ForeignKey.GetParentTableListName]) + sLineBreak;
  end;
  // Are we referenced as a foreign key?
  for referencedField in ReferencedObjects do
  begin
    Result := Result + indent + Format('if Assigned(f%0:s) then',
      [referencedField.GetParentTableListName]) + sLineBreak + indent + indent +
      Format('f%0:s.Free;', [referencedField.GetParentTableListName]) +
      sLineBreak;
  end;

  Result := Result + indent + 'inherited;' + sLineBreak;
  Result := Result + 'end;' + sLineBreak + sLineBreak;

  Result := Result + Format('function T%s.GetId: string;', [fObjectName]) +
    sLineBreak;
  Result := Result + 'begin' + sLineBreak;

  if (fKeyType = 'string') then
    Result := Result + indent + Format('Result := f%s;', [fKeyFieldName]) +
      sLineBreak
  else
    Result := Result + indent + Format('Result := intToStr(f%s);',
      [fKeyFieldName]) + sLineBreak;
  Result := Result + 'end;' + sLineBreak + sLineBreak;

  if HasNumericKey then
  begin
    Result := Result + Format('function T%s.GetIdInt: Int64;', [fObjectName]) +
      sLineBreak;
    Result := Result + 'begin' + sLineBreak;
    Result := Result + indent + Format('Result := f%s;', [fKeyFieldName]) +
      sLineBreak;
    Result := Result + 'end;' + sLineBreak + sLineBreak;
  end;

  // Are we referenced as a foreign key?
  if (ReferencedObjects.count > 0) then
  begin
    Result := Result + Format('procedure T%s.OnCreate;', [fObjectName, fKeyType]
      ) + sLineBreak;
    Result := Result + 'begin' + sLineBreak;
    Result := Result + indent + 'inherited;' + sLineBreak;

    for referencedField in ReferencedObjects do
    begin
      Result := Result + Indents(1) + Format('if not Assigned(f%0:s) then',
        [referencedField.GetParentTableListName]) + sLineBreak;
      Result := Result + Indents(2) + Format('f%0:s := %1:s<T%2:s>.Create;',
        [referencedField.GetParentTableListName, fOption.PropertiesClassList,
        referencedField.AppTable.fTable.ObjectName]) + sLineBreak;
    end;

    Result := Result + 'end;' + sLineBreak + sLineBreak;
  end;

  fkLists := GetFKClassLists;
  if fkLists <> '' then
  begin
    Result := Result + Format('procedure T%s.ResetLinkedObjects;', [fObjectName]
      ) + sLineBreak;
    Result := Result + 'begin' + sLineBreak;
    Result := Result + indent + 'inherited;' + sLineBreak;

    for wkFld in fFields do
    begin
      if Assigned(wkFld.ForeignKey) then
      begin
        Result := Result + indent + Format('%0:s := %0:s;',
          [wkFld.EffectiveName]) + sLineBreak;
      end;
    end;
    Result := Result + 'end;' + sLineBreak + sLineBreak;

    Result := Result + Format('procedure T%s.SetLists(%s);',
      [fObjectName, fkLists]) + sLineBreak;
    Result := Result + 'begin' + sLineBreak;
    Result := Result + GetAssignFKClassLists;
    Result := Result + indent + 'ResetLinkedObjects;' + sLineBreak;
    Result := Result + 'end;' + sLineBreak + sLineBreak;

    // Create methods to set properties with a foreign key
    for wkFld in fFields do
    begin
      if Assigned(wkFld.ForeignKey) then
      begin
        ForeignKey := wkFld.ForeignKey;
        Result := Result + Format('procedure T%2:s.Set%0:s(const Value: %1:s);',
          [wkFld.EffectiveName, wkFld.FieldTypeStr, fObjectName]) + sLineBreak;
        Result := Result + 'begin' + sLineBreak;

        if (wkFld.FieldTypeStr <> 'string') then
          Result := Result + indent + Format('if Assigned(f%0:sList) then',
            [ForeignKey.AppTable.fTable.ObjectName]) + sLineBreak
        else
        begin
          Result := Result + indent + Format('if Assigned(f%0:sList) and',
            [ForeignKey.AppTable.fTable.ObjectName]) + sLineBreak;
          Result := Result + Indents(2) +
            Format('((Value <> f%0:s) or (Not Assigned(f%1:s) and (Value <> ''''))) then',
            [wkFld.EffectiveName, wkFld.GetForeignKeyObjectName]) + sLineBreak;
        end;

        Result := Result + indent + 'begin' + sLineBreak;
        indentLvl := 2;
        if not wkFld.OneWayLinkToForeignKey then
        begin
          Result := Result + Indents(indentLvl) +
            Format('if Assigned(f%0:s) then', [wkFld.GetForeignKeyObjectName]) +
            sLineBreak;
          Result := Result + Indents(3) + Format('f%0:s.%1:s.Extract(Self);',
            [wkFld.GetForeignKeyObjectName, wkFld.GetParentTableListName]) +
            sLineBreak;

          if (wkFld.FieldTypeStr = 'string') then
          begin
            Result := Result + Indents(indentLvl) + 'if (Value <> '''') then' +
              sLineBreak;
            Result := Result + Indents(indentLvl) + 'begin;' + sLineBreak;
            inc(indentLvl);
          end;

          Result := Result + Indents(indentLvl) +
            Format('f%0:s := f%1:sList.GetById(Value);',
            [wkFld.GetForeignKeyObjectName,
            ForeignKey.AppTable.fTable.ObjectName]) + sLineBreak;

          Result := Result + Indents(indentLvl) +
            Format('if Assigned(f%0:s) then', [wkFld.GetForeignKeyObjectName]) +
            sLineBreak;
          Result := Result + Indents(indentLvl + 1) +
            //Format('f%0:s.%1:s.RegisterProperty(Self);',
            Format('f%0:s.%1:s.Add(Self);',
            [wkFld.GetForeignKeyObjectName, wkFld.GetParentTableListName]) +
            sLineBreak;

          if (wkFld.FieldTypeStr = 'string') then
          begin
            dec(indentLvl);
            Result := Result + Indents(indentLvl) + 'end;' + sLineBreak;
          end;
        end
        else
        begin
          Result := Result + Indents(indentLvl) +
            Format('f%0:s := f%1:sList.GetById(Value);',
            [wkFld.GetForeignKeyObjectName,
            ForeignKey.AppTable.fTable.ObjectName]) + sLineBreak;
        end;
        Result := Result + indent + 'end;' + sLineBreak;

        Result := Result + indent + Format('f%0:s := Value;',
          [wkFld.EffectiveName]) + sLineBreak;
        Result := Result + 'end;' + sLineBreak + sLineBreak;
      end;
    end;
  end;

  // Add any custom methods
  Result := Result + GetTemplateCode(tmpltBaseObjectMethods);

  // Now the list object
  Result := Result + Format('{ T%sList }', [fObjectName]) + sLineBreak;
  constructorMethod := GetCreateStmnt;
  if (constructorMethod <> '') then
  begin
    Result := Result + (Format('constructor T%sList.%s',
      [fObjectName, constructorMethod])) + sLineBreak;
    Result := Result + 'begin' + sLineBreak;
    Result := Result + indent + 'inherited Create(Table, AOwnsObjects);' +
      sLineBreak;

    Result := Result + GetAssignFKClassLists;
    if HasNumericKey then
      Result := Result + indent + 'Self.HasIntId := True;' + sLineBreak;
    Result := Result + 'end;' + sLineBreak + sLineBreak;

    setLists := '';
    for wkAppTable in fForeignKeyTables do
    begin
      if setLists = '' then
        setLists := 'inItem.SetLists('
      else
        setLists := setLists + ',' + sLineBreak + Indents(1);
      setLists := setLists + Format('f%sList', [wkAppTable.fTable.ObjectName]);
    end;

    if (setLists <> '') then
    begin
      setLists := setLists + ');';

      Result := Result +
        (Format('procedure T%0:sList.AddItem(Fdc: TFDConnection; inItem: T%0:s);',
        [fObjectName])) + sLineBreak;
      Result := Result + 'begin' + sLineBreak;
      Result := Result + indent + 'inherited AddItem(Fdc, inItem);' +
        sLineBreak;
      Result := Result + indent + setLists + sLineBreak;
      Result := Result + 'end;' + sLineBreak + sLineBreak;

      Result := Result +
        (Format('procedure T%0:sList.LoadFields(var inItem: T%0:s; Fields: TFields);',
        [fObjectName])) + sLineBreak;
      Result := Result + 'begin' + sLineBreak;
      Result := Result + indent + 'inherited LoadFields(inItem, Fields);' +
        sLineBreak;
      Result := Result + indent + setLists + sLineBreak;
      Result := Result + 'end;' + sLineBreak + sLineBreak;
    end;
  end;

  Result := Result + GetTemplateCode(tmpltDbBaseObjectListMethods);
  // Result := TrimRight(Result);
end;

function TAppTable.GetInterfaceClause: string;
var
  // wkDataType: string;
  wkFld: TAppField;
  ws: string;
  properties: string;
  methods: string;
  constructorMethod: string;
  listPrivateVars: string;
  ForeignKey: TAppField;
  fkLists: string;
  referencedField: TAppField;
  wkPropertyName: string;
begin
  Result := '';
  properties := '';
  methods := '';
  listPrivateVars := '';
  // First the object class
  if (Self.ObjectAttributes <> '') then
    Result := Result + Indents + Self.ObjectAttributes + sLineBreak;
  Result := Result + Indents + Format('T%s = class(%s)',
    [fObjectName, fOption.BaseClass]) + sLineBreak;
  Result := Result + Indents + 'strict private' + sLineBreak;

  for wkFld in fFields do
  begin
    if (wkFld.FieldName <> 'LclCreated') and
      (wkFld.FieldName <> 'LclLastUpdated') then
    begin
      if (wkFld.ColumnAlias = '') then
        wkPropertyName := wkFld.FieldName
      else
        wkPropertyName := wkFld.ColumnAlias;

      ws := 'f' + wkPropertyName + ': ';
      Result := Result + Indents(2) + ws + wkFld.FieldTypeStr + ';' +
        sLineBreak;

      // Assume first field is the index/Id...for now
      if (fKeyFieldName = '') then
      begin
        fKeyType := wkFld.FieldTypeStr;
        // fKeyFieldName := wkFld.FieldName;
        fKeyFieldName := wkPropertyName;
      end;

      // Do we have a foreign key?
      if Assigned(wkFld.ForeignKey) then
      begin
        ForeignKey := wkFld.ForeignKey;
        Result := Result + Indents(2) + Format('f%0:s: T%1:s;',
          [wkFld.GetForeignKeyObjectName, ForeignKey.AppTable.Table.ObjectName])
          + sLineBreak;

        // We only need to do this for the first foreign key that refers to a
        // given forign key table
        if (fForeignKeyTables.Contains(ForeignKey.AppTable) = False) then
        begin
          fForeignKeyTables.Add(ForeignKey.AppTable);
          Result := Result + Indents(2) + Format('f%0:sList: T%0:sList;',
            [ForeignKey.AppTable.Table.ObjectName]) + sLineBreak;

          if (listPrivateVars <> '') then
            listPrivateVars := listPrivateVars + sLineBreak;
          listPrivateVars := listPrivateVars + Indents(2) +
            Format('f%0:sList: T%0:sList;',
            [ForeignKey.AppTable.fTable.ObjectName]);
        end;

        methods := methods + Indents(2) +
          Format('procedure Set%0:s(const Value: %1:s);',
          [wkPropertyName, wkFld.FieldTypeStr]) + sLineBreak;

        if (wkFld.ColumnAlias <> '') then
          properties := properties + Indents(2) +
            Format('[StOrmAliasOf(''%s'')]', [wkFld.FieldName]) + sLineBreak;
        if (wkFld.Attributes <> '') then
          properties := properties + Indents(2) + wkFld.Attributes + sLineBreak;
        properties := properties + Indents(2) +
          Format('property %0:s: %1:s read f%0:s write Set%0:s;',
          [wkPropertyName, wkFld.FieldTypeStr]) + sLineBreak;
        properties := properties + Indents(2) +
          Format('property %0:s: T%1:s read f%0:s;',
          [wkFld.GetForeignKeyObjectName, ForeignKey.AppTable.Table.ObjectName])
          + sLineBreak;
      end
      else
      begin
        if (wkFld.ColumnAlias <> '') then
          properties := properties + Indents(2) +
            Format('[StOrmAliasOf(''%s'')]', [wkFld.FieldName]) + sLineBreak;
        if (wkFld.Attributes <> '') then
          properties := properties + Indents(2) + wkFld.Attributes + sLineBreak;
        properties := properties + Indents(2) +
          Format('property %0:s: %1:s read f%0:s write f%0:s;',
          [wkPropertyName, wkFld.FieldTypeStr]) + sLineBreak;
      end;
    end;
  end;

  // Are we referenced as a foreign key?
  for referencedField in ReferencedObjects do
  begin
    Result := Result + Indents(2) + Format('f%0:s: %1:s<T%2:s>;',
      [referencedField.GetParentTableListName, fOption.PropertiesClassList,
      referencedField.AppTable.fTable.ObjectName]) + sLineBreak;
    properties := properties + Indents(2) +
      Format('property %0:s: %1:s<T%2:s> read f%0:s;',
      [referencedField.GetParentTableListName, fOption.PropertiesClassList,
      referencedField.AppTable.fTable.ObjectName]) + sLineBreak;
  end;

  Result := Result + GetTemplateCode(tmpltBaseObjectStrictPrivate) + methods;

  Result := Result + Indents + 'private' + sLineBreak +
    GetTemplateCode(tmpltBaseObjectPrivate);

  Result := Result + Indents + 'published' + sLineBreak;
  if GetFKClassLists <> '' then
    Result := Result + Indents(2) + 'procedure ResetLinkedObjects; override;' +
      sLineBreak;
  Result := Result + GetTemplateCode(tmpltBaseObjectPublished);

  Result := Result + Indents + 'public' + sLineBreak;

  ws := GetTemplateCode(tmpltBaseObjectConstructor);
  if (ws <> '') then
  begin
    Result := Result + Indents(2) + 'constructor Create;' + sLineBreak;
  end;

  Result := Result + Indents(2) + 'destructor Destroy; override;' + sLineBreak;
  Result := Result + Indents(2) + 'function GetId: string; override;' +
    sLineBreak;
  if HasNumericKey then
    Result := Result + Indents(2) + 'function GetIdInt: Int64; override;' +
      sLineBreak;

  Result := Result + GetTemplateCode(tmpltBaseObjectPublic);

  // Are we referenced as a foreign key?
  if (ReferencedObjects.count > 0) then
    Result := Result + Indents(2) + 'procedure OnCreate; override;' +
      sLineBreak;

  // If we have foreign keys
  fkLists := GetFKClassLists;
  if fkLists <> '' then
  begin
    Result := Result + Indents(2) + Format('procedure SetLists(%s);', [fkLists])
      + sLineBreak;
  end;
  Result := Result + properties + Indents + 'end;' + sLineBreak + sLineBreak;

  // Now the list class
  if (Self.ListAttributes <> '') then
    Result := Result + Indents + Self.ListAttributes + sLineBreak;
  Result := Result + Indents + Format('T%0:sList = class(%1:s<T%0:s>)',
    [fObjectName, fOption.BaseClassList]) + sLineBreak;
  Result := Result + Indents + 'private' + sLineBreak;
  if (listPrivateVars <> '') then
    Result := Result + listPrivateVars + sLineBreak;
  Result := Result + GetTemplateCode(tmpltDbBaseObjectListPrivate);

  Result := Result + Indents + 'published' + sLineBreak +
    GetTemplateCode(tmpltDbBaseObjectListPublished);
  constructorMethod := GetCreateStmnt(True);
  if (fForeignKeyTables.count > 0) then
  begin
    Result := Result + Indents(2) +
      Format('procedure LoadFields(var inItem: T%0:s; Fields: TFields); override;',
      [fObjectName]) + sLineBreak;
    Result := Result + Indents(2) +
      Format('procedure AddItem(Fdc: TFDConnection; inItem: T%0:s); override;',
      [fObjectName]) + sLineBreak;
  end;

  Result := Result + Indents + 'public' + sLineBreak +
    GetTemplateCode(tmpltDbBaseObjectListPublic);
  if (constructorMethod <> '') then
    Result := Result + Indents(2) + 'constructor ' + constructorMethod +
      sLineBreak;

  Result := Result + Indents + 'end;' + sLineBreak;
end;

function TAppTable.GetTemplateCode(TemplatePoint_Id: integer): string;
var
  wkTableTemplate: TTableTemplate;
begin
  wkTableTemplate := fTableTemplateList.FindTemplate(fTable.Table_Id,
    TemplatePoint_Id);
  if not Assigned(wkTableTemplate) then
    Result := ''
  else
    Result := TrimRight(wkTableTemplate.TemplateCode);
  if Result <> '' then
    Result := Result + sLineBreak;
end;

{ TAppForeignKey }

// constructor TAppForeignKey.Create(Field: TAppField; ParentTableName: string;
// ParentFieldName: string);
// begin
// inherited Create;
// // fField := Field;
// // fParentTableName := ParentTableName;
// // fParentFieldName := ParentFieldName;
// end;

{ TAppFieldList }

function TAppFieldList.FindField(FieldName: string): TAppField;
var
  wkField: TAppField;
begin
  for wkField in Self do
  begin
    if wkField.FieldName = FieldName then
      Exit(wkField);
  end;
  Result := nil;
end;

{ TAppTables }

function TAppTables.FindTable(TableName: string): TAppTable;
var
  wkTable: TAppTable;
begin
  for wkTable in Self do
  begin
    if (CompareText(wkTable.TableName, TableName) = 0) then
      Exit(wkTable);
  end;
  Result := nil;
end;

function TAppTables.GetAllTablesImplementation(const ClassName: string;
  const BaseClassName: string; const BaseClassListName: string): string;
var
  wkTable: TAppTable;
  wkField: TAppField;
  ix: integer;
  wkAppTable: TAppTable;
begin
  Result := Format('{ T%s }', [ClassName]) + sLineBreak;
  Result := Result + (Format('constructor T%s.Create;', [ClassName])) +
    sLineBreak;
  Result := Result + 'begin' + sLineBreak + indent + 'inherited;' + sLineBreak +
    Indents(1) + 'fAllTablesList := TList.Create;' + sLineBreak;

  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + indent + Format('f%0:sList := T%0:sList.Create(''%1:s''',
      [wkTable.ObjectName, wkTable.TableName]);
    for wkAppTable in wkTable.ForeignKeyTables do
    begin
      Result := Result + Format(', f%0:sList', [wkAppTable.ObjectName]);
    end;
    Result := Result + ');' + sLineBreak + Indents(1) +
      Format('fAllTablesList.Add(f%0:sList);', [wkTable.ObjectName]) +
      sLineBreak;
  end;
  Result := Result + 'end;' + sLineBreak + sLineBreak;

  Result := Result + Format('destructor T%s.Destroy;', [ClassName]) +
    sLineBreak;
  Result := Result + 'begin' + sLineBreak + GetDestructorClause + Indents(1) +
    'fAllTablesList.Free;' + sLineBreak + 'end;' + sLineBreak + sLineBreak;

  Result := Result + Format('procedure T%s.LoadAllTables(Fdc: TFDConnection;' +
    '%s%sSetAsDefaultFdc: Boolean);',
    [ClassName, sLineBreak, Indents(2)]) + sLineBreak + 'begin' + sLineBreak;
  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + indent + Format('if not f%0:sList.Loaded then' +
      sLineBreak, [wkTable.ObjectName]) + indent + 'begin' + sLineBreak +
      Indents(2) + Format('f%0:sList.LoadAllItems(Fdc);%s',
      [wkTable.ObjectName, sLineBreak]) +
      Indents(2) + 'if SetAsDefaultFdc then'  + sLineBreak +
      Indents(3) + Format('f%0:sList.DfltConnection := Fdc;', [wkTable.ObjectName]) +
      sLineBreak + indent + 'end;' + sLineBreak;
  end;
  Result := Result + 'end;' + sLineBreak + sLineBreak;

  Result := Result + Format('procedure T%s.UnLoadAllTables;', [ClassName]) +
    sLineBreak + 'var' + sLineBreak + Indents + 'ix: integer;' + sLineBreak +
    'begin' + sLineBreak;
  Result := Result + Indents + 'for ix := fAllTablesList.Count - 1 downto 0 do'
    + sLineBreak;
  Result := Result + Indents(1) + 'begin' + sLineBreak;
  Result := Result + Indents(2) + Format('%s<%s>(fAllTablesList[ix]).Clear;',
    [BaseClassListName, BaseClassName]) + sLineBreak;
  Result := Result + Indents(1) + 'end;' + sLineBreak;
  Result := Result + 'end;' + sLineBreak + sLineBreak;

  Result := Result +
    Format('procedure T%s.SetAllTablesDefaultConnection(Fdc: TFDConnection);',
    [ClassName]) + sLineBreak + 'var' + sLineBreak + Indents + 'ix: integer;' +
    sLineBreak + 'begin' + sLineBreak;
  Result := Result + Indents + 'for ix := 0 to fAllTablesList.Count - 1 do' +
    sLineBreak;
  Result := Result + Indents(2) +
    Format('%s<%s>(fAllTablesList[ix]).DfltConnection := Fdc;',
    [BaseClassListName, BaseClassName]) + sLineBreak;
  Result := Result + 'end;' + sLineBreak + sLineBreak;

end;

function TAppTables.GetAllTablesInterface(const ClassName: string;
  const BaseClassListName: string): string;
var
  wkTable: TAppTable;
  ix: integer;
begin
  Result := Indents + Format('T%s = class' + sLineBreak + Indents +
    'strict private', [ClassName]) + sLineBreak;
  Result := Result + Indents(2) + 'fAllTablesList: TList;' + sLineBreak;

  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + Indents(2) + Format('f%0:sList: T%0:sList;',
      [wkTable.ObjectName]) + sLineBreak;
  end;

  Result := Result + Indents + 'public' + sLineBreak + Indents(2) +
    'constructor Create;' + sLineBreak + Indents(2) +
    'destructor Destroy; override;' + sLineBreak + Indents(2) +
    'procedure LoadAllTables(Fdc: TFDConnection; ' +
    'SetAsDefaultFdc: Boolean = False);' + sLineBreak + Indents(2) +
    'procedure UnLoadAllTables;' + sLineBreak + Indents(2) +
    'procedure SetAllTablesDefaultConnection(Fdc: TFDConnection);' + sLineBreak
    + Indents(2) + 'property AllTablesList: TList read fAllTablesList;' +
    sLineBreak;

  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + Indents(2) + Format('property %0:sList: T%0:sList ' +
      'read f%0:sList;', [wkTable.ObjectName]) + sLineBreak;
  end;

  Result := Result + Indents + 'end;' + sLineBreak;
end;

function TAppTables.GetConstructorClause: string;
var
  wkTable: TAppTable;
  wkField: TAppField;
  ix: integer;
begin
  Result := '';
  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + indent +
      Format('property %0:sList : T%0:sList read f%0:sList;',
      [wkTable.ObjectName]) + sLineBreak;
  end;

  Result := Result + sLineBreak;

  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + indent + Format('f%0:sList := T%0:sList.Create(''%1:s''',
      [wkTable.ObjectName, wkTable.TableName]);
    for wkField in wkTable.ForeignKeys do
    begin
      Result := Result + Format(', f%0:sList',
        [wkField.ForeignKey.AppTable.ObjectName]);
    end;
    Result := Result + ');' + sLineBreak;

  end;
  Result := Result + sLineBreak;

  for ix := 0 to Self.count - 1 do
  begin
    wkTable := Self[ix];
    Result := Result + indent +
      Format('AllAppTables.%0:sList.LoadAllItems(fFdc);', [wkTable.ObjectName])
      + sLineBreak;
  end;
end;

function TAppTables.GetDestructorClause: string;
var
  wkTable: TAppTable;
  ix: integer;
begin
  Result := '';
  for ix := Self.count - 1 downto 0 do
  begin
    wkTable := Self[ix];
    Result := Result + indent + Format('f%0:sList.Free;', [wkTable.ObjectName])
      + sLineBreak;
  end;
end;

function TAppTables.GetImplementationClause: string;
var
  wkTable: TAppTable;
begin
  Result := '';
  for wkTable in Self do
  begin
    Result := Result + wkTable.ImplementationClause + sLineBreak;
  end;
end;

function TAppTables.GetInterfaceClause: string;
var
  wkTable: TAppTable;
begin
  Result := 'type' + sLineBreak;
  Self.SortTables;
  // Forward declarations
  for wkTable in Self do
  begin
    if wkTable.ForeignKeys.count > 0 then
      Result := Result + Indents + Format('T%s = class;', [wkTable.ObjectName])
        + sLineBreak;
  end;
  Result := Result + sLineBreak;

  // Now the class interfaces
  for wkTable in Self do
  begin
    Result := Result + wkTable.InterfaceClause + sLineBreak;
  end;
end;

procedure TAppTables.SortTables;
var
  wkUnsorted: TAppTables;
  wkSorted: TAppTables;
  wkTable: TAppTable;
  wkFK: TAppField;
  allMoved: Boolean;
  ix: integer;
begin
  wkUnsorted := TAppTables.Create(False);
  wkSorted := TAppTables.Create(False);
  try
    for wkTable in Self do
    begin
      wkUnsorted.Add(wkTable);
    end;

    while (wkUnsorted.count > 0) do
    begin
      for wkTable in wkUnsorted do
      begin
        if (wkTable.ForeignKeys.count = 0) then
          // Move to sorted list if there's no foreign keys
          allMoved := True
        else
        // Or if all foreign keys have already been moved
        begin
          allMoved := True;
          for wkFK in wkTable.ForeignKeys do
            if (wkSorted.FindTable(wkFK.ParentTableName) = nil) then
            begin
              allMoved := False;
              break;
            end;
        end;

        if allMoved then
        begin
          wkSorted.Add(wkTable);
          wkUnsorted.Remove(wkTable);
          break;
        end;
      end;
    end;

    // Now resequence our list
    for ix := 0 to wkSorted.count - 1 do
    begin
      wkSorted[ix].SortSeqn := ix;
    end;

    Self.Sort(TComparer<TAppTable>.Construct(
      function(const L, R: TAppTable): integer
      begin
        if L.SortSeqn > R.SortSeqn then
          Result := 1
        else if L.SortSeqn < R.SortSeqn then
          Result := -1
        else
          Result := 0;
      end));

  finally
    wkUnsorted.Free;
    wkSorted.Free;
  end;
end;

procedure TAppTables.SynchForeignKeys;
var
  currTable: TAppTable;
  currField: TAppField;
  forTable: TAppTable;
  forField: TAppField;
begin
  for currTable in Self do
  begin
    for currField in currTable.Fields do
    begin
      if (currField.ParentTableName <> '') then
      begin
        forTable := Self.FindTable(currField.ParentTableName);
        if Assigned(forTable) then
        begin
          forField := forTable.Fields.FindField(currField.ParentFieldName);
          currField.ForeignKey := forField;
          if not currField.OneWayLinkToForeignKey then
            forTable.ReferencedObjects.Add(currField);
        end
        else
        begin
          raise Exception.Create
            (Format('Foreign key table %s not found while processing %s.%s!',
            [currField.ParentTableName, currTable.TableName,
            currField.FieldName]));
        end;
        currTable.ForeignKeys.Add(currField)
      end;
    end;
  end;
end;

end.
