unit frmOptions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, FireDAC.Comp.Client,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, clsGenerator;

type
  TFormOptions = class(TForm)
    Label2: TLabel;
    cbxBaseClass: TComboBox;
    Button1: TButton;
    Label1: TLabel;
    cbxDbListClass: TComboBox;
    Label3: TLabel;
    cbxPropertyListClass: TComboBox;
    Label4: TLabel;
    edtUnitName: TEdit;
    Label5: TLabel;
    mmoInterface: TMemo;
    Label6: TLabel;
    mmoImplementation: TMemo;
    procedure cbxBaseClassChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure mmoKeyPress(Sender: TObject; var Key: Char);
  private
    fOptions: TOptionsList;
    fOption: TOptions;
    fFdc: TFDConnection;
    procedure SetOptions(const Value: TOptionsList);
  public
    property fdc: TFDConnection write fFdc;
    property Options: TOptionsList write SetOptions;
  end;

var
  FormOptions: TFormOptions;

implementation

{$R *.dfm}
{ TFormOptions }

procedure TFormOptions.Button1Click(Sender: TObject);
begin
  if (fOption.UnitName <> edtUnitName.Text) then
    cbxBaseClassChange(Sender);
  Close;
end;

procedure TFormOptions.cbxBaseClassChange(Sender: TObject);
begin
  fOption.BaseClass := cbxBaseClass.Text;
  fOption.BaseClassList := cbxDbListClass.Text;
  fOption.PropertiesClassList := cbxPropertyListClass.Text;
  fOption.UnitName := Trim(edtUnitName.Text);
  fOption.InterfaceUses := mmoInterface.Lines.Text;
  fOption.ImplementationUses := mmoImplementation.Lines.Text;
  fOptions.UpdateDb(fOption, fFdc);
end;

procedure TFormOptions.mmoKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = ^a) and (Sender is TMemo) then
  begin
    (Sender as TMemo).SelectAll;
    Key := #0;
  end;
end;

procedure TFormOptions.SetOptions(const Value: TOptionsList);
begin
  fOptions := Value;
  fOption := fOptions[0];

  if (cbxBaseClass.Items.IndexOf(fOption.BaseClass) < 0) then
    cbxBaseClass.Items.Add(fOption.BaseClass);
  if (cbxDbListClass.Items.IndexOf(fOption.BaseClassList) < 0) then
    cbxDbListClass.Items.Add(fOption.BaseClassList);
  if (cbxPropertyListClass.Items.IndexOf(fOption.PropertiesClassList) < 0) then
    cbxPropertyListClass.Items.Add(fOption.PropertiesClassList);

  cbxBaseClass.ItemIndex := cbxBaseClass.Items.IndexOf(fOption.BaseClass);
  cbxDbListClass.ItemIndex := cbxDbListClass.Items.IndexOf
    (fOption.BaseClassList);
  cbxPropertyListClass.ItemIndex := cbxPropertyListClass.Items.IndexOf
    (fOption.PropertiesClassList);
  edtUnitName.Text := fOption.UnitName;
  mmoInterface.Lines.Text := fOption.InterfaceUses;
  mmoImplementation.Lines.Text := fOption.ImplementationUses;
end;

end.
