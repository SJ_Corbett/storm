unit frmAutoFill;

interface

uses
  System.Classes, Vcl.Forms, Vcl.StdCtrls;

type
  TFormAutoFill = class(TForm)
  private
    fAutoFillObject: TObject;
    procedure SetAutoFillObject(const Value: TObject);
    procedure SetFieldDtls(TheOwner: TComponent; TheObject: TObject);
  public
    property AutoFillObject: TObject read fAutoFillObject
      write SetAutoFillObject;
  end;

implementation

uses
  System.SysUtils, System.Rtti, System.TypInfo;

{ TFormAutoFill }

procedure TFormAutoFill.SetAutoFillObject(const Value: TObject);
begin
  fAutoFillObject := Value;
  SetFieldDtls(Self, Value);
end;

procedure TFormAutoFill.SetFieldDtls(TheOwner: TComponent; TheObject: TObject);

  function FindValueByName(TheObject: TObject; const PropertyName: string;
    var ResultString: string; var ResultBool: Boolean;
    RttiType: TRttiType): Boolean;
  var
    wkProp: TRttiProperty;
  begin

    Result := False;
    ResultString := '';
    ResultBool := False;

    for wkProp in RttiType.GetProperties do
    begin
      if (wkProp.Visibility in [TMemberVisibility.mvPublished,
        TMemberVisibility.mvPublic]) and (wkProp.IsWritable) and
        (wkProp.IsReadable) and (CompareText(wkProp.Name, PropertyName) = 0)
      then
      begin
        case wkProp.PropertyType.TypeKind of
          TTypeKind.tkChar, TTypeKind.tkString, TTypeKind.tkLString,
            TTypeKind.tkUString:
            ResultString := wkProp.GetValue(TheObject).AsString;
          TTypeKind.tkWString, TTypeKind.tkWChar:
            ResultString := wkProp.GetValue(TheObject).AsString;

          TTypeKind.tkInteger, TTypeKind.tkInt64:
            ResultString := intToStr(wkProp.GetValue(TheObject).AsInteger);

          TTypeKind.tkFloat:
            ResultString :=
              Format('%.5n', [wkProp.GetValue(TheObject).AsExtended]);
          TTypeKind.tkEnumeration:
            begin
              if wkProp.PropertyType.Name = 'Boolean' then
                ResultBool := wkProp.GetValue(TheObject).AsBoolean
              else
              begin
                // wkTv := wkProp.GetValue(TStormBaseClass(Item));
                // wkParam.AsInteger := wkTv.AsOrdinal;
                ResultString := intToStr(wkProp.GetValue(TheObject).AsOrdinal);
              end;
            end;
        end;
        Result := True;
        break;
      end;
    end;
  end;

var
  ix: integer;
  wkComponent: TComponent;
  fRttiType: TRttiType;
  Context: TRTTIContext;
  theStringValue: string;
  theBoolValue: Boolean;
  compPfx: string;

begin
  fRttiType := Context.GetType(TheObject.ClassInfo);
  for ix := 0 to TheOwner.ComponentCount - 1 do
  begin
    wkComponent := TheOwner.Components[ix];
    if (length(wkComponent.Name) > 3) then
    begin
      compPfx := string(wkComponent.Name).SubString(0, 3).ToUpper;
      if (compPfx = 'EDT') or (compPfx = 'CBX') then
      begin
        if FindValueByName(TheObject, string(wkComponent.Name).SubString(3),
          theStringValue, theBoolValue, fRttiType) then
        begin
          if (wkComponent is TEdit) then
            TEdit(wkComponent).Text := theStringValue
          else if (wkComponent is TCheckBox) then
            TCheckBox(wkComponent).Checked := theBoolValue;
        end;
      end;
    end;
    SetFieldDtls(wkComponent, TheObject);
  end;
end;

end.
