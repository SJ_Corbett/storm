unit appConstants;

interface

const
  tmpltBaseObjectPrivate = 10;
  tmpltBaseObjectStrictPrivate = 20;
  tmpltBaseObjectPublished = 30;
  tmpltBaseObjectPublic = 40;
  tmpltBaseObjectMethods = 50;
  tmpltBaseObjectConstructor = 80;
  tmpltBaseObjectDestructor = 90;
  tmpltDbBaseObjectListPrivate = 110;
  tmpltDbBaseObjectListStrictPrivate = 120;
  tmpltDbBaseObjectListPublished = 130;
  tmpltDbBaseObjectListPublic = 140;
  tmpltDbBaseObjectListMethods = 150;

implementation

end.
