﻿unit clsGenerator;

interface

uses
  System.SysUtils, System.Classes, Data.Db, FireDAC.Comp.Client, stormBaseClasses;

type
  TColumns = class;
  TTableTemplate = class;

  TOptions = class(TStormBaseClass)
  strict private
    fOption_Id: integer;
    fBaseClass: string;
    fBaseClassList: string;
    fPropertiesClassList: string;
    fVersion: integer;
    fUnitName: string;
    fInterfaceUses: string;
    fImplementationUses: string;
  private
  published
  public
    destructor Destroy; override;
    function GetId: string; override;
    function GetIdInt: Int64; override;
    property Option_Id: integer read fOption_Id write fOption_Id;
    property BaseClass: string read fBaseClass write fBaseClass;
    property BaseClassList: string read fBaseClassList write fBaseClassList;
    property PropertiesClassList: string read fPropertiesClassList
      write fPropertiesClassList;
    property Version: integer read fVersion write fVersion;
    property UnitName: string read fUnitName write fUnitName;
    property InterfaceUses: string read fInterfaceUses write fInterfaceUses;
    property ImplementationUses: string read fImplementationUses
      write fImplementationUses;
  end;

  TOptionsList = class(TStormBaseClassDbList<TOptions>)
  public
    constructor Create(Table: string;  AOwnsObjects: Boolean = True);
  end;

  TTables = class(TStormBaseClass)
  strict private
    fTable_Id: string;
    fTable_Name: string;
    fObjectName: string;
    fIgnoreTable: Boolean;
    fListAttributes: string;
    fObjectAttributes: string;
    fColumnsList: TStormPropertiesList<TColumns>;
    fTableTemplatesList: TStormPropertiesList<TTableTemplate>;
  private
  published
  public
    destructor Destroy; override;
    function GetId: string; override;
    function FindColumn(ColumnName: string): TColumns;
    procedure OnCreate; override;
    property Table_Id: string read fTable_Id write fTable_Id;
    property Table_Name: string read fTable_Name write fTable_Name;
    property ObjectName: string read fObjectName write fObjectName;
    property IgnoreTable: Boolean read fIgnoreTable write fIgnoreTable;
    property ListAttributes: string read fListAttributes write fListAttributes;
    property ObjectAttributes: string read fObjectAttributes
      write fObjectAttributes;
    property ColumnsList: TStormPropertiesList<TColumns> read fColumnsList;
    property TableTemplatesList: TStormPropertiesList<TTableTemplate>
      read fTableTemplatesList;
  end;

  TTablesList = class(TStormBaseClassDbList<TTables>)
  private
  published
  public
    function FindTable(TableName: string): TTables;
  end;

  TColumns = class(TStormBaseClass)
  strict private
    fColumnId: string;
    fTable_Id: string;
    fTables: TTables;
    fTablesList: TTablesList;
    fColumnName: string;
    fIgnoreColumn: Boolean;
    fColumnAlias: string;
    fObjectName: string;
    fObjectListName: string;
    fAttributes: string;
    fOneWayLinkToForeignKey: Boolean;
    procedure SetTable_Id(const Value: string);
  private
  published
    procedure ResetLinkedObjects; override;
  public
    destructor Destroy; override;
    function GetId: string; override;
    procedure SetLists(TablesList: TTablesList);
    property ColumnId: string read fColumnId write fColumnId;
    property Table_Id: string read fTable_Id write SetTable_Id;
    property Tables: TTables read fTables;
    property ColumnName: string read fColumnName write fColumnName;
    property IgnoreColumn: Boolean read fIgnoreColumn write fIgnoreColumn;
    property ColumnAlias: string read fColumnAlias write fColumnAlias;
    property ObjectName: string read fObjectName write fObjectName;
    property ObjectListName: string read fObjectListName write fObjectListName;
    property Attributes: string read fAttributes write fAttributes;
    property OneWayLinkToForeignKey: Boolean read fOneWayLinkToForeignKey
      write fOneWayLinkToForeignKey;
  end;

  TColumnsList = class(TStormBaseClassDbList<TColumns>)
  private
    fTablesList: TTablesList;
  published
    procedure LoadFields(var inItem: TColumns; Fields: TFields); override;
    procedure AddItem(Fdc: TFDConnection; inItem: TColumns); override;
  public
    constructor Create(Table: string; TablesList: TTablesList;
      AOwnsObjects: Boolean = True);
  end;

  TTemplatePoint = class(TStormBaseClass)
  strict private
    fTemplatePoint_Id: integer;
    fDescription: string;
    fTableTemplateList: TStormPropertiesList<TTableTemplate>;
  private
  published
  public
    destructor Destroy; override;
    function GetId: string; override;
    function GetIdInt: Int64; override;
    procedure OnCreate; override;
    property TemplatePoint_Id: integer read fTemplatePoint_Id
      write fTemplatePoint_Id;
    property Description: string read fDescription write fDescription;
    property TableTemplateList: TStormPropertiesList<TTableTemplate>
      read fTableTemplateList;
  end;

  TTemplatePointList = class(TStormBaseClassDbList<TTemplatePoint>)
  public
    constructor Create(Table: string;  AOwnsObjects: Boolean = True);
  end;

  TTableTemplate = class(TStormBaseClass)
  strict private
    fTableTemplate_Id: string;
    fTable_Id: string;
    fTables: TTables;
    fTablesList: TTablesList;
    fTemplatePoint_Id: integer;
    fTemplatePoint: TTemplatePoint;
    fTemplatePointList: TTemplatePointList;
    fTemplateCode: string;
    procedure SetTable_Id(const Value: string);
    procedure SetTemplatePoint_Id(const Value: integer);
  private
  published
    procedure ResetLinkedObjects; override;
  public
    destructor Destroy; override;
    function GetId: string; override;
    procedure SetLists(TablesList: TTablesList;
      TemplatePointList: TTemplatePointList);
    property TableTemplate_Id: string read fTableTemplate_Id
      write fTableTemplate_Id;
    property Table_Id: string read fTable_Id write SetTable_Id;
    property Tables: TTables read fTables;
    property TemplatePoint_Id: integer read fTemplatePoint_Id
      write SetTemplatePoint_Id;
    property TemplatePoint: TTemplatePoint read fTemplatePoint;
    property TemplateCode: string read fTemplateCode write fTemplateCode;
  end;

  TTableTemplateList = class(TStormBaseClassDbList<TTableTemplate>)
  private
    fTablesList: TTablesList;
    fTemplatePointList: TTemplatePointList;
  published
    procedure LoadFields(var inItem: TTableTemplate; Fields: TFields); override;
    procedure AddItem(Fdc: TFDConnection; inItem: TTableTemplate); override;
  public
    function FindTemplate(const Table_Id: string; TemplatePoint_Id: integer)
      : TTableTemplate;
    constructor Create(Table: string; TablesList: TTablesList;
      TemplatePointList: TTemplatePointList; AOwnsObjects: Boolean = True);
  end;

  TclsGenerator = class
  strict private
    fAllTablesList: TList;
    fOptionsList: TOptionsList;
    fTablesList: TTablesList;
    fColumnsList: TColumnsList;
    fTemplatePointList: TTemplatePointList;
    fTableTemplateList: TTableTemplateList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadAllTables(Fdc: TFDConnection);
    procedure UnLoadAllTables;
    property AllTablesList: TList read fAllTablesList;
    property OptionsList: TOptionsList read fOptionsList;
    property TablesList: TTablesList read fTablesList;
    property ColumnsList: TColumnsList read fColumnsList;
    property TemplatePointList: TTemplatePointList read fTemplatePointList;
    property TableTemplateList: TTableTemplateList read fTableTemplateList;
  end;

implementation

uses
  System.Generics.Defaults;

{ TclsGenerator }
constructor TclsGenerator.Create;
begin
  inherited;
  fAllTablesList := TList.Create;
  fOptionsList := TOptionsList.Create('Options');
  fAllTablesList.Add(fOptionsList);
  fTablesList := TTablesList.Create('Tables');
  fAllTablesList.Add(fTablesList);
  fColumnsList := TColumnsList.Create('Columns', fTablesList);
  fAllTablesList.Add(fColumnsList);
  fTemplatePointList := TTemplatePointList.Create('TemplatePoints');
  fAllTablesList.Add(fTemplatePointList);
  fTableTemplateList := TTableTemplateList.Create('TableTemplates', fTablesList,
    fTemplatePointList);
  fAllTablesList.Add(fTableTemplateList);
end;

destructor TclsGenerator.Destroy;
begin
  fTableTemplateList.Free;
  fTemplatePointList.Free;
  fColumnsList.Free;
  fTablesList.Free;
  fOptionsList.Free;
  fAllTablesList.Free;
end;

procedure TclsGenerator.LoadAllTables(Fdc: TFDConnection);
begin
  if not fOptionsList.Loaded then
    fOptionsList.LoadAllItems(Fdc);
  if not fTablesList.Loaded then
    fTablesList.LoadAllItems(Fdc);
  if not fColumnsList.Loaded then
    fColumnsList.LoadAllItems(Fdc);
  if not fTemplatePointList.Loaded then
    fTemplatePointList.LoadAllItems(Fdc);
  if not fTableTemplateList.Loaded then
    fTableTemplateList.LoadAllItems(Fdc);
end;

procedure TclsGenerator.UnLoadAllTables;
var
  ix: integer;
begin
  for ix := fAllTablesList.Count - 1 downto 0 do
    TStormBaseClassDbList<TStormBaseClass>(fAllTablesList[ix]).Clear;
end;

{ TOptions }
destructor TOptions.Destroy;
begin
  inherited;
end;

function TOptions.GetId: string;
begin
  Result := intToStr(fOption_Id);
end;

function TOptions.GetIdInt: Int64;
begin
  Result := fOption_Id;
end;

{ TOptionsList }

{ TTables }
destructor TTables.Destroy;
begin
  if Assigned(fColumnsList) then
    fColumnsList.Free;
  if Assigned(fTableTemplatesList) then
    fTableTemplatesList.Free;
  inherited;
end;

function TTables.GetId: string;
begin
  Result := fTable_Id;
end;

procedure TTables.OnCreate;
begin
  inherited;
  if not Assigned(fColumnsList) then
    fColumnsList := TStormPropertiesList<TColumns>.Create;
  if not Assigned(fTableTemplatesList) then
    fTableTemplatesList := TStormPropertiesList<TTableTemplate>.Create;
end;

function TTables.FindColumn(ColumnName: string): TColumns;
var
  wkItem: TColumns;
begin
  for wkItem in fColumnsList do
  begin
    if wkItem.ColumnName = ColumnName then
      Exit(wkItem);
  end;
  Result := nil;
end;

{ TTablesList }
function TTablesList.FindTable(TableName: string): TTables;
var
  wkItem: TTables;
begin
  for wkItem in Self do
  begin
    if wkItem.Table_Name = TableName then
      Exit(wkItem);
  end;
  Result := nil;
end;

{ TColumns }
destructor TColumns.Destroy;
begin
  if Assigned(fTables) then
    fTables.ColumnsList.Extract(Self);
  inherited;
end;

function TColumns.GetId: string;
begin
  Result := fColumnId;
end;

procedure TColumns.ResetLinkedObjects;
begin
  inherited;
  Table_Id := Table_Id;
end;

procedure TColumns.SetLists(TablesList: TTablesList);
begin
  fTablesList := TablesList;
  ResetLinkedObjects;
end;

procedure TColumns.SetTable_Id(const Value: string);
begin
  if Assigned(fTablesList) and
    ((Value <> fTable_Id) or (Not Assigned(fTables) and (Value <> ''))) then
  begin
    if Assigned(fTables) then
      fTables.ColumnsList.Extract(Self);
    if (Value <> '') then
    begin;
      fTables := fTablesList.GetById(Value);
      if Assigned(fTables) then
        fTables.ColumnsList.RegisterProperty(Self);
    end;
  end;
  fTable_Id := Value;
end;

{ TColumnsList }
constructor TColumnsList.Create(Table: string; TablesList: TTablesList;
  AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  fTablesList := TablesList;
end;

procedure TColumnsList.AddItem(Fdc: TFDConnection; inItem: TColumns);
begin
  inherited AddItem(Fdc, inItem);
  inItem.SetLists(fTablesList);
end;

procedure TColumnsList.LoadFields(var inItem: TColumns; Fields: TFields);
begin
  inherited LoadFields(inItem, Fields);
  inItem.SetLists(fTablesList);
end;

{ TTemplatePoint }
destructor TTemplatePoint.Destroy;
begin
  if Assigned(fTableTemplateList) then
    fTableTemplateList.Free;
  inherited;
end;

function TTemplatePoint.GetId: string;
begin
  Result := intToStr(fTemplatePoint_Id);
end;

function TTemplatePoint.GetIdInt: Int64;
begin
  Result := fTemplatePoint_Id;
end;

procedure TTemplatePoint.OnCreate;
begin
  inherited;
  if not Assigned(fTableTemplateList) then
    fTableTemplateList := TStormPropertiesList<TTableTemplate>.Create;
end;

{ TTemplatePointList }

{ TTableTemplate }
destructor TTableTemplate.Destroy;
begin
  if Assigned(fTables) then
    fTables.TableTemplatesList.Extract(Self);
  if Assigned(fTemplatePoint) then
    fTemplatePoint.TableTemplateList.Extract(Self);
  inherited;
end;

function TTableTemplate.GetId: string;
begin
  Result := fTableTemplate_Id;
end;

procedure TTableTemplate.ResetLinkedObjects;
begin
  inherited;
  Table_Id := Table_Id;
  TemplatePoint_Id := TemplatePoint_Id;
end;

procedure TTableTemplate.SetLists(TablesList: TTablesList;
  TemplatePointList: TTemplatePointList);
begin
  fTablesList := TablesList;
  fTemplatePointList := TemplatePointList;
  ResetLinkedObjects;
end;

procedure TTableTemplate.SetTable_Id(const Value: string);
begin
  if Assigned(fTablesList) and
    ((Value <> fTable_Id) or (Not Assigned(fTables) and (Value <> ''))) then
  begin
    if Assigned(fTables) then
      fTables.TableTemplatesList.Extract(Self);
    if (Value <> '') then
    begin;
      fTables := fTablesList.GetById(Value);
      if Assigned(fTables) then
        fTables.TableTemplatesList.RegisterProperty(Self);
    end;
  end;
  fTable_Id := Value;
end;

procedure TTableTemplate.SetTemplatePoint_Id(const Value: integer);
begin
  if Assigned(fTemplatePointList) then
  begin
    if Assigned(fTemplatePoint) then
      fTemplatePoint.TableTemplateList.Extract(Self);
    fTemplatePoint := fTemplatePointList.GetById(Value);
    if Assigned(fTemplatePoint) then
      fTemplatePoint.TableTemplateList.RegisterProperty(Self);
  end;
  fTemplatePoint_Id := Value;
end;

{ TTableTemplateList }
constructor TTableTemplateList.Create(Table: string; TablesList: TTablesList;
  TemplatePointList: TTemplatePointList; AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  fTablesList := TablesList;
  fTemplatePointList := TemplatePointList;
end;

procedure TTableTemplateList.AddItem(Fdc: TFDConnection;
  inItem: TTableTemplate);
begin
  inherited AddItem(Fdc, inItem);
  inItem.SetLists(fTablesList, fTemplatePointList);
end;

procedure TTableTemplateList.LoadFields(var inItem: TTableTemplate;
  Fields: TFields);
begin
  inherited LoadFields(inItem, Fields);
  inItem.SetLists(fTablesList, fTemplatePointList);
end;

function TTableTemplateList.FindTemplate(const Table_Id: string;
  TemplatePoint_Id: integer): TTableTemplate;
var
  wkItem: TTableTemplate;
begin
  for wkItem in Self do
  begin
    if (wkItem.Table_Id = Table_Id) and
      (wkItem.TemplatePoint_Id = TemplatePoint_Id) then
      Exit(wkItem);
  end;
  Result := nil;
end;

{ TOptionsList }

constructor TOptionsList.Create(Table: string; AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  Self.HasIntId := True;
end;

{ TTemplatePointList }

constructor TTemplatePointList.Create(Table: string; AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  Self.HasIntId := True;
end;

end.
