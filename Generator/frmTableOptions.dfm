object FormTableOptions: TFormTableOptions
  Left = 0
  Top = 0
  Caption = 'FormTableOptions'
  ClientHeight = 570
  ClientWidth = 944
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  DesignSize = (
    944
    570)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 928
    Height = 519
    ActivePage = TabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    object TabSheet1: TTabSheet
      AlignWithMargins = True
      Caption = 'Options'
      DesignSize = (
        914
        485)
      object Label1: TLabel
        Left = 20
        Top = 24
        Width = 92
        Height = 13
        Caption = '&Base Object Name:'
        FocusControl = edtBaseObjectName
      end
      object lblColumns: TLabel
        Left = 68
        Top = 64
        Width = 44
        Height = 13
        Caption = 'Col&umns:'
        FocusControl = lbxColumns
      end
      object Label8: TLabel
        Left = 391
        Top = 24
        Width = 106
        Height = 13
        Caption = 'Objec&t List Attributes:'
        FocusControl = edtListAttributes
      end
      object Label9: TLabel
        Left = 625
        Top = 24
        Width = 87
        Height = 13
        Caption = 'Ob&ject Attributes:'
        FocusControl = edtObjectAttributes
      end
      object edtBaseObjectName: TEdit
        Left = 120
        Top = 21
        Width = 121
        Height = 21
        TabOrder = 0
        Text = 'edtBaseObjectName'
      end
      object cbxIgnore: TCheckBox
        Left = 280
        Top = 23
        Width = 97
        Height = 17
        Caption = '&Ignore this table'
        TabOrder = 1
        OnClick = TableDataChange
      end
      object lbxColumns: TListBox
        Left = 120
        Top = 64
        Width = 185
        Height = 313
        Anchors = [akLeft, akTop, akBottom]
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbxColumnsClick
      end
      object gbxDetails: TGroupBox
        Left = 336
        Top = 64
        Width = 337
        Height = 201
        Caption = ' Column Details '
        TabOrder = 5
        object Label4: TLabel
          Left = 109
          Top = 56
          Width = 64
          Height = 13
          Caption = 'Column &Alias:'
          FocusControl = edtColumnAlias
        end
        object Label5: TLabel
          Left = 107
          Top = 83
          Width = 66
          Height = 13
          Caption = '&Object Name:'
          FocusControl = edtObjectName
        end
        object Label6: TLabel
          Left = 88
          Top = 110
          Width = 85
          Height = 13
          Caption = 'Object &List Name:'
          FocusControl = edtObjectListName
        end
        object Label7: TLabel
          Left = 83
          Top = 137
          Width = 90
          Height = 13
          Caption = 'Column Att&ributes:'
          FocusControl = edtAttributes
        end
        object cbxIgnoreColumn: TCheckBox
          Left = 100
          Top = 22
          Width = 97
          Height = 17
          Hint = 
            'If ticked, this column is not included in the generated object m' +
            'odel'
          Alignment = taLeftJustify
          Caption = 'I&gnore Column:'
          TabOrder = 0
          OnClick = cbxColumnClick
        end
        object edtColumnAlias: TEdit
          Left = 184
          Top = 53
          Width = 121
          Height = 21
          Hint = 
            'The name of the property for this column.  Note: Requires stormB' +
            'aseClasses in the interface uses clause!'
          TabOrder = 1
          Text = 'edtColumnAlias'
          OnExit = edtFieldExit
        end
        object edtObjectName: TEdit
          Left = 184
          Top = 80
          Width = 121
          Height = 21
          Hint = 'This is the name of any linked object in this object'
          TabOrder = 2
          Text = 'edtObjectName'
          OnExit = edtFieldExit
        end
        object edtObjectListName: TEdit
          Left = 184
          Top = 107
          Width = 121
          Height = 21
          Hint = 
            'This is the name of the list in the linked object that refers to' +
            ' this object'
          TabOrder = 3
          Text = 'edtObjectListName'
          OnExit = edtFieldExit
        end
        object edtAttributes: TEdit
          Left = 184
          Top = 134
          Width = 121
          Height = 21
          Hint = 'Optional column attributes.  Used for custom processing.'
          TabOrder = 4
          Text = 'edtAttributes'
          OnExit = edtFieldExit
        end
        object cbxOneWayLinkToForeignKey: TCheckBox
          Left = 24
          Top = 166
          Width = 173
          Height = 17
          Hint = 
            'If ticked, the foreign key objects are linked to this object but' +
            ' there is no reference from this object to the foreign key objec' +
            't.'
          Alignment = taLeftJustify
          Caption = 'O&ne Way Link To Foreign Keys:'
          TabOrder = 5
          OnClick = cbxColumnClick
        end
      end
      object edtListAttributes: TEdit
        Left = 507
        Top = 21
        Width = 105
        Height = 21
        Hint = 'Optional column attributes.  Used for custom processing.'
        TabOrder = 2
      end
      object edtObjectAttributes: TEdit
        Left = 718
        Top = 21
        Width = 105
        Height = 21
        Hint = 'Optional column attributes.  Used for custom processing.'
        TabOrder = 3
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Code Templates'
      ImageIndex = 1
      object Label2: TLabel
        Left = 13
        Top = 19
        Width = 75
        Height = 13
        Caption = '&Template Point:'
        FocusControl = cbxTemplates
      end
      object Label3: TLabel
        Left = 12
        Top = 51
        Width = 76
        Height = 13
        Caption = 'Template &Code:'
        FocusControl = mmoCode
      end
      object cbxTemplates: TComboBox
        Left = 96
        Top = 16
        Width = 257
        Height = 21
        Style = csDropDownList
        TabOrder = 0
        OnChange = cbxTemplatesChange
      end
      object mmoCode: TMemo
        Left = 96
        Top = 43
        Width = 797
        Height = 416
        ScrollBars = ssBoth
        TabOrder = 1
        OnKeyPress = mmoKeyPress
      end
    end
  end
  object btnOk: TButton
    Left = 760
    Top = 537
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Ok'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 861
    Top = 537
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 1
  end
end
