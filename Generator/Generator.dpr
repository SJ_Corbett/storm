program Generator;

{$R *.dres}

uses
  Vcl.Forms,
  frmMain in 'frmMain.pas' {FormMain},
  clsTable in 'clsTable.pas',
  stormBaseClasses in '..\stormBaseClasses.pas',
  stormInterfaces in '..\stormInterfaces.pas',
  XSuperJSON in '..\XSuperJSON.pas',
  XSuperObject in '..\XSuperObject.pas',
  stormFunctions in '..\stormFunctions.pas',
  clsGenerator in 'clsGenerator.pas',
  frmOptions in 'frmOptions.pas' {FormOptions},
  frmTableOptions in 'frmTableOptions.pas' {FormTableOptions},
  appConstants in 'appConstants.pas',
  frmAutoFill in 'frmAutoFill.pas';

{$R *.dres}
{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;

end.
