unit TestAppBaseClassesTeams;

interface

uses
  TestFramework, System.Generics.Collections,
  stormBaseClasses,
  System.Rtti, appClasses,

  FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.ConsoleUI.Wait,
  FireDAC.FMXUI.Wait, FireDAC.Comp.UI, Data.DB, FireDAC.Comp.Client;

type

  TestTAppBaseClassTeamList = class(TTestCase)
  strict private
    fTeamMembers: TPersonBaseList;
    fTeams: TTeamBaseList;
    fFdcLcl: TFDConnection;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    destructor Destroy; override;
    procedure TestLoadAllItems;
    procedure TestGetByIdInt;
    procedure TestCreatePerson;
    procedure TestCreateAndAddTeamToDb;
    procedure TestAddItemsFromJsonArray;
  end;

implementation

uses
  System.SysUtils, FireDAC.DApt, System.JSON;

var
  fFDGUIxWaitCursor1: TFDGUIxWaitCursor;

destructor TestTAppBaseClassTeamList.Destroy;
begin
  // if Assigned(fFDGUIxWaitCursor1) then
  // fFDGUIxWaitCursor1.Free;
  inherited;
end;

procedure TestTAppBaseClassTeamList.SetUp;
begin
  fTeams := TTeamBaseList.Create('Teams');
  fTeamMembers := TPersonBaseList.Create('Persons', fTeams);
  // fTeams := TStormPropertiesList<TTeamBase>.Create;

  fFdcLcl := TFDConnection.Create(nil);
  fFdcLcl.Params.Values['DriverID'] := 'SQLite';
  fFdcLcl.Params.Values['OpenMode'] := 'CreateUTF16';
  fFdcLcl.Params.Values['StringFormat'] := 'Unicode';
  fFdcLcl.Params.Values['DateTimeFormat'] := 'DateTime';
  fFdcLcl.Params.Values['GUIDFormat'] := 'String';
  fFdcLcl.Params.Values['LockingMode'] := 'Normal';
  fFdcLcl.Params.Values['SharedCache'] := 'False';
  fFdcLcl.Params.Database := '..\..\StOrmUnitTestDb.s3db';
end;

procedure TestTAppBaseClassTeamList.TearDown;
begin
  FreeAndNil(fTeamMembers);
  FreeAndNil(fTeams);
  fFdcLcl.Close;
  fFdcLcl.Free;
end;

procedure TestTAppBaseClassTeamList.TestAddItemsFromJsonArray;
var
  ws: string;
  ws2: string;
  wkList: TStormBaseClassList<TPersonBase>;
  wkPerson: TPersonBase;
  wkJsonArray: TJSONArray;

const
  expected0 = '{"Players":{"colums":["PersonId","TeamId","PersonName","Level",'
    + '"Gender",' +
    '"LclCreated","LclLastUpdated"],"data":[["ABC",1,"Alice",0,2,123.456,' +
    '42551.6891189583],["DEF",2,"John",0,1,0,0]';
  expected1 = expected0 + ']}}';
  expected2 = expected0 + ',["ABC",1,"Alice",0,2,123.456,' +
    '42551.6891189583],["DEF",2,"John",0,1,0,0]]}}';
begin
  // FAppBaseClassList.LoadAllItems(fFdcLcl);
  // jo := FAppBaseClassList.JSonArray;
  // try
  // ws := jo.ToString;
  // FAppBaseClassList.AddItemsFromJsonArray(ws);
  // // CheckTrue(ws.StartsWith(expected));
  // // CheckTrue(ws.EndsWith(']]}}'));
  // finally
  // jo.Free;
  // end;
  wkList := TStormBaseClassList<TPersonBase>.Create('Players');
  try
    wkPerson := TPersonBase.Create;
    wkPerson.PersonId := 'ABC';
    wkPerson.TeamId := 1;
    wkPerson.PersonName := 'Alice';
    wkPerson.Gender := agFemale;
    wkPerson.LclCreated := 123.456;
    wkPerson.LclLastUpdated := 42551.6891189583;
    wkList.Add(wkPerson);

    wkPerson := TPersonBase.Create;
    wkPerson.PersonId := 'DEF';
    wkPerson.PersonName := 'John';
    wkPerson.TeamId := 2;
    wkPerson.Gender := agMale;
    wkPerson.LclCreated := 0;
    wkPerson.LclLastUpdated := 0;
    wkList.Add(wkPerson);

    ws := wkList.GetJsonArrayString;
    CheckEquals(expected1, ws, 'Problem in GetJSONArrayString');
    wkList.AddItemsFromJsonArray(ws);
    ws := wkList.GetJsonArrayString;
    CheckEquals(expected2, ws, 'Problem in AddItemsFromJsonArray(string)');

    wkJsonArray := TJSONArray.Create;
    try
      for wkPerson in wkList do
      begin
        wkJsonArray.AddElement(wkPerson.GetJson);
      end;

      wkList.Clear;
      wkList.AddItemsFromJsonArray(wkJsonArray);
      ws2 := wkList.GetJsonArrayString;
      CheckEquals(ws, ws2, 'Problem in AddItemsFromJsonArray(TJSONArray)');
    finally
      wkJsonArray.Free;
    end;

    wkPerson := wkList.GetById('ABC');
    CheckNotNull(wkPerson, 'Unable to retrieve any Alices!');
    CheckEquals(Ord(wkPerson.Gender), Ord(agFemale), 'Alice is no longer female!');
  finally
    wkList.Free;
  end;
end;

procedure TestTAppBaseClassTeamList.TestCreateAndAddTeamToDb;
var
  wkTeam: TTeamBase;
begin
  fTeams.LoadAllItems(fFdcLcl);
  CheckEquals(2, fTeams.Count, 'Only 2 teams expected before test!');
  wkTeam := TTeamBase.Create;
  try
    wkTeam.TeamId := 3;
    wkTeam.TeamName := 'Grade C';
    fTeams.AddItem(fFdcLcl, wkTeam);
    CheckEquals(3, fTeams.Count, '3 teams now expected!');
    fTeams.DeleteDb(wkTeam, fFdcLcl);
    CheckEquals(2, fTeams.Count, 'Only 2 teams expected!');
  finally
    // wkTeam.Free;  Now owned by list!
  end;
end;

procedure TestTAppBaseClassTeamList.TestCreatePerson;
var
  wkPerson: TPersonBase;
begin
  wkPerson := TPersonBase.Create;
  try
    wkPerson.PersonId := 'ABC';
    wkPerson.PersonName := 'Alice';
    CheckEquals(wkPerson.PersonId, 'ABC');
    CheckEquals(wkPerson.PersonName, 'Alice');
    // CheckEquals(wkPerson.GetId, 'ABC');
    CheckTrue(wkPerson.FieldList.ContainsKey('LEVEL'));
    CheckTrue(wkPerson.FieldList.ContainsKey('LCLLASTUPDATED'));
  finally
    wkPerson.Free;
  end;
end;

procedure TestTAppBaseClassTeamList.TestGetByIdInt;
var
  wkTeam: TTeamBase;
begin
  fTeams.LoadAllItems(fFdcLcl);
  wkTeam := fTeams.GetById(1);
  CheckTrue(Assigned(wkTeam), 'Team NOT found by integer Id!');
  CheckEquals('Grade A', wkTeam.TeamName, 'Team name is not correct');
  CheckEquals(1, wkTeam.TeamId, 'Team number is not correct');
end;

procedure TestTAppBaseClassTeamList.TestLoadAllItems;
begin
  fTeams.LoadAllItems(fFdcLcl);
  CheckTrue(fTeams.Count > 1, 'At least 2 teams should have been loaded!');
end;

initialization

// ReportMemoryLeaksOnShutdown := True;
// Register any test cases with the test runner
RegisterTest(TestTAppBaseClassTeamList.Suite);
fFDGUIxWaitCursor1 := TFDGUIxWaitCursor.Create(nil);
fFDGUIxWaitCursor1.Provider := 'Console';

finalization

fFDGUIxWaitCursor1.Free;

end.
