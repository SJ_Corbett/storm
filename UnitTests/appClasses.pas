unit appClasses;

interface

uses
  System.Generics.Collections, System.Rtti, FireDAC.Comp.Client, Data.Db,
  stormBaseClasses;

type
  TAppGender = (agUnknown, agMale, agFemale);

  TPersonBase = class;

  TTeamBase = class(TStormBaseClass)
  strict private
    fTeamName: string;
    fTeamId: integer;
    fTeamMembers: TStormPropertiesList<TPersonBase>;
  public
    // constructor Create;
    procedure OnCreate; override;
    destructor Destroy; override;

    function GetId: string; override;
    function GetIdInt: Int64; override;
    property TeamId: integer read fTeamId write fTeamId;
    property TeamName: string read fTeamName write fTeamName;
    property TeamMembers: TStormPropertiesList<TPersonBase> read fTeamMembers;
  end;

  TTeamBaseList = class(TStormBaseClassDbList<TTeamBase>)
  published
    constructor Create(Table: string; AOwnsObjects: Boolean = True); override;
  end;

  TPersonBase = class(TStormBaseClass)
  strict private
    fPersonName: string;
    fPersonId: string;
    fLevel: integer;
    fGender: TAppGender;
    fTeamList: TTeamBaseList;
    fTeamId: integer;
    fTeam: TTeamBase;
    procedure SetTeamId(const Value: integer);
  public
    destructor Destroy; override;
    procedure SetLists(TeamList: TTeamBaseList);
    function GetId: string; override;
    property PersonId: string read fPersonId write fPersonId;
    property TeamId: integer read fTeamId write SetTeamId;
    property PersonName: string read fPersonName write fPersonName;
    property Level: integer read fLevel write fLevel;
    property Gender: TAppGender read fGender write fGender;
    property TeamList: TTeamBaseList read fTeamList;
  end;

  TPersonBaseList = class(TStormBaseClassDbList<TPersonBase>)
  private
    fTeamBaseList: TTeamBaseList;
  published
    procedure LoadFields(var inObj: TPersonBase; Fields: TFields); override;
    procedure AddItem(Fdc: TFDConnection; inItem: TPersonBase); override;
  public
    constructor Create(Table: string; TeamBaseList: TTeamBaseList;
      AOwnsObjects: Boolean = True);
  end;

  /// <sumary> TPersonAliasBase was used to test the ability to
  /// publish the fields from the database with a different property name.
  /// </summary>
  TPersonAliasBase = class(TStormBaseClass)
  strict private
    fAliasName: string;
    fPersonId: string;
    fLevel: integer;
    fGender: TAppGender;
    // fTeamList: TTeamBaseList;
    fTeamId: integer;
    // fTeam: TTeamBase;
    procedure SetTeamId(const Value: integer);
  public
    destructor Destroy; override;
    function GetId: string; override;
    procedure SetLists(TeamList: TTeamBaseList);
    property PersonId: string read fPersonId write fPersonId;
    property TeamId: integer read fTeamId write SetTeamId;
    [StOrmAliasOf('PersonName')]
    property AliasName: string read fAliasName write fAliasName;
    property Level: integer read fLevel write fLevel;
    property Gender: TAppGender read fGender write fGender;
    // property TeamList: TTeamBaseList read fTeamList;
  end;

  TPersonAliasBaseList = class(TStormBaseClassDbList<TPersonAliasBase>)
  private
    // fTeamBaseList: TTeamBaseList;
  published
    procedure LoadFields(var inObj: TPersonAliasBase; Fields: TFields);
      override;
    procedure AddItem(Fdc: TFDConnection; inItem: TPersonAliasBase); override;
  public
    constructor Create(Table: string; AOwnsObjects: Boolean = True);
  end;

  TSchedulerBaseEvent = class(TStormBaseClass)
  strict private
    fSchedulerBaseEventID: string;
    fStartDateTime: TDateTime;
    fLastUpdated: TDateTime;
    fEventType: integer;
    fResponsible: string;
    fCaption: string;
    fDescription: string;
    fEndDateTime: TDateTime;
    fInstructions: string;
    // fLclLastUpdated: TDateTime;
    fResult: integer;
    fComments: string;
    fUpdatedBy: string;
    fTrueOrFalse: Boolean;
  public
    constructor Create;
    property SchedulerBaseEventID: string read fSchedulerBaseEventID
      write fSchedulerBaseEventID;
    property EventType: integer read fEventType write fEventType;
    property StartDateTime: TDateTime read fStartDateTime write fStartDateTime;
    property EndDateTime: TDateTime read fEndDateTime write fEndDateTime;
    function GetId: string; override;
    property Caption: string read fCaption write fCaption;
    property Description: string read fDescription write fDescription;
    property Responsible: string read fResponsible write fResponsible;
    property Instructions: string read fInstructions write fInstructions;
    property Result: integer read fResult write fResult;
    property Comments: string read fComments write fComments;
    property LastUpdated: TDateTime read fLastUpdated write fLastUpdated;
    property UpdatedBy: string read fUpdatedBy write fUpdatedBy;
    // property LclLastUpdated: TDateTime read fLclLastUpdated
    // write fLclLastUpdated;
    property TrueOrFalse: Boolean read fTrueOrFalse write fTrueOrFalse;
  end;

  TSchedulerBaseEvents = class(TStormBaseClassDbList<TSchedulerBaseEvent>)
  published
    constructor Create(Table: string; AOwnsObjects: Boolean = True); override;
  end;

  TAddressTable = class(TStormBaseClass)
  strict private
    fKey_Int: integer;
    fName: string;
    fAddress_1: string;
    fAddress_2: string;
    fState: string;
    fPostcode: string;
  published
  public
    // property ID: integer read fID write fID;
    destructor Destroy; override;
    function GetId: string; override;
    function GetIdInt: Int64; override;
    property Key_Int: integer read fKey_Int write fKey_Int;
    property Name: string read fName write fName;
    property Address_1: string read fAddress_1 write fAddress_1;
    property Address_2: string read fAddress_2 write fAddress_2;
    property State: string read fState write fState;
    property Postcode: string read fPostcode write fPostcode;
  end;

  TAddressTableList = class(TStormBaseClassDbList<TAddressTable>)
  private
  published
    constructor Create(Table: string; AOwnsObjects: Boolean = True); override;
  public
  end;

  TCustomer = class(TStormBaseClass)
  strict private
    fKey_Alpha: string;
    fName: string;
    fAddress_1: string;
    fAddress_2: string;
    fState: string;
    fPostcode: string;
    fBalance: double;
    FRouteId: integer;
    FDateAccountOpened: TDate;
    FDateOfLastPurchase: TDate;
    FEarliestDeliveryTime: TTime;
    FLatestDeliveryTime: TTime;
  published
  public
    // property ID: integer read fID write fID;
    destructor Destroy; override;
    function GetId: string; override;
    property Key_Alpha: string read fKey_Alpha write fKey_Alpha;
    property Name: string read fName write fName;
    property Address_1: string read fAddress_1 write fAddress_1;
    property Address_2: string read fAddress_2 write fAddress_2;
    property State: string read fState write fState;
    property Postcode: string read fPostcode write fPostcode;
    property Balance: double read fBalance write fBalance;
    property RouteId: integer read FRouteId write FRouteId;
    property DateAccountOpened: TDate read FDateAccountOpened
      write FDateAccountOpened;
    property DateOfLastPurchase: TDate read FDateOfLastPurchase
      write FDateOfLastPurchase;
    property EarliestDeliveryTime: TTime read FEarliestDeliveryTime
      write FEarliestDeliveryTime;
    property LatestDeliveryTime: TTime read FLatestDeliveryTime
      write FLatestDeliveryTime;
  end;

  TCustomerList = class(TStormBaseClassDbList<TCustomer>)
  private
  published
  public
  end;

implementation

uses
  System.TypInfo, System.SysUtils;
{ TAppClients }

constructor TSchedulerBaseEvents.Create(Table: string; AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  // fTable := 'Clients';
end;

{ TAppClient }

constructor TSchedulerBaseEvent.Create;
begin
  inherited;
end;

{ TTeamBase }

// constructor TTeamBase.Create(Table: string;
// TeamMembers: TStormBaseClassList<TPersonBase>;
// AOwnsObjects);
// begin
// inherited Create(Table, AOwnsObjects);
// fTeamMembers := TeamMembers;
// end;

function TSchedulerBaseEvent.GetId: string;
begin
  Result := fSchedulerBaseEventID;
end;

{ TTeamBase }

// constructor TTeamBase.Create;
// begin
// inherited;
// //OnCreate;
// end;

destructor TTeamBase.Destroy;
begin
  if Assigned(fTeamMembers) then
    fTeamMembers.Free;
  inherited;
end;

function TTeamBase.GetId: string;
begin
  Result := intToStr(fTeamId);
end;

function TTeamBase.GetIdInt: Int64;
begin
  Result := fTeamId;
end;

procedure TTeamBase.OnCreate;
begin
  inherited;
  if Not Assigned(fTeamMembers) then
    fTeamMembers := TStormPropertiesList<TPersonBase>.Create;
end;

{ TPersonBase }

// constructor TPersonBase.Create;
// begin
// inherited;
// OnCreate;
// end;

destructor TPersonBase.Destroy;
begin
  if Assigned(fTeam) then
    fTeam.TeamMembers.Extract(Self);
  inherited;
end;

function TPersonBase.GetId: string;
begin
  Result := fPersonId;
end;

procedure TPersonBase.SetLists(TeamList: TTeamBaseList);
begin
  fTeamList := TeamList;
  // Trigger load of object...maybe could be done first time object is requested?
  TeamId := TeamId;
end;

procedure TPersonBase.SetTeamId(const Value: integer);
begin
  if Assigned(fTeamList) and
    ((Value <> fTeamId) or (Not Assigned(fTeam) and (Value <> 0))) then
  begin
    if Assigned(fTeam) then
      fTeam.TeamMembers.Extract(Self);
    if (Value <> 0) then
    begin
      fTeam := fTeamList.GetById(Value);
      if Assigned(fTeam) then
        fTeam.TeamMembers.RegisterProperty(Self);
    end;
  end;
  fTeamId := Value;
end;

{ TPersonBaseList }

procedure TPersonBaseList.AddItem(Fdc: TFDConnection; inItem: TPersonBase);
begin
  inherited;
  inItem.SetLists(fTeamBaseList);
end;

constructor TPersonBaseList.Create(Table: string; TeamBaseList: TTeamBaseList;
  AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  fTeamBaseList := TeamBaseList;
end;

procedure TPersonBaseList.LoadFields(var inObj: TPersonBase; Fields: TFields);
begin
  inherited;
  inObj.SetLists(fTeamBaseList);
end;

{ TPersonAliasBase }

destructor TPersonAliasBase.Destroy;
begin
  // if Assigned(fTeam) then
  // fTeam.TeamMembers.Extract(Self);
  inherited;
end;

function TPersonAliasBase.GetId: string;
begin
  Result := fPersonId;
end;

procedure TPersonAliasBase.SetLists(TeamList: TTeamBaseList);
begin
  // fTeamList := TeamList;
  TeamId := TeamId;
end;

procedure TPersonAliasBase.SetTeamId(const Value: integer);
begin
  (*
    if Assigned(fTeamList) and
    ((Value <> fTeamId) or (Not Assigned(fTeam) and (Value <> 0))) then
    begin
    if Assigned(fTeam) then
    fTeam.TeamMembers.Extract(Self);
    if (Value <> 0) then
    begin
    fTeam := fTeamList.GetById(Value);
    if Assigned(fTeam) then
    fTeam.TeamMembers.RegisterProperty(Self);
    end;
    end;
  *)
  fTeamId := Value;
end;

{ TPersonAliasBaseList }

procedure TPersonAliasBaseList.AddItem(Fdc: TFDConnection;
  inItem: TPersonAliasBase);
begin
  inherited;
  // inItem.SetLists(fTeamBaseList);
end;

constructor TPersonAliasBaseList.Create(Table: string; AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  // fTeamBaseList := TeamBaseList;
end;

procedure TPersonAliasBaseList.LoadFields(var inObj: TPersonAliasBase;
  Fields: TFields);
begin
  inherited;
  // inObj.SetLists(fTeamBaseList);
end;

{ TAddressTable }

destructor TAddressTable.Destroy;
begin
  inherited;
end;

function TAddressTable.GetId: string;
begin
  // Result := Format('%d.10f', [fKey_Int]);
  Result := intToStr(fKey_Int);
end;

function TAddressTable.GetIdInt: Int64;
begin
  Result := fKey_Int;
end;

{ TAddressTableList }

constructor TAddressTableList.Create(Table: string; AOwnsObjects: Boolean);
begin
  inherited Create(Table, AOwnsObjects);
  Self.HasIntId := True;
end;

{ TCustomer }

destructor TCustomer.Destroy;
begin
  inherited;
end;

function TCustomer.GetId: string;
begin
  Result := fKey_Alpha;
end;

{ TTeamBaseList }

constructor TTeamBaseList.Create(Table: string; AOwnsObjects: Boolean);
begin
  inherited;
  Self.HasIntId := True;
end;

end.
