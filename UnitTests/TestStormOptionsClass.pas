unit TestStormOptionsClass;

interface

uses
  TestFramework, appClasses, stormBaseClasses;

type

  TestTStormOptionsClass = class(TTestCase)
  strict private
    FPersonAliasBase: TPersonAliasBase;
  public
    // procedure SetUp; override;
    // procedure TearDown; override;
  published
    procedure TestStormOptions;
  end;

implementation

uses
  System.TimeSpan, System.Classes, System.SysUtils, System.DateUtils;

{ TestTStormOptionsClass }

procedure TestTStormOptionsClass.TestStormOptions;
var
  lStarted: TDateTime;
  lFinished: TDateTime;
  lClassTimeLocal: TDateTime;
  lClassTimeUTC: TDateTime;
  lZeroTimeSpan: TTimespan;
begin
  lStarted := now;
  lClassTimeLocal := TStormOptionsClass.GetNow;
  TStormOptionsClass.UseUtcTime := True;
  lClassTimeUTC := TStormOptionsClass.GetNow;
  lFinished := now;

  lZeroTimeSpan := TTimespan.Create(0);
  if (TTimeZone.Local.UtcOffset > lZeroTimeSpan) then
    CheckTrue(((lClassTimeLocal - lClassTimeUTC) >= (lFinished - lStarted)),
      Format('Problem with UTC time! %s %s', [FormatDateTime('dd hh:mm:ss',
      (lClassTimeLocal - lClassTimeUTC)), FormatDateTime('dd hh:mm:ss',
      (lFinished - lStarted))]))
  else if (TTimeZone.Local.UtcOffset <= lZeroTimeSpan) then
    CheckTrue(((lClassTimeLocal - lClassTimeUTC) < (lFinished - lStarted)),
      'Problem with UTC time!')
  else if (TTimeZone.Local.UtcOffset = lZeroTimeSpan) then
    CheckTrue(((lClassTimeLocal - lClassTimeUTC) <= (lFinished - lStarted)),
      'Problem with UTC time!');
end;

initialization

RegisterTest(TestTStormOptionsClass.Suite);

end.
