unit TestBaseDbClassList;

interface

uses
  TestFramework, System.Generics.Collections, appClasses, stormBaseClasses,
  TestAppClassesAlias, System.Rtti, FireDAC.Comp.Client,
  FireDAC.Phys.SQLiteVDataSet, Data.DB;

type
  // Test methods for class TestTPersonAliasBase

  TestTheBaseDbClassList = class(TTestCase)
  strict private
    fAddressTable: TFDMemTable;
    fCustomerTable: TFDMemTable;
    fFdcLcl: TFDConnection;
    fFdcLclSql: TFDLocalSQL;
    fAddressTableList: TAddressTableList;
    fCustomerList: TCustomerList;
    fOnLoadedProcString: string;
    fOnLoadingProcString: string;
    procedure TestFinding;
    procedure TheOnLoadedProc(Sender: TObject);
    procedure TheOnLoadingProc(Sender: TObject);
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestLoadSql;
    procedure TestClearDbList;
    procedure TestDeleting;
    procedure TestFindingUnSorted;
    procedure TestFindingSorted;
    procedure TestOnLoadingLoaded;
    procedure TestLoadingFromDataSet;
  end;

implementation

uses
  System.Classes, System.SysUtils, Winapi.Windows, FireDAC.Stan.Intf;

procedure TestTheBaseDbClassList.SetUp;
var
  wkQry: TFDQuery;
  ResStream: TResourceStream;
  strStream: TStringStream;

begin
  fFdcLcl := TFDConnection.Create(nil);
  fFdcLcl.Params.Values['DriverID'] := 'SQLite';

  fFdcLclSql := TFDLocalSQL.Create(nil);
  fFdcLclSql.Connection := fFdcLcl;
  fFdcLclSql.Active := True;

  fAddressTable := TFDMemTable.Create(nil);
  fAddressTable.Name := 'AddressTable';

  with fAddressTable.FieldDefs do
  begin
    with AddFieldDef do
    begin
      Name := 'Key_Int';
      DataType := ftInteger;
    end;
    with AddFieldDef do
    begin
      Name := 'Name';
      DataType := ftString;
      Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Address_1';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Address_2';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'State';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Postcode';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'LclCreated';
      DataType := ftDateTime;
    end;
    with AddFieldDef do
    begin
      Name := 'LclLastUpdated';
      DataType := ftDateTime;
    end;
  end;

  fAddressTable.LocalSQL := fFdcLclSql;
  // with fAddressTable.Indexes.Add do
  // begin
  // Name := 'Primary';
  // Fields := 'Key_Int';
  // Options := [TFDSortOption.soPrimary];
  // Distinct := True;
  // Active := True;
  // Selected := True;
  // end;

  fCustomerTable := TFDMemTable.Create(nil);
  fCustomerTable.Name := 'CustomerTable';

  with fCustomerTable.FieldDefs do
  begin
    with AddFieldDef do
    begin
      // Note not the same case as in the class definitions!
      Name := 'Key_ALPHA';
      DataType := ftString;
    end;
    with AddFieldDef do
    begin
      Name := 'Name';
      DataType := ftString;
      Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Address_1';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Address_2';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'State';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Postcode';
      DataType := ftString;
      // Size := 50;
    end;
    with AddFieldDef do
    begin
      Name := 'Balance';
      DataType := ftFloat;
    end;
    with AddFieldDef do
    begin
      Name := 'RouteId';
      DataType := ftInteger;
    end;
    with AddFieldDef do
    begin
      Name := 'LclCreated';
      DataType := ftDateTime;
    end;
    with AddFieldDef do
    begin
      Name := 'LclLastUpdated';
      DataType := ftDateTime;
    end;
  end;

  fCustomerTable.LocalSQL := fFdcLclSql;

  wkQry := TFDQuery.Create(nil);
  try
    wkQry.Connection := fFdcLcl;
    ResStream := TResourceStream.Create(hInstance, 'SqlSetUpListTest',
      RT_RCDATA);
    try
      strStream := TStringStream.Create;
      try
        strStream.CopyFrom(ResStream, 0);
        strStream.Position := 0;
        wkQry.SQL.Text := strStream.ReadString(strStream.Size);
        wkQry.ExecSQL;
      finally
        strStream.Free;
      end;
    finally
      ResStream.Free;
    end;
  finally
    wkQry.Free;
  end;

  // fAddressTableList := TAddressTableList.Create('AddressTable', fFdcLcl);
  fAddressTableList := TAddressTableList.Create('AddressTable');
  fAddressTableList.DfltConnection := fFdcLcl;
  fAddressTableList.LoadAllItems;

  fCustomerList := TCustomerList.Create('CustomerTable');
  fCustomerList.DfltConnection := fFdcLcl;
  fCustomerList.LoadAllItems;
end;

procedure TestTheBaseDbClassList.TearDown;
begin
  fAddressTableList.Free;
  fAddressTable.Free;
  fCustomerList.Free;
  fCustomerTable.Free;
  fFdcLclSql.Free;
  fFdcLcl.Free;
end;

procedure TestTheBaseDbClassList.TestClearDbList;
var
  fCustomerListCount: Integer;
begin
  fCustomerListCount := fCustomerList.Count;
  Status(Format('TestClearDbList: The Customer List has %d records',
    [fCustomerListCount]));
  CheckTrue((fCustomerList.Loaded), 'The Customer List must have been loaded!');
  CheckTrue((fCustomerListCount > 0), 'The Customer List must have records!');
  fCustomerList.Clear;
  CheckTrue((fCustomerList.Count = 0),
    'The Customer List should have been cleared!');
  CheckFalse((fCustomerList.Loaded),
    'The Customer List must have been un-loaded!');
  fCustomerList.LoadAllItems;
  CheckTrue((fCustomerList.Loaded),
    'The Customer List must have been re-loaded!');
  CheckTrue((fCustomerList.Count = fCustomerListCount),
    Format('The Customer List should have %d entries!', [fCustomerListCount]));
end;

procedure TestTheBaseDbClassList.TestDeleting;
var
  wkAddressTable: TAddressTable;
  success: Boolean;
begin
  wkAddressTable := fAddressTableList.GetById(37447);
  CheckNotNull(wkAddressTable, 'Cannot find Id 37447');
  success := fAddressTableList.DeleteDb(wkAddressTable, fAddressTableList.DfltConnection);
  CheckTrue(success, 'Id 37447 wasn''t deleted! (1)');
  wkAddressTable := fAddressTableList.GetById(37447);
  CheckNull(wkAddressTable, 'Id 37447 wasn''t deleted! (2)');

  success := fAddressTableList.DeleteDb('44457');
  CheckTrue(success, 'Id 44457 wasn''t deleted! (1)');
  wkAddressTable := fAddressTableList.GetById(37447);
  CheckNull(wkAddressTable, 'Id 44457 wasn''t deleted! (2)');

end;

procedure TestTheBaseDbClassList.TestFinding;
var
  wkAddressTable: TAddressTable;
  wkCustomer: TCustomer;
begin
  wkAddressTable := fAddressTableList.GetById(14);
  CheckNotNull(wkAddressTable,
    'Unable to find address with the lowest Id (14)');
  CheckEquals('Lowest Number', wkAddressTable.Name,
    'Wrong name returned for address 14');

  wkAddressTable := fAddressTableList.GetById(15);
  if Assigned(wkAddressTable) then
    CheckNull(wkAddressTable, 'Found address ' + wkAddressTable.Id +
      ' for Id (15)');

  wkAddressTable := fAddressTableList.GetById(47404);
  CheckNotNull(wkAddressTable,
    'Unable to find address with a middle Id (47404)');
  CheckEquals('Middling Number', wkAddressTable.Name,
    'Wrong name returned for address 47404');
  CheckEquals('3310', wkAddressTable.Postcode,
    'Wrong Postcode returned for address 47404');

  wkAddressTable := fAddressTableList.GetById(99950);
  CheckNotNull(wkAddressTable,
    'Unable to find address with the highest Id (99950)');
  CheckEquals('Highest Number', wkAddressTable.Name,
    'Wrong name returned for address 99950');
  CheckEquals('SA', wkAddressTable.State,
    'Wrong state returned for address 99950');

  wkCustomer := fCustomerList.GetById('AAA');
  CheckNotNull(wkCustomer, 'Unable to find customer with the lowest Id (AAA)');
  CheckEquals('Lowest Key', wkCustomer.Name,
    'Wrong name returned for customer AAA');
  CheckEquals('NSW', wkCustomer.State, 'Wrong state returned for customer AAA');

  wkCustomer := fCustomerList.GetById('MDI');
  CheckNotNull(wkCustomer, 'Unable to find customer with a middle Id (MDI)');
  CheckEquals('Magna Duis Industries', wkCustomer.Name,
    'Wrong name returned for customer MDI');
  CheckEquals('VIC', wkCustomer.State, 'Wrong state returned for customer MDI');

  wkCustomer := fCustomerList.GetById('ZZZ');
  CheckNotNull(wkCustomer, 'Unable to find customer with the highest Id (ZZZ)');
  CheckEquals('Highest Key', wkCustomer.Name,
    'Wrong name returned for customer ZZZ');
  CheckEquals('SA', wkCustomer.State, 'Wrong state returned for customer ZZZ');
end;

procedure TestTheBaseDbClassList.TestFindingSorted;
var
  wkAddressTable: TAddressTable;
  ix: Integer;
  wkAddress: TAddressTable;
begin
  Status('Sorting list...');
  fAddressTableList.Sorted := True;
  for ix := 0 to 1000 do
    TestFinding;

  wkAddressTable := fAddressTableList[0];
  CheckEquals(14, wkAddressTable.GetIdInt,
    'Wrong record returned for index [0]');

  wkAddressTable := fAddressTableList[fAddressTableList.Count - 1];
  CheckEquals(99950, wkAddressTable.GetIdInt,
    'Wrong record returned for index [lastOne]');

  wkAddress := fAddressTableList.CreateItem('2');
  wkAddress.Name := 'Added';

  wkAddressTable := fAddressTableList.GetById(2);
  CheckNotNull(wkAddressTable, 'Unable to find address with  Id (2)');
  CheckEquals('Added', wkAddressTable.Name,
    'Wrong name returned for address 2');

  Status('Done!');
end;

procedure TestTheBaseDbClassList.TestFindingUnSorted;
var
  wkAddressTable: TAddressTable;
  wkAddress: TAddressTable;
  ix: Integer;
begin
  Status('UnSorted list...');
  fAddressTableList.Sorted := False;
  for ix := 0 to 1000 do
    TestFinding;

  wkAddressTable := fAddressTableList[0];
  CheckEquals(19691, wkAddressTable.GetIdInt,
    'Wrong record returned for index [0]');

  wkAddressTable := fAddressTableList[fAddressTableList.Count - 1];
  CheckEquals(95145, wkAddressTable.GetIdInt,
    'Wrong record returned for index [lastOne]');

  wkAddress := fAddressTableList.CreateItem('2');
  wkAddress.Name := 'Added';

  wkAddressTable := fAddressTableList.GetById(2);
  CheckNotNull(wkAddressTable, 'Unable to find address with  Id (2)');
  CheckEquals('Added', wkAddressTable.Name,
    'Wrong name returned for address 2');
end;

procedure TestTheBaseDbClassList.TestLoadingFromDataSet;
var
  wkCustomer: TCustomer;
begin
  fAddressTableList.Clear;
  fAddressTableList.LoadAllItems(fAddressTable);
  CheckEquals(1545, fAddressTableList.Count,
    'Proplem loading AddressTableList data from memory table');

  fCustomerList.Clear;
  fCustomerList.LoadAllItems(fCustomerTable);
  CheckEquals(fCustomerTable.RecordCount, fCustomerList.Count,
    'Proplem loading CustomerList data from CustomerTable');

  wkCustomer := fCustomerList.GetById('RDI');
  CheckNotNull(wkCustomer, 'Unable to find customer with the Id (RDI)');

  CheckEquals( 45.25, wkCustomer.Balance,
    'Wrong Balance returned for customer RDI');
  CheckEquals(7, wkCustomer.RouteId,
    'Wrong RouteId returned for customer RDI');
end;

procedure TestTheBaseDbClassList.TestLoadSql;
begin
  CheckEquals(1545, fAddressTable.RecordCount,
    'Proplem loading AddressTable data into memory table');
  CheckEquals(fAddressTable.RecordCount, fAddressTableList.Count,
    'Proplem loading AddressTable into object list');
end;

procedure TestTheBaseDbClassList.TestOnLoadingLoaded;
begin
  Check(fAddressTableList.Count > 0, 'fAddressTableList Should have records!');
  fAddressTableList.Clear;
  Check(fAddressTableList.Count = 0,
    'fAddressTableList Should have no records!');
  fOnLoadedProcString := '';
  fOnLoadingProcString := '';
  fAddressTableList.OnLoading := TheOnLoadingProc;
  fAddressTableList.OnLoaded := TheOnLoadedProc;
  fAddressTableList.LoadAllItems;
  CheckEquals(fAddressTableList.TableId, fOnLoadingProcString,
    'OnLoading was NOT called!');
  CheckEquals('Loaded', fOnLoadedProcString, 'OnLoaded was NOT called!');
end;

procedure TestTheBaseDbClassList.TheOnLoadedProc(Sender: TObject);
begin
  if Sender IS TAddressTableList then
  begin
    fOnLoadedProcString := 'Loaded';
  end;
end;

procedure TestTheBaseDbClassList.TheOnLoadingProc(Sender: TObject);
begin
  if Sender IS TAddressTableList then
  begin
    fOnLoadingProcString := TAddressTableList(Sender).TableId;
  end;
end;

initialization

// Register any test cases with the test runner
RegisterTest(TestTheBaseDbClassList.Suite);

end.
