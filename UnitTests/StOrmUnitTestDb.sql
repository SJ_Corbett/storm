--
-- Use this to generate an SQLite database for unit testing.
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: LclChanges
DROP TABLE IF EXISTS LclChanges;
CREATE TABLE LclChanges (LclChangeId varchar NOT NULL, LclTableNameId int NOT NULL, RecordId varchar, DeleteRecd char (1), OriginalData text NOT NULL, PRIMARY KEY (LclChangeId));

-- Table: LclTableNames
DROP TABLE IF EXISTS LclTableNames;
CREATE TABLE LclTableNames (LclTableNameId INTEGER NOT NULL, TableName varchar (20) NOT NULL, Description varchar (40), Allow_Update char (1), LoadSequence [int] NOT NULL, PRIMARY KEY (LclTableNameId));
INSERT INTO LclTableNames (LclTableNameId, TableName, Description, Allow_Update, LoadSequence) VALUES (10, 'LclTableNames', 'System table names', 'Y', 10);
INSERT INTO LclTableNames (LclTableNameId, TableName, Description, Allow_Update, LoadSequence) VALUES (100, 'SchedulerBaseEvents', 'Scheduled base events', 'Y', 100);

-- Table: SchedulerBaseEvents
DROP TABLE IF EXISTS SchedulerBaseEvents;
CREATE TABLE SchedulerBaseEvents (SchedulerBaseEventID varchar NOT NULL, EventType [int], StartDateTime [datetime], EndDateTime [datetime], Caption varchar (40), Description text, Responsible text, Instructions text, Result [int], Comments text, LastUpdated [datetime], UpdatedBy text, LclCreated DATETIME, LclLastUpdated [datetime], PRIMARY KEY (SchedulerBaseEventID));
INSERT INTO SchedulerBaseEvents (SchedulerBaseEventID, EventType, StartDateTime, EndDateTime, Caption, Description, Responsible, Instructions, Result, Comments, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated) VALUES ('{88FC8173-F2B0-E511-B487-001C421A6668}', 1, 42371.4, 42371.5, 'SAREX 4�SARMC Directed SAR', '', 'Capt Kirk', '', 0, '', 42371.5, 'Steve', NULL, 42383.9);
INSERT INTO SchedulerBaseEvents (SchedulerBaseEventID, EventType, StartDateTime, EndDateTime, Caption, Description, Responsible, Instructions, Result, Comments, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated) VALUES ('{F8B1CB9A-F2B0-E511-B487-001C421A6668}', 1, 42371.5, 42371.6, 'BD12 BOARDEX 2', 'Boarding exercise', 'CPO Smith', '', 0, '', 42371.5, 'Steve', NULL, 42383.9);
INSERT INTO SchedulerBaseEvents (SchedulerBaseEventID, EventType, StartDateTime, EndDateTime, Caption, Description, Responsible, Instructions, Result, Comments, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated) VALUES ('{A0BDE9E3-A9B1-E511-B487-001C421A6668}', 1, 42372.4, 42372.5, 'MW05 Minehunting - Bottom Type C', '', 'Lt Small', 'Be very careful!', 0, '', 42372.4, 'Steve', NULL, 42383.9);
INSERT INTO SchedulerBaseEvents (SchedulerBaseEventID, EventType, StartDateTime, EndDateTime, Caption, Description, Responsible, Instructions, Result, Comments, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated) VALUES ('{50EBBF8D-DDB1-E511-B487-001C421A6668}', 1, 42372.7, 42372.7, 'SD02 Wharf Search', '', 'PO Jones', '', 0, '', 42372.7, 'Steve', NULL, 42383.9);

-- Table: Teams
DROP TABLE IF EXISTS Teams;
CREATE TABLE Teams ( TeamId INTEGER NOT NULL, TeamName TEXT, LastUpdated [DATETIME], UpdatedBy TEXT, LclCreated DATETIME, LclLastUpdated [DATETIME], PRIMARY KEY ( TeamId )
);
INSERT INTO Teams (TeamId, TeamName, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated) VALUES (2, 'Grade B', NULL, NULL, NULL, NULL);
INSERT INTO Teams (TeamId, TeamName, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated) VALUES (1, 'Grade A', NULL, NULL, NULL, NULL);

-- Table: Persons
DROP TABLE IF EXISTS Persons;
CREATE TABLE Persons (PersonId VARCHAR NOT NULL, TeamId INTEGER, PersonName TEXT, Level int, LastUpdated [DATETIME], UpdatedBy TEXT, LclCreated DATETIME, LclLastUpdated [DATETIME], PRIMARY KEY (PersonId));
INSERT INTO Persons (PersonId, TeamId, PersonName, Level, LastUpdated, UpdatedBy, LclCreated, LclLastUpdated)
 VALUES ('1', 1, 'Fred Smith', 1, NULL, NULL, NULL, NULL),
 ('4', 2, 'John Howard', 1, NULL, NULL, NULL, NULL),
 ('3', 2, 'Marty Feldman', 2, NULL, NULL, NULL, NULL),
 ('2', 1, 'Joe Bloggs', 3, NULL, NULL, NULL, NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
