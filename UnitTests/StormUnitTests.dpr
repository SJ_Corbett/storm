program StormUnitTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

{$R *.dres}

uses
  DUnitTestRunner,
  TestAppBaseClassesTeams in 'TestAppBaseClassesTeams.pas',
  stormBaseClasses in '..\stormBaseClasses.pas',
  stormFunctions in '..\stormFunctions.pas',
  appClasses in 'appClasses.pas',
  stormInterfaces in '..\stormInterfaces.pas',
  TestAppBaseClasses in 'TestAppBaseClasses.pas',
  XSuperJSON in '..\XSuperJSON.pas',
  XSuperObject in '..\XSuperObject.pas',
  TestAppClassesAlias in 'TestAppClassesAlias.pas',
  TestBaseDbClassList in 'TestBaseDbClassList.pas',
  TestStormOptionsClass in 'TestStormOptionsClass.pas';

{$R *.RES}

begin
  ReportMemoryLeaksOnShutdown := True;
  DUnitTestRunner.RunRegisteredTests;
end.

