﻿unit TestAppClassesAlias;

interface

uses
  TestFramework, appClasses, FireDAC.Comp.Client, System.Generics.Collections,
  stormBaseClasses, System.Rtti;

type
  // Test methods for class TPersonAliasBase

  TestTPersonAliasBase = class(TTestCase)
  strict private
    FPersonAliasBase: TPersonAliasBase;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestSetLists;
  end;
  // Test methods for class TPersonAliasBaseList

  TestTPersonAliasBaseList = class(TTestCase)
  strict private
    FPersonAliasBaseList: TPersonAliasBaseList;
    fFdcLcl: TFDConnection;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestLoadAllItems;
    procedure TestAliasUpdate;
  end;

implementation

procedure TestTPersonAliasBase.SetUp;
begin
  FPersonAliasBase := TPersonAliasBase.Create;
end;

procedure TestTPersonAliasBase.TearDown;
begin
  FPersonAliasBase.Free;
  FPersonAliasBase := nil;
end;

procedure TestTPersonAliasBase.TestSetLists;
var
  TeamList: TTeamBaseList;
begin
  // TODO: Setup method call parameters
  FPersonAliasBase.SetLists(TeamList);
  // TODO: Validate method results
end;

procedure TestTPersonAliasBaseList.SetUp;
begin
  FPersonAliasBaseList := TPersonAliasBaseList.Create('Persons');

  fFdcLcl := TFDConnection.Create(nil);
  fFdcLcl.Params.Values['DriverID'] := 'SQLite';
  fFdcLcl.Params.Values['OpenMode'] := 'CreateUTF16';
  fFdcLcl.Params.Values['StringFormat'] := 'Unicode';
  fFdcLcl.Params.Values['DateTimeFormat'] := 'DateTime';
  fFdcLcl.Params.Values['GUIDFormat'] := 'String';
  fFdcLcl.Params.Values['LockingMode'] := 'Normal';
  fFdcLcl.Params.Values['SharedCache'] := 'False';
  fFdcLcl.Params.Database := '..\..\StOrmUnitTestDb.s3db';
end;

procedure TestTPersonAliasBaseList.TearDown;
begin
  FPersonAliasBaseList.Free;
  FPersonAliasBaseList := nil;
  fFdcLcl.Close;
  fFdcLcl.Free;
end;

procedure TestTPersonAliasBaseList.TestAliasUpdate;
var
  wkItem: TPersonAliasBase;
  newName: string;
  oldName: string;
begin
  FPersonAliasBaseList.LoadAllItems(fFdcLcl);
  CheckTrue(FPersonAliasBaseList.Count > 1, 'At least 2 persons should have been loaded!');
  wkItem := FPersonAliasBaseList.GetById('3');
  CheckTrue(Assigned(wkItem), 'Unable to locate person 3, Marty Feldman');

  oldName := wkItem.AliasName;
  newName :=        'Jim Carey';
  wkItem.AliasName := newName;
  FPersonAliasBaseList.UpdateDb(wkItem, fFdcLcl);

  wkItem := FPersonAliasBaseList.GetById('3');
  CheckEquals(newName, wkItem.AliasName, 'Update of alias name failed!');

  // Put it back!
  wkItem.AliasName := oldName;
  FPersonAliasBaseList.UpdateDb(wkItem, fFdcLcl);
end;

procedure TestTPersonAliasBaseList.TestLoadAllItems;
var
  wkItem: TPersonAliasBase;
begin
  FPersonAliasBaseList.LoadAllItems(fFdcLcl);
  CheckTrue(FPersonAliasBaseList.Count > 1, 'At least 2 persons should have been loaded!');
  wkItem := FPersonAliasBaseList.GetById('3');
  CheckTrue(Assigned(wkItem), 'Unable to locate person 3, Marty Feldman');
  // For this to work we need to update StOrm to either also load/save
  // Protected properties or rename fields using property tags
  CheckEquals('Marty Feldman', wkItem.AliasName, 'Alias name is NOT Marty Feldman!');
end;


initialization
  // Register any test cases with the test runner
  RegisterTest(TestTPersonAliasBase.Suite);
  RegisterTest(TestTPersonAliasBaseList.Suite);
end.

