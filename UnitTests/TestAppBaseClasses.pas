unit TestappBaseClasses;

interface

uses
  TestFramework, System.Generics.Collections,
  stormBaseClasses,
  System.Rtti, appClasses,

  FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.ConsoleUI.Wait,
  FireDAC.FMXUI.Wait, FireDAC.Comp.UI, Data.DB, FireDAC.Comp.Client;

type
  TestTAppBaseClass = class(TTestCase)
  strict private
    FAppBaseClass: TSchedulerBaseEvent;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestAssign;
    procedure TestBaseClassToJSon;
    procedure TestBaseClassToJSonWithOptions;
    procedure TestBaseClassJSonArray;
    procedure TestBaseClassCreationFromJSON;
    procedure TestGetJsonString;
    procedure TestToSeperator;
    procedure TestSetFieldValueByString;
  end;

  TestTAppBaseClassList = class(TTestCase)
  strict private
    FAppBaseClassList: TStormBaseClassDbList<TSchedulerBaseEvent>;
    fFdcLcl: TFDConnection;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    destructor Destroy; override;
    procedure TestLoadAllItems;
    procedure TestCreateFromSeperated;
    procedure TestCreateItem;
    procedure TestEnumeration;
    procedure TestFieldList;
    procedure TestGetById;
    procedure TestGetJsonArray;
    procedure TestGetJsonString;
    procedure TestJSON;
    procedure TestJSONArray;
    procedure TestToSeperator;
    procedure TestUpdateDb;
  end;

implementation

uses
  System.SysUtils, System.Math, FireDAC.DApt, System.JSON, System.JSON.Writers,
  System.DateUtils;

var
  fFDGUIxWaitCursor1: TFDGUIxWaitCursor;

procedure TestTAppBaseClass.SetUp;
begin
  FAppBaseClass := TSchedulerBaseEvent.Create;
  FAppBaseClass.StartDateTime := 42442;
  FAppBaseClass.EndDateTime := FAppBaseClass.StartDateTime + 1;
  FAppBaseClass.Caption := 'My Event Caption';
  FAppBaseClass.EventType := 3;
  // FAppBaseClass.LastUpdated := now;
  FAppBaseClass.LastUpdated := 42442.7488244444;
  FAppBaseClass.UpdatedBy := 'Mr Steve';
  FAppBaseClass.TrueOrFalse := True;

  // To ensure timestamps assigned by item creation are ignored
  FAppBaseClass.LclCreated := 0;
  FAppBaseClass.LclLastUpdated := 0;
end;

procedure TestTAppBaseClass.TearDown;
begin
  FAppBaseClass.Free;
  FAppBaseClass := nil;
end;

procedure TestTAppBaseClass.TestBaseClassToJSon;
var
  wkEvent: TSchedulerBaseEvent;
  ws: string;
  jo: TJSONObject;
const
  expected = '{"SchedulerBaseEventID":"","EventType":3,"StartDateTime":42442,' +
    '"EndDateTime":42443,"Caption":"My Event Caption","Description":"",' +
    '"Responsible":"","Instructions":"","Result":0,"Comments":"",' +
    '"LastUpdated":42442.7488244444,"UpdatedBy":"Mr Steve","TrueOrFalse":1,' +
    '"LclCreated":0,"LclLastUpdated":0}';
begin
  wkEvent := TSchedulerBaseEvent.Create;
  try
    // wkEvent.StartDateTime := trunc(now);
    wkEvent.StartDateTime := 42442;
    wkEvent.EndDateTime := wkEvent.StartDateTime + 1;
    wkEvent.Caption := 'My Event Caption';
    wkEvent.EventType := 3;
    // wkEvent.LastUpdated := now;
    wkEvent.LastUpdated := 42442.7488244444;
    wkEvent.UpdatedBy := 'Mr Steve';
    wkEvent.TrueOrFalse := True;

    // To ensure timestamps assigned by item creation are ignored
    wkEvent.LclCreated := 0;
    wkEvent.LclLastUpdated := 0;

    jo := wkEvent.GetJson;
    ws := jo.ToString;
    jo.Free;

    CheckEquals(expected, ws, 'Problem with TAppBaseClass.JSon');
  finally
    wkEvent.Free;
  end;
end;

procedure TestTAppBaseClass.TestBaseClassToJSonWithOptions;
var
  wkEvent: TSchedulerBaseEvent;
  ws: string;
const
  expected = '{"SchedulerBaseEventID":"","EventType":3,"StartDateTime":"2016-03-13T00:00:00",' +
    '"EndDateTime":"2016-03-14T00:00:00","Caption":"My Event Caption","Description":"",' +
    '"Responsible":"","Instructions":"","Result":0,"Comments":"",' +
    '"LastUpdated":"2016-03-13T17:58:18","UpdatedBy":"Mr Steve","TrueOrFalse":1,' +
    '"LclCreated":"1899-12-30T00:00:00","LclLastUpdated":"1899-12-30T00:00:00"}';
begin
  wkEvent := TSchedulerBaseEvent.Create;
  try
    // wkEvent.StartDateTime := trunc(now);
    wkEvent.StartDateTime := 42442;
    wkEvent.EndDateTime := wkEvent.StartDateTime + 1;
    wkEvent.Caption := 'My Event Caption';
    wkEvent.EventType := 3;
    // wkEvent.LastUpdated := now;
    wkEvent.LastUpdated := 42442.7488244444;
    wkEvent.UpdatedBy := 'Mr Steve';
    wkEvent.TrueOrFalse := True;

    // To ensure timestamps assigned by item creation are ignored
    wkEvent.LclCreated := 0;
    wkEvent.LclLastUpdated := 0;

    ws := wkEvent.GetJsonString([sjoIsoDateFormat]);

    CheckEquals(expected, ws,
      'Problem with TAppBaseClass.TestBaseClassToJSonWithOptions');
  finally
    wkEvent.Free;
  end;
end;

procedure TestTAppBaseClass.TestGetJsonString;
var
  wkEvent: TSchedulerBaseEvent;
  ws: string;
const
  expected = '{"SchedulerBaseEventID":"","EventType":3,"StartDateTime":42442,' +
    '"EndDateTime":42443,"Caption":"My Event Caption","Description":"",' +
    '"Responsible":"","Instructions":"","Result":0,"Comments":"",' +
    '"LastUpdated":42442.7488244444,"UpdatedBy":"Mr Steve","TrueOrFalse":1,' +
    '"LclCreated":0,"LclLastUpdated":0}';
begin
  wkEvent := TSchedulerBaseEvent.Create;
  try
    // wkEvent.StartDateTime := trunc(now);
    wkEvent.StartDateTime := 42442;
    wkEvent.EndDateTime := wkEvent.StartDateTime + 1;
    wkEvent.Caption := 'My Event Caption';
    wkEvent.EventType := 3;
    // wkEvent.LastUpdated := now;
    wkEvent.LastUpdated := 42442.7488244444;
    wkEvent.UpdatedBy := 'Mr Steve';
    wkEvent.TrueOrFalse := True;

    // To ensure timestamps assigned by item creation are ignored
    wkEvent.LclCreated := 0;
    wkEvent.LclLastUpdated := 0;

    ws := wkEvent.GetJsonString;

    CheckEquals(expected, ws, 'Problem with TAppBaseClass.GetJsonString');
  finally
    wkEvent.Free;
  end;
end;

procedure TestTAppBaseClass.TestSetFieldValueByString;
var
  wkEvent: TSchedulerBaseEvent;
  ws: string;
  wkInt: integer;
  wkTimeDate: TDateTime;
begin
  wkEvent := TSchedulerBaseEvent.Create;
  try
    ws := 'My Event Caption';
    CheckTrue(wkEvent.SetFieldValueByName('Caption', ws),
      'Problem finding field Caption');
    CheckEquals(ws, wkEvent.Caption, 'Problem setting Caption string');

    CheckFalse(wkEvent.SetFieldValueByName('Unknown Field', ws));

    for wkInt := -9 to 9999 do
    begin
      wkEvent.SetFieldValueByName('EventType', intToStr(wkInt));
      CheckEquals(wkInt, wkEvent.EventType,
        'Problem setting EventType integer');
    end;

    wkTimeDate := now;
    wkEvent.SetFieldValueByName('StartDateTime', Format('%g', [wkTimeDate]));
    CheckEquals(wkTimeDate, wkEvent.StartDateTime, 0.0000001,
      'Problem setting StartDateTime DateTime');

    CheckTrue(wkEvent.SetFieldValueByName('TrueOrFalse', '1'),
      'Problem setting field TrueOrFalse (1)');
    CheckTrue(wkEvent.TrueOrFalse, 'Problem setting field TrueOrFalse (2)');
    CheckTrue(wkEvent.SetFieldValueByName('TrueOrFalse', '0'),
      'Problem setting field TrueOrFalse (3)');
    CheckFalse(wkEvent.TrueOrFalse, 'Problem setting field TrueOrFalse (4)');
  finally
    wkEvent.Free;
  end;
end;

procedure TestTAppBaseClass.TestToSeperator;
var
  wkEvent: TSchedulerBaseEvent;
  ws: string;
const
  expected =
    '%0:s3%0:s42442%0:s42443%0:sMy Event Caption%0:s%0:s%0:s%0:s0%0:s%0:s' +
    '42442.7488244444%0:sMr Steve%0:s1%0:s0%0:s0';
begin
  wkEvent := TSchedulerBaseEvent.Create;
  try
    // wkEvent.StartDateTime := trunc(now);
    wkEvent.StartDateTime := 42442;
    wkEvent.EndDateTime := wkEvent.StartDateTime + 1;
    wkEvent.Caption := 'My Event Caption';
    wkEvent.EventType := 3;
    // wkEvent.LastUpdated := now;
    wkEvent.LastUpdated := 42442.7488244444;
    wkEvent.UpdatedBy := 'Mr Steve';
    wkEvent.TrueOrFalse := True;

    // To ensure timestamps assigned by item creation are ignored
    wkEvent.LclCreated := 0;
    wkEvent.LclLastUpdated := 0;

    ws := wkEvent.ToSeperatedValue;

    CheckEquals(Format(expected, [#9]), ws,
      'Problem with TAppBaseClass.ToSeperatedValue');
  finally
    wkEvent.Free;
  end;
end;

procedure TestTAppBaseClass.TestAssign;
var
  wkEvent: TSchedulerBaseEvent;
begin
  wkEvent := TSchedulerBaseEvent.Create;
  wkEvent.Assign(FAppBaseClass);
  try
    CheckEquals(wkEvent.StartDateTime, FAppBaseClass.StartDateTime,
      'Assign: TDateTime problem');
    CheckEquals(wkEvent.SchedulerBaseEventID,
      FAppBaseClass.SchedulerBaseEventID, 'Assign: string problem');
    CheckEquals(wkEvent.EventType, FAppBaseClass.EventType,
      'Assign: integer problem');
    CheckEquals(wkEvent.TrueOrFalse, FAppBaseClass.TrueOrFalse,
      'Assign: Boolean problem');
  finally
    wkEvent.Free;
  end;
end;

procedure TestTAppBaseClass.TestBaseClassCreationFromJSON;
var
  baseEvent: TSchedulerBaseEvent;
  JSONObject: System.JSON.TJSONObject;
  person: TPersonBase;
  customer: TCustomer;
  wkDate: TDate;
  wkTime: TTime;

begin
  JSONObject := System.JSON.TJSONObject.Create;
  try
    JSONObject.AddPair('SchedulerBaseEventID', 'ABC123');
    JSONObject.AddPair('EventType', '7');
    JSONObject.AddPair('TrueOrFalse', '1');
    JSONObject.AddPair('LastUpdated', '43392.5340280787');
    baseEvent := TSchedulerBaseEvent.CreateFromJSON(JSONObject);
    try
      CheckEquals('ABC123', baseEvent.SchedulerBaseEventID,
        'Unable to create from JSON Object (1 String)');
      CheckEquals(7, baseEvent.EventType,
        'Unable to create from JSON Object (2 Integer)');
      CheckEquals(True, baseEvent.TrueOrFalse,
        'Unable to create from JSON Object (3 Boolean)');
      CheckTrue(CompareValue(43392.5340280787, baseEvent.LastUpdated, 1E-11)
        = 0, 'Unable to create from JSON Object (4 Double/TDateTime)');
    finally
      baseEvent.Free;
    end;
  finally
    JSONObject.Free;
  end;

  JSONObject := System.JSON.TJSONObject.Create;
  try
    JSONObject.AddPair('Gender', '2');
    person := TPersonBase.CreateFromJSON(JSONObject);
    try
      CheckTrue(TAppGender.agFemale = person.Gender,
        'Unable to create from JSON Object (5 Enumerated type)');
    finally
      person.Free;
    end;
  finally
    JSONObject.Free;
  end;

  JSONObject := System.JSON.TJSONObject.Create;
  try
    JSONObject.AddPair('Key_Alpha', 'A2');
    JSONObject.AddPair('Name', 'Test A2');
    JSONObject.AddPair('DateAccountOpened', '2018-12-28T00:00:00');
    JSONObject.AddPair('DateOfLastPurchase', '2018-12-29');
    JSONObject.AddPair('EarliestDeliveryTime', '0001-01-01T09:30:00');
    JSONObject.AddPair('LatestDeliveryTime', '15:30:00');
    customer := TCustomer.CreateFromJSON(JSONObject);
    try
      wkDate := EncodeDate(2018, 12, 28);
      CheckTrue(CompareDate(customer.DateAccountOpened, wkDate) = 0,
        'Unable to create from JSON Object (6 TDate type from ISO date)');
      wkTime := EncodeTime(09, 30, 0, 0);
      CheckTrue(CompareTime(customer.EarliestDeliveryTime, wkTime) = 0,
        'Unable to create from JSON Object (7 TTime type from ISO time)');
      wkTime := EncodeTime(15, 30, 0, 0);
      CheckTrue(CompareTime(customer.LatestDeliveryTime, wkTime) = 0,
        'Unable to create from JSON Object (8 TTime type from hh:mm:ss time)');
      wkDate := EncodeDate(2018, 12, 29);
      CheckTrue(CompareDate(customer.DateOfLastPurchase, wkDate) = 0,
        'Unable to create from JSON Object (9 TDate type from yyyy-mm-dd date)');
    finally
      customer.Free;
    end;
  finally
    JSONObject.Free;
  end;
end;

procedure TestTAppBaseClass.TestBaseClassJSonArray;
var
  wkEvent: TSchedulerBaseEvent;
  ws: string;
const
  expected = '["",3,42442,42443,"My Event Caption","","","",0,"",' +
    '42442.7488244444,"Mr Steve",1,0,0]';
begin
  wkEvent := TSchedulerBaseEvent.Create;
  try
    // wkEvent.StartDateTime := trunc(now);
    wkEvent.StartDateTime := 42442;
    wkEvent.EndDateTime := wkEvent.StartDateTime + 1;
    wkEvent.Caption := 'My Event Caption';
    wkEvent.EventType := 3;
    // wkEvent.LastUpdated := now;
    wkEvent.LastUpdated := 42442.7488244444;
    wkEvent.UpdatedBy := 'Mr Steve';
    wkEvent.TrueOrFalse := True;

    // To ensure timestamps assigned by item creation are ignored
    wkEvent.LclCreated := 0;
    wkEvent.LclLastUpdated := 0;

    ws := wkEvent.GetJsonArray;

    CheckEquals(expected, ws, 'Problem with TAppBaseClass.JSonArray');
  finally
    wkEvent.Free;
  end;
end;

destructor TestTAppBaseClassList.Destroy;
begin
  // if Assigned(fFDGUIxWaitCursor1) then
  // fFDGUIxWaitCursor1.Free;
  inherited;
end;

procedure TestTAppBaseClassList.SetUp;
begin
  FAppBaseClassList := TStormBaseClassDbList<TSchedulerBaseEvent>.Create
    ('SchedulerBaseEvents');

  fFdcLcl := TFDConnection.Create(nil);
  fFdcLcl.Params.Values['DriverID'] := 'SQLite';
  fFdcLcl.Params.Values['OpenMode'] := 'CreateUTF16';
  fFdcLcl.Params.Values['StringFormat'] := 'Unicode';
  fFdcLcl.Params.Values['DateTimeFormat'] := 'DateTime';
  fFdcLcl.Params.Values['GUIDFormat'] := 'String';
  fFdcLcl.Params.Values['LockingMode'] := 'Normal';
  fFdcLcl.Params.Values['SharedCache'] := 'False';
  fFdcLcl.Params.Database := '..\..\StOrmUnitTestDb.s3db';

end;

procedure TestTAppBaseClassList.TearDown;
begin
  FAppBaseClassList.Free;
  FAppBaseClassList := nil;
  fFdcLcl.Close;
  fFdcLcl.Free;
end;

procedure TestTAppBaseClassList.TestCreateFromSeperated;
var
  wkPerson: TPersonBase;
  wkDate: TDateTime;
const
  personCsv = 'ABC,3,Alice,0,2,123.456,42551.6891189583';

begin
  wkPerson := TPersonBase.CreateFromSeperated
    (personCsv.Replace(',', stOrmSeperator, [rfReplaceAll]));
  try
    CheckEquals('ABC', wkPerson.PersonId);
    CheckEquals(3, wkPerson.TeamId, 'Team Id Failed');
    CheckEquals('Alice', wkPerson.PersonName);
    CheckTrue(wkPerson.Gender = agFemale);
    wkDate := 123.456;
    CheckEquals(wkDate, wkPerson.LclCreated);
    wkDate := 42551.6891189583;
    CheckEquals(wkDate, wkPerson.LclLastUpdated);
  finally
    wkPerson.Free;
  end;
end;

procedure TestTAppBaseClassList.TestCreateItem;
var
  wkT: TSchedulerBaseEvent;
  wkId: string;
begin
  wkT := FAppBaseClassList.CreateItem(fFdcLcl);
  wkId := wkT.Id;
  // FAppBaseClassList.UpdateDb(wkT, fFdcLcl);
  // Now delete it!!
  CheckTrue(FAppBaseClassList.DeleteDb(wkT, fFdcLcl),
    'Failed to remove added item!');
end;

procedure TestTAppBaseClassList.TestEnumeration;
var
  wkEvent: TSchedulerBaseEvent;
  counter: integer;
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  counter := 0;
  for wkEvent in FAppBaseClassList do
  begin
    CheckTrue(FAppBaseClassList[counter] = wkEvent,
      'Something in the enumerations isn''t right??');
    inc(counter);
  end;
  CheckEquals(FAppBaseClassList.Count, counter,
    'Wrong number of enumerations!');
end;

procedure TestTAppBaseClassList.TestFieldList;
var
  wkEvent: TSchedulerBaseEvent;
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  if FAppBaseClassList.Count > 0 then
  begin
    wkEvent := FAppBaseClassList[0];
    CheckFalse(wkEvent.FieldList.ContainsKey('SchedulerBaseEvents'));
    CheckTrue(wkEvent.FieldList.ContainsKey('SCHEDULERBASEEVENTID'));
    CheckTrue(wkEvent.FieldList.ContainsKey('CAPTION'));
    CheckTrue(wkEvent.FieldList.ContainsKey('LCLLASTUPDATED'));
  end
  else
    Fail('No items loaded!');
end;

procedure TestTAppBaseClassList.TestGetById;
var
  expectedBaseEvent: TSchedulerBaseEvent;
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  expectedBaseEvent := FAppBaseClassList.GetById
    ('{88FC8173-F2B0-E511-B487-001C421A6668}');
  CheckNotNull(expectedBaseEvent, 'FindById failed!');
end;

procedure TestTAppBaseClassList.TestGetJsonArray;
var
  ws: string;
  jo: TJSONObject;
const
  expected = '{"SchedulerBaseEvents":{"colums":["SchedulerBaseEventID",' +
    '"EventType","StartDateTime","EndDateTime","Caption","Description",' +
    '"Responsible","Instructions","Result","Comments","LastUpdated",' +
    '"UpdatedBy","TrueOrFalse","LclCreated","LclLastUpdated"],"data":[["';
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  jo := FAppBaseClassList.GetJsonArray;
  ws := jo.ToString;
  CheckTrue(ws.StartsWith(expected));
  CheckTrue(ws.EndsWith(']]}}'));
  jo.Free;
end;

procedure TestTAppBaseClassList.TestGetJsonString;
var
  ws: string;
  wkTeamList: TTeamBaseList;
  wkTeam: TTeamBase;
const
  expected = '{"Teams":[{"TeamId":1,"TeamName":"Red Team",' +
    '"LclCreated":43419.5542359259,"LclLastUpdated":43419.5542359259},' +
    '{"TeamId":2,"TeamName":"\"Blue Team\"","LclCreated":43419.5542359259,' +
    '"LclLastUpdated":43419.5542359259}]}';
begin
  wkTeamList := TTeamBaseList.Create('Teams');
  try
    wkTeam := TTeamBase.Create;
    wkTeam.TeamId := 1;
    wkTeam.TeamName := 'Red Team';
    wkTeam.LclCreated := 43419.5542359259;
    wkTeam.LclLastUpdated := 43419.5542359259;
    wkTeamList.Add(wkTeam);

    wkTeam := TTeamBase.Create;
    wkTeam.TeamId := 2;
    wkTeam.TeamName := '"Blue Team"';
    wkTeam.LclCreated := 43419.5542359259;
    wkTeam.LclLastUpdated := 43419.5542359259;
    wkTeamList.Add(wkTeam);

    ws := wkTeamList.AsJSONString;

    CheckEquals(expected, ws, 'Problem with TStormBaseClassList.AsJSONString');
  finally
    wkTeamList.Free;
  end;
end;

procedure TestTAppBaseClassList.TestJSON;
var
  ws: string;
  jo: TJSONObject;
const
  expected = '{"SchedulerBaseEvents":[{"SchedulerBaseEventID":"{';
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  jo := FAppBaseClassList.JSON;
  ws := jo.ToString;
  CheckTrue(ws.StartsWith(expected));
  CheckTrue(ws.EndsWith('}]}'));
  jo.Free;
end;

procedure TestTAppBaseClassList.TestJSONArray;
var
  ws: string;
  jo: TJSONObject;
const
  expected = '{"SchedulerBaseEvents":{"colums":["SchedulerBaseEventID",' +
    '"EventType","StartDateTime","EndDateTime","Caption","Description",' +
    '"Responsible","Instructions","Result","Comments","LastUpdated",' +
    '"UpdatedBy","TrueOrFalse","LclCreated","LclLastUpdated"],"data":[["{';
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  jo := FAppBaseClassList.GetJsonArray;
  ws := jo.ToString;
  CheckTrue(ws.StartsWith(expected));
  CheckTrue(ws.EndsWith(']]}}'));
  jo.Free;
end;

procedure TestTAppBaseClassList.TestLoadAllItems;
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
end;

procedure TestTAppBaseClassList.TestToSeperator;
var
  ws: string;
  wkBaseEvent: TSchedulerBaseEvent;
  expectedBaseEvent: TSchedulerBaseEvent;
  Values: TArray<String>;
begin
  FAppBaseClassList.LoadAllItems(fFdcLcl);
  expectedBaseEvent := FAppBaseClassList.GetById
    ('{88FC8173-F2B0-E511-B487-001C421A6668}');

  ws := FAppBaseClassList.ToSeperatedValue;
  // CheckTrue(ws.StartsWith(expected));
  // CheckTrue(ws.EndsWith(']]}}'));

  Values := ws.Split([#10]);
  wkBaseEvent := TSchedulerBaseEvent.CreateFromSeperated(Values[0]);
  try
    CheckEquals(wkBaseEvent.Responsible, 'Capt Kirk');
    CheckEquals(wkBaseEvent.EventType, 1);
    CheckEquals(wkBaseEvent.SchedulerBaseEventID,
      '{88FC8173-F2B0-E511-B487-001C421A6668}');
    if Assigned(expectedBaseEvent) then
      CheckEquals(wkBaseEvent.StartDateTime, expectedBaseEvent.StartDateTime);
    CheckFalse(wkBaseEvent.TrueOrFalse);
  finally
    wkBaseEvent.Free;
  end;
end;

procedure TestTAppBaseClassList.TestUpdateDb;
var
  Fdc: TFDConnection;
  Item: TSchedulerBaseEvent;
begin
  // TODO: Setup method call parameters
  Fdc := nil;
  // FAppBaseClassList.UpdateDb(Item, Fdc);
  // TODO: Validate method results
end;

initialization

// Register any test cases with the test runner
RegisterTest(TestTAppBaseClass.Suite);
RegisterTest(TestTAppBaseClassList.Suite);
fFDGUIxWaitCursor1 := TFDGUIxWaitCursor.Create(nil);
fFDGUIxWaitCursor1.Provider := 'Console';

finalization

fFDGUIxWaitCursor1.Free;

end.
